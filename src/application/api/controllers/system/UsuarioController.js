'use strict';

const debug = require('debug')('app:controller:usuario');
const moment = require('moment');
const path = require('path');
const { userData, generateToken } = require('../../../lib/auth');
const { crearPdf } = require('../../../lib/util');
const { ignoreTLS } = require('../../../../common/config/mail');

module.exports = function setupUsuarioController (services) {
  const { Iop, PersonaService, UsuarioService, ModuloService, Parametro,ProgramaService, DetallesprogramaService, DatosgeneralService,ConstanteService, KardexService } = services;

  async function personaSegip (req, res, next) {
    
    const { ci } = req.params;
    const { fechaNacimiento, complemento, tipoDoc = 'CI' } = req.query;

    let persona;
    try {
      persona = await PersonaService.find({
        nro_documento: ci + (complemento ? '-' + complemento : ''),
        tipo_documento: tipoDoc,
        fecha_nacimiento: moment(fechaNacimiento, 'DD/MM/YYYY').format('YYYY-MM-DD')
      });
      if (persona.code === 1) {
        persona = persona.data;
        persona = {
          paterno: persona.primer_apellido,
          materno: persona.segundo_apellido,
          nombres: persona.nombres,
          nacionalidad: persona.nacionalidad,
          telefono: persona.telefono,
          movil: persona.movil,
          genero: persona.genero,
          id_persona: persona.id,
          lugar_exp: persona.lugar_exp,
          lugar_nacimiento: persona.lugar_nacimiento,
          direccion: persona.direccion

        };
        persona = { persona };
      } else {
        return res.send({ observacion: 'La persona no está registrada en el sistema, complete los datos para registrarla.' });
      }
    } catch (e) {
      return next(e);
    }

    console.log('Respuesta segip 1', persona);
    if (persona.error && persona.error.indexOf('observación') !== -1) {
      return res.send({ observacion: persona.error });
    }
    res.send(persona);
  }

  // cambiar contrasena
  async function cambiarPass (req, res, next) {
    debug('Cambiar contraseña de usuario');

    const { password, newPassword } = req.body;

    try {
      let _user = await userData(req, services);
      let user = await UsuarioService.userExist(_user.usuario, password);
      if (user.code === 1) {
        user = await UsuarioService.update({
          id: _user.id,
          contrasena: newPassword
        });
        if (user.code === 1) {
          res.send({ message: 'Contraseña cambiada correctamente' });
        } else {
          res.status(412).send({ error: user.data.message || user.data || 'No se puede cambiar la contraseña' });
        }
      } else {
        res.send({ error: 'Su contraseña anterior es incorrecta' });
      }
    } catch (e) {
      return next(e);
    }
  }

  // desactivar cuenta
  async function desactivarCuenta (req, res, next) {
    debug('Desactivar cuenta de usuario');

    const { password } = req.body;
    try {
      let _user = await userData(req, services);
      let user = await UsuarioService.userExist(_user.usuario, password);
      if (user.code === 1) {
        user = await UsuarioService.update({
          id: _user.id,
          estado: 'INACTIVO'
        });
        if (user.code === 1) {
          res.send({ message: '¡Cuenta desactivada!' });
        } else {
          res.status(412).send({ error: user.data.message || user.data || 'No se pudo desactivar la cuenta' });
        }
      } else {
        res.send({ error: 'Su contraseña es incorrecta' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function obtenerMenu (req, res, next) {
    debug('Obteniendo menú y permisos');

    let user = await userData(req, services);
    console.log('usuario menu', user);
    let menu;
    let token;
    let permisos = {};
    const { all } = req.query;

    try {
      // Obteniendo menu
      menu = await ModuloService.getMenu(user.id_rol, false, !!all);

      if (all) {
        return res.send({ menu: menu.data.menu });
      }
      let permissions = menu.data.permissions;
      menu = menu.data.menu;

      // Generando token
      token = await generateToken(Parametro, user.usuario, permissions);

      // Formateando permisos
      permissions.map(item => (permisos[item] = true));
    } catch (e) {
      return next(e);
    }

    res.send({
      permisos,
      menu,
      token
    });
  }

  async function regenerarPassword (req, res, next) {
    debug('Regenerando password');

    let user = await userData(req, services);
    const { id } = req.params;
    try {
      let result = await UsuarioService.regenerar(id, user.id);
      if (result.code === -1) {
        return next(new Error(result.message));
      }
      if (result.data) {
        res.send(result.data);
      } else {
        return next(new Error('No se pudo regenerar la contraseña.'));
      }
    } catch (e) {
      return next(e);
    }
  }

  async function generarPdfEntrevistas (req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { estado_est,idPrograma,idVersion, userName } = req.params;
      // let datos = await UsuarioService.findStudentUsers(req.params, estado_est,idPrograma,idVersion);
      let datos = await UsuarioService.findAll({ 
        estado_estudiante: estado_est, 
        id_programa: idPrograma, 
        id_version: idVersion 
      });
      let user = await UsuarioService.findAll({
        usuario: userName
      });
      console.log('prueba de nombre de usuario para audiotria: ', );
      let usuarioRep = user.data.rows[0].persona.nombres.substring(0, 1) + user.data.rows[0].persona.primer_apellido.substring(0, 1) + user.data.rows[0].persona.segundo_apellido.substring(0, 1) ;
      console.log('datos de usuarioRep: ', usuarioRep);
      let prog = await ProgramaService.findById(idPrograma);
      let programa = prog.data;
      let detallesPrograma = await DetallesprogramaService.findById(idVersion);
      let version = detallesPrograma.data;
      let fecha_emision = new Date();
      moment.locale('es');
      fecha_emision = moment(fecha_emision).format('LL');
      let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a')
      if (datos.code === 1) {
        let usuario = datos.data;
        let nombrePrograma = `PROGRAMA: ${programa.nombre}\n`;
        let gestion = `GESTIÓN: ${version.gestion} \n`;
        let headers = [ {text: 'Nombre Completo', bold: true, alignment: 'center', fontSize: 7,fillColor: '#cccccc' }, {text: 'Fecha', bold: true, alignment: 'center', fontSize: 7, fillColor: '#cccccc' }, {text: 'Hora', bold: true, alignment: 'center', fontSize: 7, fillColor: '#cccccc'}, {text: 'Lugar', bold: true, alignment: 'center', fontSize: 7, fillColor: '#cccccc'  }];
        let filas = [headers];
        for (let item of usuario.rows) {
          filas.push([
            {text: item.persona.nombre_completo, fontSize: 7 }, 
            {text: moment (item.fecha_entrevista).format('DD/MM/YYYY'), fontSize: 7 }, 
            {text: item.hora_entrevista,  fontSize: 7 },
            {text: item.lugar_entrevista, fontSize: 7 }          
          ]);
        }
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:10,
                columns: [
                {
                    fontSize: 9,
                    text:[
                    {
                    text: '--------------------------------------------------------------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };      
        },
          content: [    
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 80, y: 65}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 110,
              absolutePosition: {x: 450, y: 70}
            },            
            { style: 'header',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [ {text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS       \n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n LISTA DE ENTREVISTAS\n'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              margin: [20, 25, 10, 15],
              table: {
                widths: [ 150, '*' ],
                body: [
                  [{text: 'PROGRAMA:', style: 'datos' }, {text: programa.nombre, fontSize: 7}],
                  [{text: 'VERSIÓN:', style: 'datos' }, {text: version.version, fontSize: 7}],
                  [{text: 'GESTIÓN:', style: 'datos'}, {text: version.gestion, fontSize: 7}],
                  [{text: 'COORDINADOR:', style: 'datos' }, {text: version.coordinador, fontSize: 7}],
                  [{text: 'FECHA DE EMISIÓN:', style: 'datos'}, {text: fecha_emision, fontSize: 7}]
                ]
              },
              layout:'noBorders'
            },
            //  { text: nombrePrograma, style: 'info' },
            //  { text: gestion, style: 'info' },
            //  { text: 'FECHA EMISION: ' + fecha_emision + '\n\n', style: 'info'},
            { 
              margin: [20, 5, 20, 3], // optional              
              table: {
                headerRows: 1,
                widths: [ 230, 50, 50, '*' ],
                body: filas
              }
            },
            {
              text: `CIDES*${usuarioRep}*${fechaRep}`, style: 'rev'
            }
          ],
          styles: {
            header: {
              fontSize: 13,
              bold: true,
              alignment: 'center',
              margin: [20, 20, 5, 15]
            },
            info: {
              margin: [10, 5, 5, 3],
              fontSize: 11,
              bold: true,
              fillColor: '#c7c9c8'
            },
            datos: {
              fontSize: 7,
              bold: true,
              margin: [15, 0, 0, 0]
            },
            rev: {
              fontSize: 8, 
              bold: true, 
              margin: [20, 5, 5, 5],
              italics: true
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  

  function getCodigo (codigo = '', prefix = '', length = 5) {
    let year = parseInt((new Date().getFullYear() + '').substring(2));
    let number = 1;
    if (codigo.split('/').length > 1) {
      let [ code, oldYear ] = codigo.split('/');
      if (parseInt(oldYear) === year) {
        number = parseInt(code.replace(`${prefix}-`, '')) + 1;
      }
    }
    return `${prefix}-${(number).toString().padStart(length, '0')}/${year}`;
  }

  async function generarPdfCartaAceptacion (req, res, next) {
    console.log('ingresa a usuario Controller');
    try {
      const { idUsuario, idPrograma, idVersion } = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      console.log('Informacion de Datos: ', datos);
      let prog = await ProgramaService.findById(datos.data.id_programa);
      let programa = prog.data;
      let detallesPrograma = await DetallesprogramaService.findById(datos.data.id_version);
      let version = detallesPrograma.data;
      let correlativo_carta = 0;
      let tp = programa.tipo;
      let datosGenerales = await DatosgeneralService.findAll({ 
        gestion: version.gestion 
      });
      console.log('Datos Generales: ', datosGenerales.data.rows[0].id);
      let fecha_emision = new Date();
      moment.locale('es');
      fecha_emision = moment(fecha_emision).format('LL');
      let fecha_carta = `La Paz, ${fecha_emision}`;
      if (datos.code === 1) {
        let usuario = datos.data;
        if (usuario.nro_ccaceptacion) {
          correlativo_carta = usuario.nro_ccaceptacion;
        } else {
          correlativo_carta = datosGenerales.data.rows[0].cc_aceptacion;
          let correlativo = parseInt(datosGenerales.data.rows[0].cc_aceptacion) + 1;
          let actualizaDatoGeneral = await DatosgeneralService.createOrUpdate({
            id: datosGenerales.data.rows[0].id,
            cc_aceptacion: correlativo
          });
          console.log('actualiza el correlativo ', actualizaDatoGeneral);
          datos = await UsuarioService.update({
            id: idUsuario,
            nro_ccaceptacion: correlativo_carta
          });
        }
        let coordinadorPrograma = version.coordinador;
        let nombre_postulante = `Sr(a). ${usuario.persona.nombre_completo} \n Presente. \n\n De mi consideración:\n\n`;
        let cite = `CITE DIREC.CIDES N° ${correlativo_carta}/${version.gestion}\n ${fecha_carta} \n`;
        let parrafo_aceptacion = `Habiendo postulado al programa ${programa.nombre} (${version.periodo}) en su versión ${version.version} y luego de una entrevista satisfactoria con ${coordinadorPrograma} Coordinador(a) del programa, comunico a usted que su solicitud de admisión ha sido aceptada, la misma estará sujeta a estándares académicos establecidos.\n\n Sin otro particular, saludo a usted atentamente.`
        let firma_director = `${datosGenerales.data.rows[0].dir_acronimo} ${datosGenerales.data.rows[0].nom_director} \n ${datosGenerales.data.rows[0].dir_descripcion} `;
        let data = {
          content: [
            { style: 'header',
              table: {
                widths: [420],
                body: [
                  [{border: [true, true, true, true],
                   text: cite, italics: true}    
                  ]
                ]
              }    
            },
            {
              text: [
                nombre_postulante,
              ],
              style: 'parrafo'  
            },
            {
              text: [parrafo_aceptacion,
              '\n\nSaludos Cordiales.'
              ],
              style: 'parrafo'
            },
            {
              text: [firma_director],
              style: 'firma'

            }
          ],
          styles: {
            header: {
              fontSize: 11,
              bold: true,
              alignment: 'left',
              margin: [20, 130, 40, 80]
            },
            firma: {
              fontSize: 11,
              italics: true,
              alignment: 'center',
              margin: [0, 120, 0, 120],
              bold: true
            },
            parrafo: {
              margin: [20, 0, 40, 0],
              fontSize: 10,
              alignment: 'justify'
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function generarPdfFormularioInscripcion (req, res, next) {
    try {
      const { idUsuario} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      let fecha_emision = new Date();
      moment.locale('es');
      fecha_emision = moment(fecha_emision).format('LL');
      if (datos.code === 1) {fecha_emision
        let usuario = datos.data;
        let persona = usuario.persona;
        let programa = usuario.programa;
        let detalles = usuario.detallesprograma;
        let kardex = usuario.kardex;
        let espacio = `{text: '',border:[false,false,false,false]}`;
        if(persona.tipo_documento === 'CI') {
          persona.tipo_documento = 'CÉDULA DE IDENTIDAD';
        }
        if(persona.genero === 'M') {
          persona.genero = 'MASCULINO';
        } else {
          persona.genero = 'FEMENINO';
        }
        let listaIdiomas = [];
        let lenguaje = [];
        let valor = [];
        let estudio = [];
        let fecha_estudio = [];
        let institucion = [];
        for (let item of kardex.idiomas) {
          //item.texto = item.texto.substring(3).toUpperCase();
          lenguaje.push([     
            item.texto
          ]);
          if(item.valor === '') {
            item.valor = 'Ninguno';
          }
          item.valor = item.valor.toUpperCase();
          valor.push([
            item.valor
          ]);        
        }
        for (let item of kardex.estudios) {
          estudio.push([
            item.nombre
          ]); 
          fecha_estudio.push([
            item.fecha
          ]);
          institucion.push([
            item.tipo
          ]);
        }
        console.log('USUARIO: ', usuario);
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:10,
                columns: [
                {
                    fontSize: 9,
                    text:[
                    {
                    text: '--------------------------------------------------------------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };      
          },
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 66, y: 45}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 42}
            },
            { style: 'header',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [ {text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n BASE DE DATOS - ANTECEDENTES ACADÉMICOS\n ALUMNOS CIDES-UMSA\n\n FORMULARIO DE INSCRIPCIÓN\n'}]
                ]
              },
              layout: 'noBorders'
            },  
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS GENERALES'}]
                ]
              },
              layout: 'noBorders'
            }, 
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*',5,100],
                body: [
                  [{text: 'PROGRAMA ACADEMICO', style: 'info'},{}, {text: '1. FECHA DE INSCRIPCIÓN', style: 'info'}],
                  [{text: programa.nombre  +' ('+ detalles.periodo+')'},{}, {text: kardex.fecha_inscripcion}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '2. APELLIDOS', style: 'info'},{}, {text: '3. NOMBRES', style: 'info'}],
                  [{text: persona.primer_apellido +' '+ persona.segundo_apellido},{}, {text:persona.nombres}],
                  [{text: 'TIPO DE DOCUMENTO', style: 'info'},{}, {text: '4. NRO. DOCUMENTO', style: 'info'}],
                  [{text: persona.tipo_documento},{},{text: persona.nro_documento}]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*', 5,'*',5,'*'],
                body: [
                  [{text: '5. LUGAR DE EXPEDICIÓN', style: 'info'},{}, {text: '6. EDAD', style: 'info'}, {}, {text: '7. SEXO', style: 'info'},{},{text: '8. ESTADO CIVIL', style: 'info'}],
                  [{text: persona.lugar_exp},{},kardex.edad,{} , persona.genero, {},kardex.estado_civil]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '9. FECHA DE NACIMIENTO', style: 'info'}, {}, {text: '10. LUGAR DE NACIEMIENTO', style: 'info'}],
                  [{text:persona.fecha_nacimiento},{}, {text: persona.lugar_nacimiento}],
                  [{text: '11. LUGAR DE RESIDENCIA', style: 'info'},{},{text: '12. RESIDENCIA', style: 'info'}],
                  [{text: kardex.lugar_residencia },{},{text: kardex.residencia}]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS DE SU DOMICILIO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '13. DIRECCIÓN', style: 'info'}],
                  [persona.direccion]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '14. TELÉFONO', style: 'info'},{}, {text: 'CELULAR', style: 'info'}],
                  [persona.telefono,{},persona.movil]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '15. EMAIL(PERSONAL)', style: 'info'}],
                  [usuario.email]
                ]
              },
              layout: 'noBorders'
            }, 
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS LUGAR DE TRABAJO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '16. RAZÓN SOCIAL', style: 'info'}],
                  [kardex.razon_social+ ''],
                  [{text: '17. DIRECCIÓN', style: 'info'}],
                  [kardex.t_direccion + '']
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '18. TELÉFONO', style: 'info'},{}, {text: '19. EMAIL(INSTITUCIONAL)', style: 'info'}],
                  [kardex.t_telefono,{},kardex.t_email]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS BACHILLERATO O COLEGIO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '20. AÑO EGRESO COLEGIO', style: 'info'},{}, {text: '21. ADMIN. COLEGIO', style: 'info'}],
                  [kardex.anio_egreso,{},kardex.admin_colegio],
                  [{text: '22. NOMBRE COLEGIO', style: 'info'},{}, {}],
                  [kardex.nombre_colegio, {}, {}]
                ] 
              },
              layout: 'noBorders'
            },
            {
              text: '\n'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [  
                  [{text: 'DATOS UNIVERSITAROS'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [300, 5, '*'],
                body: [
                  [{text: '23. LUGAR DE ESTUDIOS UNIVERSITARIOS', style: 'info'},{}, {text: '24. AÑO DE FINALIZACIÓN', style: 'info'}],
                  [kardex.univ_lugar, {}, kardex.univ_anio_fin]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '25. UNIVERSIDAD DE PROCEDENCIA', style: 'info'}],
                  [kardex.universidad+ '']
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '26. ÚLTIMO GRADO ACADÉMICO OBTENIDO', style: 'info'},{}, {text: '27. ESTADO DEL TÍTULO', style: 'info'}],
                  [kardex.u_titulacion,{},kardex.estado_titulo]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '28. PROFESIÓN', style: 'info'}],
                  [kardex.profesion+ '']
                ]
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'IDIOMAS'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*',5,'*'],
                body: [
                  [{text: 'LENGUAJE', style: 'info'}, {}, {text: 'NIVEL', style: 'info'}],
                  [lenguaje, {}, valor]
                ]
              },
              layout: 'noBorders'
            },
             { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'ULTIMOS ESTUDIOS REALIZADOS'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [250, 5, '*', 5,'*'],
                body: [
                  [{text: '34. ULTIMOS ESTUDIOS REALIZADOS', style: 'info'},{}, {text: 'FECHA', style: 'info'},{}, {text: 'INS. PÚBLICA/PRIVADA', style: 'info'}],
                  [estudio,{},fecha_estudio,{},institucion]
                ] 
              },
              layout: 'noBorders'
            },
            { text: 'Declaración Jurada\n', style: 'header', alignment: 'center', decoration: 'underline'},
            { text: [ `Yo ${persona.nombre_completo}, declaro y juro la veracidad de esta información, para afectos legales y fines consiguientes, firmo al pie del documento, en la ciudad de La Paz,  a la fecha, ${fecha_emision}.\n\n\n\n`],
              style: 'parrafo',
              alignment: 'justify'
            },
            {
              style: 'tableFirmas',
              table: {
                headerRows: 1,
                widths: ['*',25,'*'],
                body: [
                  [{text: persona.nombre_completo, style: 'firma',  border: [false,true,false,false]}, {text:'', border:[false,false,false,false]}, {text: detalles.coordinador, style: 'firma',border: [false,true,false,false]}],
                  [{text: 'Firma Alumno', style: 'firma',border: [false,false,false,false]}, {text:'', border:[false,false,false,false]}, {text: 'VoBo Coordinador', style: 'firma',border: [false,false,false,false]}]
                ]
              }
            }
          ], 
          styles: {
            header: {
              fontSize: 13,
              bold: true,
              alignment: 'center',
              margin: [5, 10, 5, 3]
            },
            firma: {
              fontSize: 10,
              italics: true,
              alignment: 'center',
              bold: true,
              margin: [0, 0, 0, 0],
            },
            parrafo: {
              margin: [5, 10, 5, 5],
              alignment: 'center'
            },
            tableFirmas: {
              margin: [40, 0, 40, 0]
            },
            info: {
              fontSize: 8,
              bold: true,
              fillColor: '#c7c9c8'
            },
            tableDatos: {
              margin: [5, 0, 5, 3], 
              fontSize: 11
            },
            subtitulo: {
              fontSize: 14,
              fillColor: '#c7c9c8',
              bold: true,
              margin: [5, 15, 5, 5]
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

async function generarPdfRequisitos (req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
     const { idUsuario, userName} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      if (datos.code === 1) {
        console.log('Ingresa a requisitos')
        let usuario = datos.data
        let persona = datos.data.persona;
        let contrato = datos.data;
        let programa = datos.data.programa ;
        let detallesp = datos.data.detallesprograma;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} (${detallesp.version} VERSIÓN) `;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let user = await UsuarioService.findAll({
          usuario: userName
        });
        let usuarioRep = user.data.rows[0].persona.nombres.substring(0, 1) + user.data.rows[0].persona.primer_apellido.substring(0, 1) + user.data.rows[0].persona.segundo_apellido.substring(0, 1) ;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let requisitos = JSON.parse(JSON.stringify(usuario.requisitos));
        let headers =[ {text: 'Nro.', style: 'headerRequisitos'}, {text: 'REQUISITO', style: 'headerRequisitos'},{text: 'SI/NO', style: 'headerRequisitos'}, {text: 'OBSERVACIONES', style: 'headerRequisitos'}];
        let filas = [headers];
        let nro = 0;
        console.log('requisitos', requisitos);
        for (let item of requisitos ) {
          nro = nro + 1;
          item.nro = nro;
          if(!item.observacion) {
            item.observacion = '';
          }
          if (!item.value) {
             item.value = 'NO'; 
          } else {
            item.value = 'SI';
          }
          item.nombre = {text: item.nombre, alignment: 'left', fontSize: 7};
          item.nro = {text: item.nro, fontSize: 7};
          item.value = {text: item.value, fontSize: 7};
          item.observacion = {text: item.observacion.toUpperCase(), fontSize: 7};
          filas.push([
            item.nro,
            item.nombre, 
            item.value,
            item.observacion
          ]);
         console.log('ITEM', filas);
        }
        let data = {
            footer: function(currentPage, pageCount) {
              return {
                  margin:10,
                  columns: [
                  {
                      fontSize: 9,
                      text:[
                      {
                      text: '--------------------------------------------------------------------------' +
                      '\n',
                      margin: [0, 20]
                      },
                      {
                      text: currentPage.toString() + ' de ' + pageCount,
                      }
                      ],
                      alignment: 'center'
                  }
                  ]
              };      
          },
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 66, y: 45}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 42}
            },
            { text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n REQUISITOS PRESENTADOS\n\n', style: 'header', alignment: 'center' },
            {
            style: 'tableDatos',
            table: {
              headerRows: 1,
              widths: [100,'*'],
              body: [
                [ { text: 'Nombre Completo:', style: 'info' }, {text: persona.nombre_completo, fontSize: 7}],
                [{ text: 'Nro. de Documento:', style: 'info' }, {text: persona.nro_documento, fontSize: 7}],
                [{ text: 'Programa:', style: 'info' }, {text: nomPrograma, fontSize: 7}],
                [{ text: 'Fecha de Emisión:', style: 'info'}, {text: fecha_emision, fontSize: 7}],
              ]
            },
            layout: 'noBorders'
          },
          {
            style: 'infoRequisitos',
            table: {
              headerRows: 1,
              widths: [20, '*', 30,100],
              body: filas
            }
          },
          {
            text: `CIDES*${usuarioRep}*${fechaRep}`, style: 'rev'
          },
          {
              style: 'tableFirmas',
              table: {
                headerRows: 1,
                widths: ['*',25,'*'],
                body: [
                  [{text: 'Firma Estudiante', style: 'firma',border: [false,true,false,false]}, {text:'', border:[false,false,false,false]}, {text: 'Firma Responsable de Registro CIDES', style: 'firma',border: [false,true,false,false]}]
                ]
              }
            }
          ],
          styles: {
            header: {
              fontSize: 15,
              bold: true,
              alignment: 'center',
              margin: [0, 20, 0, 20]
              //  decoration: 'underline'
            },
            infoRequisitos: {
              fontSize: 8,
              alignment: 'center',
              margin: [20, 0, 20, 5]
            },
            headerRequisitos: {
              fontSize: 8,
              alignment: 'center',
              bold: true
            },
            tableDatos: {
              margin: [20, 10, 0, 20]
            },
            info: {
              fontSize: 8,
              bold: true
            },
            firma: {
              fontSize: 10,
              italics: true,
              alignment: 'center',
              bold: true
            },
            tableFirmas: {
              margin: [40, 0, 40, 0]
            },
            rev: {
              fontSize: 8, 
              bold: true, 
              //margin: [20, 5, 5, 5],
              margin: [20, 0, 20, 70],
              italics: true
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function generarPdfDocRequisitos (req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
     const { idUsuario, userName} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      if (datos.code === 1) {
        console.log('Ingresa a requisitos')
        let usuario = datos.data
        let persona = datos.data.persona;
        let nombre_docente = usuario.acronimo + ' ' +  persona.nombre_completo;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let user = await UsuarioService.findAll({
          usuario: userName
        });
        let usuarioRep = user.data.rows[0].persona.nombres.substring(0, 1) + user.data.rows[0].persona.primer_apellido.substring(0, 1) + user.data.rows[0].persona.segundo_apellido.substring(0, 1) ;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let requisitos = JSON.parse(JSON.stringify(usuario.requisitos));
        let headers =[ {text: 'Nro.', style: 'headerRequisitos'}, {text: 'REQUISITO', style: 'headerRequisitos'},{text: 'SI/NO', style: 'headerRequisitos'}, {text: 'OBSERVACIONES', style: 'headerRequisitos'}];
        let filas = [headers];
        let nro = 0;
        console.log('requisitos', requisitos);
        for (let item of requisitos ) {
          nro = nro + 1;
          item.nro = nro;
          if(!item.observacion) {
            item.observacion = '';
          }
          if (!item.value) {
             item.value = 'NO'; 
          } else {
            item.value = 'SI';
          }
          item.nombre = {text: item.nombre, alignment: 'left'};
          filas.push([
            item.nro,
            item.nombre, 
            item.value,
            item.observacion
          ]);
         console.log('ITEM', filas);
        }
        let data = {
            footer: function(currentPage, pageCount) {
              return {
                  margin:10,
                  columns: [
                  {
                      fontSize: 9,
                      text:[
                      {
                      text: '--------------------------------------------------------------------------' +
                      '\n',
                      margin: [0, 20]
                      },
                      {
                      text: currentPage.toString() + ' de ' + pageCount,
                      }
                      ],
                      alignment: 'center'
                  }
                  ]
              };      
          },
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 66, y: 45}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 42}
            },
            { text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n REQUISITOS PRESENTADOS\n DOCENTE\n\n', style: 'header', alignment: 'center' },
            {
            style: 'tableDatos',
            table: {
              headerRows: 1,
              body: [
                [ { text: 'Docente:', style: 'info' },nombre_docente],
                [{ text: 'Nro. de Documento:', style: 'info' }, persona.nro_documento],
                [{ text: 'Fecha de Emisión:', style: 'info'}, fecha_emision]
              ]
            },
            layout: 'noBorders'
          },
          {
            style: 'infoRequisitos',
            table: {
              headerRows: 1,
              widths: [20, '*', 35,160],
              body: filas
            }
          },
          {
            text: `CIDES*${usuarioRep}*${fechaRep}`, style: 'rev'
          } 
          /*  {
              style: 'tableFirmas',
              table: {
                headerRows: 1,
                widths: ['*',25,'*'],
                body: [
                  [{text: 'Firma Estudiante', style: 'firma',border: [false,true,false,false]}, {text:'', border:[false,false,false,false]}, {text: 'Firma Responsable de Registro CIDES', style: 'firma',border: [false,true,false,false]}]
                ]
              }
            } */
          ],
          styles: {
            header: {
              fontSize: 15,
              bold: true,
              alignment: 'center',
              margin: [0, 20, 0, 20]
              //  decoration: 'underline'
            },
            infoRequisitos: {
              fontSize: 8,
              alignment: 'center',
              margin: [20, 0, 20, 5]
            },
            headerRequisitos: {
              fontSize: 10,
              alignment: 'center',
              bold: true
            },
            tableDatos: {
              margin: [20, 10, 0, 20]
            },
            info: {
              fontSize: 12,
              bold: true
            },
            firma: {
              fontSize: 10,
              italics: true,
              alignment: 'center',
              bold: true
            },
            tableFirmas: {
              margin: [40, 0, 40, 0]
            },
            rev: {
              fontSize: 8, 
              bold: true, 
              //margin: [20, 5, 5, 5],
              margin: [20, 0, 20, 70],
              italics: true
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function generarPdfKardexDocente (req, res, next){
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
     const { idUsuario} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      
      if (datos.code === 1) {
        let usuario = datos.data
        let persona = datos.data.persona;
        let kardex = usuario.kardex;
        let fecha_emision = new Date();
        let t_nivel = [];
        let t_titulo = [];
        let t_universidad = [];
        let t_anio = [];
        let d_materia = [];
        let d_facultad = [];
        let d_institucion = [];
        let d_anio = [];
        let p_modulo = [];
        let p_programa = [];
        let p_universidad = [];
        let p_anio = [];
        let i_tipo = [];
        let i_titulo = [];
        let i_anio = [];
        let tu_tesis = [];
        let tu_grado = [];
        let tu_universidad = [];
        let tu_anio = [];
        let tema = [];
        let especifico = [];

        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        var arr = kardex.d_tematica;
        console.log( 'variable arr', kardex.d_tematica);
        arr = arr.split(',');
        console.log( 'variable arr', arr );
        
        let tematica = [];
        
        for (let i = 0; i< arr.length; i++){
          console.log('for arr',arr[i]);
          let constante = await ConstanteService.findAll({
            codigo: arr[i]
          });
          let c_tematica = constante.data;
          for (let item of c_tematica.rows){
            if(item.codigo === arr[i]){
              console.log('item', item.nombre);
              tematica.push([
                item.nombre = {text: item.nombre, style: 'infoBlock'}
              ]);  
            }
          }
        }
        
        for (let item of kardex.d_titulos) {
          t_nivel.push([
            item.nivel = {text: item.nivel , style: 'infoBlock'}
          ]); 
          t_titulo.push([
            item.nombre = {text: item.nombre , style: 'infoBlock'}
          ]);
          t_universidad.push([
            item.universidad = {text: item.universidad , style: 'infoBlock'}
          ]);
          t_anio.push([
            item.anio = {text: item.anio , style: 'infoBlock'}
          ])
        }

        for(let item of kardex.d_pregrados){
          d_materia.push([
            item.materia = {text: item.materia, style: 'infoBlock'}
          ]);
          d_facultad.push([
            {text: item.facultad, style: 'infoBlock'}
          ]);
          d_institucion.push([
            item.institucion = {text: item.institucion, style: 'infoBlock'}
          ]);
          d_anio.push([
            item.desde = {text: item.desde, style: 'infoBlock'}
          ]);  
        }

        for (let item of kardex.d_postgrados) {
          p_modulo.push([
            item.modulo = {text: item.modulo, style: 'infoBlock'}
          ]);
          p_programa.push([
            item.programa = {text: item.programa, style: 'infoBlock'}
          ]);
          p_universidad.push([
            {text: item.universidad, style: 'infoBlock'}
          ]);
          p_anio.push([
            item.anio = {text: item.anio, style: 'infoBlock'}
          ]); 
        }

        for (let item of kardex.d_pintelectual) {
          i_tipo.push([
            item.tipo = {text: item.tipo, style: 'infoBlock'}
          ]);
          i_titulo.push([
            item.titulo = {text: item.titulo, style: 'infoBlock'}
          ]);
          i_anio.push([ 
            item.anio = {text: item.anio, style: 'infoBlock'}
          ]);
        }

        for (let item of kardex.d_cursosr) {
          tu_tesis.push([
            item.nombre = {text: item.nombre, style: 'infoBlock'}
          ]);
          tu_grado.push([
            item.grado = {text: item.grado, style: 'infoBlock'}
          ]);
          tu_universidad.push([
            item.universidad = {text: item.universidad, style: 'infoBlock'}
          ]);
          tu_anio.push([
            item.anio = {text: item.anio, style: 'infoBlock'}
          ]);
        }

        for (let item of kardex.d_cursosd) {
          tema.push([
            item.tema = {text: item.tema, style: 'infoBlock'}
          ]);
          especifico.push([
            item.especifico = {text: item.especifico, style: 'infoBlock'}
          ]);
        }
        let data = {
          content: [
            { style: 'header',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [ {text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n BASE DE DATOS - KARDEX ACADÉMICO DOCENTES'}]
                ]
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: ' I. DATOS GENERALES'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: 'APELLIDOS', style: 'info'},{}, {text: 'NOMBRES', style: 'info'}],
                  [{text: persona.primer_apellido +' '+ persona.segundo_apellido},{}, {text:persona.nombres}],
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*', 5, '*'],
                body: [
                  [{text: 'TIPO DE DOCUMENTO', style: 'info'}, {}, {text: 'NRO. DOCUMENTO', style: 'info'}, {},{text: 'LUGAR DE EXPEDICION', style: 'info'}],
                  [{text: persona.tipo_documento},{},{text: persona.nro_documento}, {}, {text: persona.lugar_exp}]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: 'LUGAR DE NACIEMIENTO', style: 'info'}, {}, {text: 'FECHA DE NACIMIENTO', style: 'info'}],
                  [{text:persona.lugar_nacimiento},{}, {text: persona.fecha_nacimiento}]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS DE SU DOMICILIO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '13. DIRECCIÓN', style: 'info'}],
                  [persona.direccion]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '14. TELÉFONO', style: 'info'},{}, {text: 'CELULAR', style: 'info'}],
                  [persona.telefono,{},persona.movil]
                ] 
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '15. EMAIL(PERSONAL)', style: 'info'}],
                  [usuario.email]
                ]
              },
              layout: 'noBorders'
            }, 
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'DATOS LUGAR DE TRABAJO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*'],
                body: [
                  [{text: '16. RAZÓN SOCIAL', style: 'info'}],
                  [kardex.razon_social+ ''],
                  [{text: '17. DIRECCIÓN', style: 'info'}],
                  [kardex.t_direccion + '']
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 5, '*'],
                body: [
                  [{text: '18. TELÉFONO', style: 'info'},{}, {text: '19. EMAIL(INSTITUCIONAL)', style: 'info'}],
                  [kardex.t_telefono,{},kardex.t_email]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
            table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'II. TITULOS ACADEMICOS'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [80, 2, 130, 2,'*', 2, 30],
                body: [
                  [{text: 'NIVEL', style: 'info'},{}, {text: 'TÍTULO ACADEMICO', style: 'info'},{}, {text: 'UNIVERSIDAD/INSTITUCIÓN', style: 'info'}, {}, {text: 'AÑO', style: 'info'}],
                  [ t_nivel,{},t_titulo,{},t_universidad,{},t_anio]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'III. EXPERIENCIA DE DOCENCIA DE PREGRADO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [80, 2, 160, 2,'*', 2, 30],
                body: [
                  [{text: 'MATERIA/MÓDULO', style: 'info'},{}, {text: 'FACULTAD/CARRERA', style: 'info'},{}, {text: 'UNIVERSIDAD/INSTITUCIÓN', style: 'info'}, {}, {text: 'AÑO', style: 'info'}],
                  [ d_materia,{},d_facultad,{},d_institucion,{},d_anio]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'IV. EXPERIENCIA DE DOCENCIA E INVESTIGACION EN POSTGRADO'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 2, 70, 2,160, 2, 30],
                body: [
                  [{text: 'MATERIA/MÓDULO', style: 'info'},{}, {text: 'PROGRAMA', style: 'info'},{}, {text: 'UNIVERSIDAD/INSTITUCIÓN', style: 'info'}, {}, {text: 'AÑO', style: 'info'}],
                  [ p_modulo,{},p_programa,{},p_universidad,{},p_anio]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'V. PRODUCCIÓN INTELECTUAL'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [100, 2, '*', 2, 30],
                body: [
                  [{text: 'TIPO', style: 'info'},{}, {text: 'TÍTULO', style: 'info'},{}, {text: 'AÑO', style: 'info'}],
                  [ i_tipo,{},i_titulo,{},i_anio]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'VI. TUTOR DE TESIS'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 2, 70, 2,160, 2, 30],
                body: [
                  [{text: 'NOMBRE DE LA TESIS', style: 'info'},{}, {text: 'GRADO', style: 'info'},{}, {text: 'UNIVERSIDAD/INSTITUCIÓN', style: 'info'}, {}, {text: 'AÑO', style: 'info'}],
                  [ tu_tesis,{},tu_grado,{},tu_universidad,{},tu_anio]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'VII. AREAS Y TEMÁTICAS QUE LE INTERESAN PARA DAR CLASES EN EL CIDES'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: '*',
                body: [
                  [{text: 'Áreas y Temáticas',style: 'info'}],
                  [ tematica]
                ] 
              },
              layout: 'noBorders'
            },
            { style: 'subtitulo',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [{text: 'TEMAS DE MAYOR EXPERIENCIA (EN LOS CUALES LE GUSTARIA DAR CLASES)'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: ['*', 2, '*'],
                body: [
                  [{text: 'TEMA GENÉRICO', style: 'info'},{}, {text: 'ESPECÍFICO', style: 'info'}],
                  [ tema, {}, especifico]
                ] 
              },
              layout: 'noBorders'
            },
            
          ],
          styles: {
            header: {
              fontSize: 13,
              bold: true,
              alignment: 'center',
              fillColor: '#c7c9c8',
              margin: [5, 10, 5, 3]
            },
            info: {
              fontSize: 8,
              bold: true,
              fillColor: '#c7c9c8'
            },
            tableDatos: {
              margin: [5, 0, 5, 3], 
              fontSize: 11
            },
            subtitulo: {
              fontSize: 14,
              fillColor: '#c7c9c8',
              bold: true,
              margin: [5, 15, 5, 5]
            },
            infoBlock: {
              fontSize: 8
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function generarPdfSegRequisitos (req, res, next) {
    try {
     const { estadoEst, idPrograma, idVersion, userName} = req.params;
      let datos = await UsuarioService.findAll({
        id_programa: idPrograma,
        id_version: idVersion
      });
      console.log('LOS DATOS SON: ', datos.data);
      if (datos.code === 1) {
        let prog = await ProgramaService.findById(idPrograma);
        let programa = prog.data;
        let detallesPrograma = await DetallesprogramaService.findById(idVersion);
        let version = detallesPrograma.data;
        moment.locale('es');
        let fecha_emision = new Date();
        fecha_emision = moment(fecha_emision).format('LL');
        let user = await UsuarioService.findAll({
          usuario: userName
        });
        let usuarioRep = user.data.rows[0].persona.nombres.substring(0, 1) + user.data.rows[0].persona.primer_apellido.substring(0, 1) + user.data.rows[0].persona.segundo_apellido.substring(0, 1) ;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let usuario = datos.data; // usuario.requisitos
        let nomRequisitos = [];
        let nroReq = 0;
        
        for (let item of usuario.rows[0].requisitos) {
          let req = { text: 'R' + (parseInt(nroReq) + 1) + ': ' + item.nombre}
          nroReq = nroReq + 1;
          nomRequisitos.push([req]);
        } 
        // [{"id":"111","nombre":"CARTA DE SOLICITUD DE ADMISION","value":true,"observacion":""},{"id":"112","nombre":"PROPUESTA DE TRABAJO DE TESIS 1 A 2 PAGINAS","value":true,"observacion":""},{"id":"113","nombre":"PROYECTO DE INVESTIGACION ENCUADRADO EN LAS LINEAS DE INVESTIGACION (MAXIMO 15 HOJAS)","value":true,"observacion":""},{"id":"114","nombre":"APROBACION PROYECTO DE INVESTIGACION POR COMITE ACADEMICO DE DOCTORADO","value":true,"observacion":""},{"id":"115","nombre":"CURRICULUM VITAE (NO DOCUMENTADO)","value":true,"observacion":""}]
        let headers =[ {text: 'Nro.', style: 'titulo' }, {text: 'C.I.', style: 'titulo' },{text: 'PATERNO', style: 'titulo' }, {text: 'MATERNO', style: 'titulo' }, {text: 'NOMBRES', style: 'titulo' }];
        console.log()
        if (usuario.rows[0].requisitos) {
          const requisitos = JSON.parse(JSON.stringify(usuario.rows[0].requisitos));
          for (let i in requisitos) {
            headers.push({ text: 'R' + (parseInt(i) + 1), style: 'titulo' });
          }
        }
        console.log('headers: ', headers);
        let filas = [headers];
        console.log('filas inicial ', filas);
        let count = 0;
        let countCompletos = 0;
        let countIncompletos = 0;
        for ( let est of usuario.rows ) {
          let aux = [];
          let flag = true;
          console.log('req: ', req);
          count = count + 1;
          est.nro = count; 
          est.requisitos = JSON.parse(JSON.stringify(est.requisitos));
          aux.push(
            est.nro,
            est.persona.nro_documento,
            est.persona.primer_apellido,
            est.persona.segundo_apellido,
            est.persona.nombres
          );
          for (let item of est.requisitos) {
            if(item.value){
              item.value = 'SI';
            } else {
              item.value = 'NO',
              flag = false
            }
            aux.push(item.value);
          }
          if(flag) {
            countCompletos = countCompletos + 1;
          }
          console.log('prueba: ', aux);
          filas = filas.concat([aux]);
        }
        countIncompletos = usuario.count - countCompletos;
        console.log('count: ', usuario.length);
        console.log('datos para acta de notas3:', filas);
        let data = {
            footer: function(currentPage, pageCount) {
              return {
                  margin:10,
                  columns: [
                  {
                      fontSize: 9,
                      text:[
                      {
                      text: '--------------------------------------------------------------------------' +
                      '\n',
                      margin: [0, 20]
                      },
                      {
                      text: currentPage.toString() + ' de ' + pageCount,
                      }
                      ],
                      alignment: 'center'
                  }
                  ]
              };      
          },
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 80, y: 65}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 70}
            },
            { text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n SEGUIMIENTO DE REQUISITOS\n\n', style: 'header', alignment: 'center' },
            {
              margin: [50, 2, 5, 0],
              table: {
                widths: [ 80, '*' ],
                body: [
                  [{text: 'PROGRAMA:', style: 'datos' }, {text: programa.nombre, style: 'datos'}],
                  [{text: 'VERSIÓN:', style: 'datos' }, {text: version.version, style: 'datos'}],
                  [{text: 'GESTIÓN:', style: 'datos'}, {text: version.gestion, style: 'datos'}],
                  [{text: 'COORDINADOR:', style: 'datos' }, {text: version.coordinador, style: 'datos'}],
                  [{text: 'FECHA DE EMISIÓN:', style: 'datos'}, {text: fecha_emision, style: 'datos'}]
                ]
              },
              layout:'noBorders'
            },
            {
              margin: [50,10,20,5],
              style: 'infoEstudiantes',
              table: {
              headerRows: 1,
              widths: ['*'],
              body: nomRequisitos
              },
              
            },
            {
              style: 'infoEstudiantes1',
              table: {
              headerRows: 1,
              // widths: [15, 50, 70, 70, 80, 100],
              body: filas
              }
            },
            {
              style: 'infoRequisitos',
              table: {
              headerRows: 1,
              widths: [150, 50],
              body: [
                  [{text: 'Estudiantes con requisitos completos'}, {text: countCompletos }],
                  [{text: 'Estudiantes con requisitos pendientes'}, {text: countIncompletos }]
                ]
              }
            },
            {
            text: `CIDES*${usuarioRep}*${fechaRep}`, style: 'rev'
            }
          ],
          styles: {
            titulo: {
              bold: true,
              alignment: 'center',
              fontSize: 7,
              fillColor: '#cccccc'
            },
            header: {
              fontSize: 15,
              bold: true,
              alignment: 'center',
              margin: [0, 50, 0, 20]
              //  decoration: 'underline'
            },
            infoRequisitos: {
              fontSize: 7,
              margin: [50, 0, 0, 0]
            },
            infoEstudiantes: {
              fontSize: 7,
              margin: [50, 0, 10, 10] 
            },
            infoEstudiantes1: {
              fontSize: 7,
              margin: [50, 0, 0, 1] 
            },
            datos: {
              fontSize: 8,
              bold: true
            },
            rev: {
              fontSize: 7, 
              bold: true, 
              margin: [50, 0, 20, 70],
              italics: true
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }
  async function uploadFoto (req, res, next) {
    debug('Subiendo foto para usuario');

    let user = await userData(req, services);

    if (!req.files) {
      return next(new Error('Debe enviar foto'));
    }

    const { foto } = req.files;
    const { idUsuario } = req.body;
    try {
      let result = await UsuarioService.uploadFoto(foto, user.id, idUsuario);
      if (result.code === -1) {
        return next(new Error(result.message));
      }
      if (result.data) {
        res.send(result.data);
      } else {
        return next(new Error('No se pudo subir el documento'));
      }
    } catch (e) {
      return next(e);
    }
  }

  return {
    uploadFoto,
    personaSegip,
    cambiarPass,
    desactivarCuenta,
    obtenerMenu,
    regenerarPassword,
    generarPdfEntrevistas,
    generarPdfCartaAceptacion,
    generarPdfFormularioInscripcion,
    generarPdfRequisitos,
    generarPdfDocRequisitos,
    generarPdfKardexDocente,
    generarPdfSegRequisitos
  };
};
