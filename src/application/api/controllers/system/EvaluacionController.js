'use strict';

const moment = require('moment');
const { crearPdf } = require('../../../lib/util');
// const { NumerosaLetras } = require('../../../lib/NumeroALetras');
module.exports = function setupRecordController (services) {
  const { EvaluacionService } = services;

  const styles = {
    titulo: {
      bold: true,
      alignment: 'center',
      fontSize: 7,
      fillColor: '#cccccc'
    },
    header: {
      fontSize: 15,
      bold: true,
      alignment: 'center'
    },
    subheader: {
      fontSize: 12,
      bold: true,
      alignment: 'center'
    },
    info: {
      fontSize: 7,
      bold: true
    },
    strong: {
      fontSize: 9,
      bold: true,
      margin: [20, 0, 10, 0]
    },
    infoEstudiantes: {
      fontSize: 7,
      margin: [20, 10, 10, 10]
    },
    tableDatos: {
      margin: [20, 0, 10, 20]
    },
    infoEscala: {
      fontSize: 7,
      margin: [20, 0, 10, 0],
      alignment: 'center',
      bold: true
    },
    rev: {
      fontSize: 7,
      bold: true,
      margin: [60, 5, 5, 5],
      italics: true
    }
  };

  async function evaluacionDocente (req, res, next) {
    try {
      const { idEvaluacion } = req.params;
      const result = await EvaluacionService.findById(idEvaluacion);
      if (result.code === 1) {
        const evaluacion = result.data.evaluacion;
        const preguntas = [
          [
            { text: '#', style: 'titulo' },
            { text: 'Descripción', style: 'titulo' },
            { text: '1', style: 'titulo' },
            { text: '2', style: 'titulo' },
            { text: '3', style: 'titulo' },
            { text: '4', style: 'titulo' },
            { text: '5', style: 'titulo' },
            { text: '6', style: 'titulo' },
            { text: '7', style: 'titulo' },
            { text: '8', style: 'titulo' },
            { text: '9', style: 'titulo' },
            { text: '10', style: 'titulo' }
          ]
        ];
        for (const item of evaluacion.preguntas) {
          let opciones = [];
          if (item.opcion === 'SI' || item.opcion === 'NO') {
            opciones = [item.opcion, '-', '-', '-', '-', '-', '-', '-', '-', '-'];
          } else {
            opciones = Array(10).fill().map((_, i) => (item.opciones[i] && item.opciones[i].value && item.opciones[i].value === item.opcion ? 'X' : ''));
          }
          preguntas.push([
            item.numero,
            item.descripcion,
            ...opciones
          ]);
        }
        let fecha = new Date();
        moment.locale('es');
        fecha = moment(fecha).format('LL');
        const data = {
          content: [
            { text: 'EVALUACION DOCENTE', style: 'header', margin: [0, 100, 0, 0] },
            { text: 'Docente: ' + result.data.docente.persona.nombre_completo, style: 'subheader' },
            { text: evaluacion.descripcion, margin: [20, 20, 20, 20], fontSize: 10 },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [15, 250, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                body: preguntas
              }
            },
            { text: 'Comentarios y Sugerencias(concretas)', style: 'subheader', margin: [20, 20, 20, 20] },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [15, '*'],
                body: evaluacion.sugerencias.map(item => ([item.numero, item.value]))
              }
            },
            { text: 'La Paz, ' + fecha, fontSize: 8, alignment: 'right', margin: [20, 20, 20, 20] }
          ],
          styles
        };
        const pdf = await crearPdf(data);
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
    } catch (e) {
      console.log('ERROR', e);
      return next(e);
    }
  }

  async function evaluacionAlumno (req, res, next) {
    try {
      const { idEvaluacion } = req.params;
      const result = await EvaluacionService.findById(idEvaluacion);
      if (result.code === 1) {
        const evaluacion = result.data.evaluacion;
        const actividades = [
          [
            { text: '#', style: 'titulo' },
            { text: 'Descripción', style: 'titulo' },
            { text: '1', style: 'titulo' },
            { text: '2', style: 'titulo' },
            { text: '3', style: 'titulo' },
            { text: '4', style: 'titulo' },
            { text: '5', style: 'titulo' }
          ]
        ];
        for (const item of evaluacion.actividades) {
          actividades.push([
            item.numero,
            item.descripcion,
            ...Array(5).fill().map((_, i) => (item.opciones[i] && item.opciones[i].value && item.opciones[i].value === item.opcion ? 'X' : ''))
          ]);
        }
        const satisfaccion = [
          [
            { text: '#', style: 'titulo' },
            { text: 'Descripción', style: 'titulo' },
            { text: '1', style: 'titulo' },
            { text: '2', style: 'titulo' },
            { text: '3', style: 'titulo' },
            { text: '4', style: 'titulo' },
            { text: '5', style: 'titulo' }
          ]
        ];
        for (const item of evaluacion.satisfaccion) {
          satisfaccion.push([
            item.numero,
            item.descripcion,
            ...Array(5).fill().map((_, i) => (item.opciones[i] && item.opciones[i].value && item.opciones[i].value === item.opcion ? 'X' : ''))
          ]);
        }
        let fecha = new Date();
        moment.locale('es');
        fecha = moment(fecha).format('LL');
        const data = {
          content: [
            { text: 'EVALUACION ALUMNO', style: 'header', margin: [0, 50, 0, 0] },
            { text: 'Docente: ' + result.data.docente.persona.nombre_completo, style: 'subheader', margin: [0, 0, 0, 10] },
            { text: 'Por favor responder a todas las preguntas utilizando la siguiente escala:', fontSize: 10 },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [15, 150],
                body: [[{ text: '#', style: 'titulo' }, { text: 'Descripción', style: 'titulo' }], ...evaluacion.puntajes.map(item => ([item.numero, item.descripcion]))]
              }
            },
            { text: 'Este formulario es anónimo pero requiere de su seriedad y aporte.', fontSize: 10 },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [15, 350, 10, 10, 10, 10, 10],
                body: actividades
              }
            },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [15, 350, 10, 10, 10, 10, 10],
                body: satisfaccion
              }
            },
            { text: `Tomaría otra clase con el profesor:       SI ${evaluacion.preferencia === 'SI' ? 'X' : ''}        NO ${evaluacion.preferencia === 'NO' ? 'X' : ''}`, fontSize: 10 },
            {
              style: 'infoEstudiantes',
              table: {
                headerRows: 1,
                widths: [150, 150, 150],
                body: [
                  [
                    { text: 'CURSO', style: 'titulo' },
                    { text: 'Lo más positivo', style: 'titulo' },
                    { text: 'Lo que se debe mejorar', style: 'titulo' }
                  ],
                  [
                    evaluacion.evaluacion.docente,
                    evaluacion.evaluacion.positivo,
                    evaluacion.evaluacion.negativo
                  ]
                ]
              }
            },
            { text: 'La Paz, ' + fecha, fontSize: 8, alignment: 'right', margin: [20, 20, 20, 20] }
          ],
          styles
        };
        const pdf = await crearPdf(data);
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
    } catch (e) {
      console.log('ERROR', e);
      return next(e);
    }
  }

  async function consolidado (req, res, next) {
    try {
      const { actividades, satisfaccion, preferencia, evaluacion, programa, docente, modulo } = req.body;
      const actividadesArray = [
        [
          { text: ' ', style: 'titulo' },
          { text: '1', style: 'titulo' },
          { text: '2', style: 'titulo' },
          { text: '3', style: 'titulo' },
          { text: '4', style: 'titulo' },
          { text: '5', style: 'titulo' },
          { text: 'Alumnos', style: 'titulo' },
          { text: 'Puntaje', style: 'titulo' }
        ],
        [
          { text: 'Pregunta', style: 'titulo' },
          { text: '60%', style: 'titulo' },
          { text: '70%', style: 'titulo' },
          { text: '80%', style: 'titulo' },
          { text: '90%', style: 'titulo' },
          { text: '100%', style: 'titulo' },
          { text: ' ', style: 'titulo' },
          { text: '%', style: 'titulo' }
        ]
      ];
      for (const item of actividades.items) {
        actividadesArray.push([
          item.numero,
          item.pregunta1,
          item.pregunta2,
          item.pregunta3,
          item.pregunta4,
          item.pregunta5,
          item.alumnos,
          item.puntaje
        ]);
      }
      const satisfaccionArray = [
        [
          { text: ' ', style: 'titulo' },
          { text: '1', style: 'titulo' },
          { text: '2', style: 'titulo' },
          { text: '3', style: 'titulo' },
          { text: '4', style: 'titulo' },
          { text: '5', style: 'titulo' },
          { text: 'Alumnos', style: 'titulo' },
          { text: 'Puntaje', style: 'titulo' }
        ],
        [
          { text: 'Pregunta', style: 'titulo' },
          { text: '60%', style: 'titulo' },
          { text: '70%', style: 'titulo' },
          { text: '80%', style: 'titulo' },
          { text: '90%', style: 'titulo' },
          { text: '100%', style: 'titulo' },
          { text: ' ', style: 'titulo' },
          { text: '%', style: 'titulo' }
        ]
      ];
      for (const item of satisfaccion.items) {
        satisfaccionArray.push([
          item.numero,
          item.pregunta1,
          item.pregunta2,
          item.pregunta3,
          item.pregunta4,
          item.pregunta5,
          item.alumnos,
          item.puntaje
        ]);
      }
      let fecha = new Date();
      moment.locale('es');
      fecha = moment(fecha).format('LL');
      const data = {
        content: [
          { text: 'CONSOLIDADO', style: 'header', margin: [0, 50, 0, 0] },
          { text: `PROGRAMA: ${programa}`, style: 'subheader' },
          { text: `MODULO: ${modulo}`, style: 'subheader' },
          { text: `DOCENTE: ${docente}`, style: 'subheader' },
          { text: 'I. ACTIVIDADES ACADEMICAS', style: 'subheader' },
          {
            style: 'infoEstudiantes',
            table: {
              headerRows: 1,
              widths: [50, 50, 50, 50, 50, 50, 50, 50],
              body: actividadesArray
            }
          },
          { text: `PROMEDIO FINAL: ${actividades.total}%`, fontSize: 10, alignment: 'right', bold: true, margin: [0, 0, 20, 0] },
          { text: 'II. SATISFACCION DEL ALUMNO', style: 'subheader' },
          {
            style: 'infoEstudiantes',
            table: {
              headerRows: 1,
              widths: [50, 50, 50, 50, 50, 50, 50, 50],
              body: satisfaccionArray
            }
          },
          { text: `PROMEDIO FINAL: ${satisfaccion.total}%`, fontSize: 10, alignment: 'right', bold: true, margin: [0, 0, 20, 20] },
          { text: `PROMEDIO GENERAL DEL DOCENTE: ${(parseInt(actividades.total) + parseInt(satisfaccion.total)) / 2}%`, fontSize: 10, alignment: 'right', bold: true, margin: [0, 0, 20, 0] },
          { text: `Tomaría otra clase con el profesor:       SI (${preferencia.si || 0})        NO (${preferencia.no || 0})`, fontSize: 10, margin: [20, 20, 20, 20] },
          {
            style: 'infoEstudiantes',
            table: {
              headerRows: 1,
              widths: [150, 150, 150],
              body: [
                [
                  { text: 'CURSO', style: 'titulo' },
                  { text: 'Lo más positivo', style: 'titulo' },
                  { text: 'Lo que se debe mejorar', style: 'titulo' }
                ],
                [
                  evaluacion.docente,
                  evaluacion.positivo,
                  evaluacion.negativo
                ]
              ]
            }
          },
          { text: 'La Paz, ' + fecha, fontSize: 8, alignment: 'right', margin: [20, 20, 20, 20] }
        ],
        styles
      };
      const pdf = await crearPdf(data);
      res.contentType('application/pdf');
      res.send(pdf);
    } catch (e) {
      console.log('ERROR', e);
      return next(e);
    }
  }

  return {
    evaluacionDocente,
    evaluacionAlumno,
    consolidado
  };
};
