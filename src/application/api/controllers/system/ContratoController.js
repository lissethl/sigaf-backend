'use strict';

const debug = require('debug')('app:controller:usuario');
const moment = require('moment');
const path = require('path');
const numeral = require('numeral');
const { userData, generateToken } = require('../../../lib/auth');
const { crearPdf } = require('../../../lib/util');
const soapRequest = require('easy-soap-request');
const fs = require('fs');
const parseString = require('xml2js').parseString;
const { nano } = require('../../../../common').text;

const url = 'http://200.7.160.39:8080/axis/SiaWS/cidesWS.jws';
const sampleHeaders = {
  'user-agent': 'sampleTest',
  'Content-Type': 'text/xml;charset=UTF-8',
  'soapAction': ''
};
// const { NumerosaLetras } = require('../../../lib/NumeroALetras');
module.exports = function setupContratoController (services) {
  const { Iop, ContratoService, UsuarioService, ModuloService, Parametro,ProgramaService, DetallesprogramaService, DatosgeneralService, ConstanteService, PagoService } = services;
  
  async function pdfCotizacion(req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const {idContrato} = req.params;
      let datos = await ContratoService.findById(idContrato);
      console.log('Los datos de contrato son: ', datos);
      if (datos.code === 1) {
        let persona = datos.data.persona;
        let contrato = datos.data;
        let programa = datos.data.programa;
        let detallesp = datos.data.detallesprograma;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} (${detallesp.version} VERSIÓN) `;
        let fecha_emision = new Date();
        let primeraCuota = parseInt(contrato.monto_reserva) + parseInt(contrato.cuota_inicial);
        //  let saldoCuotas = parseInt(contrato.costo_colegiatura) - parseInt(primeraCuota);
        let pReserva = 0;
        if (contrato.monto_reserva) {
          pReserva = (contrato.monto_reserva * 100) / contrato.costo_colegiatura;
        }  
        let plan_pagos = JSON.parse(JSON.stringify(contrato.plan_pagos));
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        console.log('plan_pagos', plan_pagos);
        //let headers =[ {text: 'GESTIÓN', style: 'tableCuotas'}, {text: 'NRO. CUOTA', style: 'tableCuotas'},{text: 'CONCEPTO', style: 'tableCuotas'}, {text: 'MONTO', style: 'tableCuotas'}, {text: 'SALDO', style: 'tableCuotas'}, {text: 'FECHA', style: 'tableCuotas'}];
        let headers =[ {text: 'GESTIÓN', style: 'tableCuotas'}, {text: 'NRO. CUOTA', style: 'tableCuotas'},{text: 'CONCEPTO', style: 'tableCuotas'}, {text: 'MONTO', style: 'tableCuotas'}, {text: 'FECHA', style: 'tableCuotas'}];
        let filas = [headers];
        for (let item of plan_pagos ) {
         //item.saldo = { text: item.saldo + '.00', style: 'numberCuota'};           
         item.monto = { text: numberFormat(item.monto) , style: 'numberCuota'}; 
          filas.push([
            item.gestion, 
            item.numero, 
            item.concepto, 
            item.monto,
            //  item.saldo,
            item.fecha
          ]);
         console.log('ITEM', item);
        }
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:10,
                columns: [
                {
                    fontSize: 9,
                    text:[
                    {
                    text: '--------------------------------------------------------------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };
          },   
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 80, y: 65}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 70}
            },  
          { text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS       \n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n PLAN DE PAGOS', style: 'header'},
          {
            style: 'tableDatos',
            table: {
              headerRows: 1,
              widths: [125, 'auto'],
              body: [
                [ { text: 'Nombre Completo:', style: 'info' }, {text: persona.nombre_completo, fontSize: 8}],
                [{ text: 'Nro. de Documento:', style: 'info' }, {text: persona.nro_documento + ' ' + persona.lugar_exp, fontSize: 8}],
                [{ text: 'Programa:', style: 'info' }, {text: nomPrograma, fontSize: 8}],
                [{text: 'Forma de Pago:', style: 'info'}, {text: contrato.opcion_pago, fontSize: 8} ],
                [{text: 'Descuento % (Si corresponde):', style: 'info'}, {text: contrato.descuento + '%', fontSize: 8} ],
                [{text: 'Costo Matricula (Por gestión) Bs.:', style: 'info'}, {text: numberFormat(contrato.monto_matricula_1), fontSize: 8} ],
                [{ text: 'Fecha de Emisión:', style: 'info'}, {text: fecha_emision, fontSize: 8}]
              ]
            },
            layout: 'noBorders'
          },
          {
            style: 'tableParametros',
            table: {
              headerRows: 1,
              widths: [100, 1, 150, 1, '*'],
              body: [
                [ 

                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Costo Real Colegiatura Bs.', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Descuento por Forma de Pago Bs.', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Costo Colegiatura Aplicada', style: 'tableHeader'
                  }
                ],
                [ {text: numberFormat(contrato.costo_colegiatura_total), fontSize: 8, alignment: 'right' }, {}, {text: numberFormat(contrato.monto_descuento), fontSize: 8, alignment: 'right'},{}, {text:numberFormat(contrato.costo_colegiatura), fontSize: 8, alignment:'right'}]  
              ] 
            },
            layout: 'noBorders'
          },
          {
            style: 'tableParametros2',
            table: {
              headerRows: 1,
              widths: [ 60, 1, 100, 1, 120, 1, '*'],
              body: [ 
                [ 
                  { border: [false, false, false, false],
                  fillColor: '#d0eae9',
                  text: 'Cuota Nro. 101', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Cuota Inicial (Reserva) Bs.', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Porcentaje Cuota Inicial(%)', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Saldo Cuota Nro. 101', style: 'tableHeader'
                  } 
                ],
                [ {text: numberFormat(primeraCuota), fontSize: 8, alignment:'right'}, {}, {text: numberFormat(contrato.monto_reserva),fontSize: 8, alignment:'right'}, {}, {text: parseFloat(pReserva).toFixed(2) + '%', fontSize: 8 }, {}, {text: numberFormat(contrato.cuota_inicial),fontSize: 8, alignment:'right'}]
              ] 
            },
            layout: 'noBorders'
          },
          /*{
            style: 'tableParametros2',
            table: {
              headerRows: 1,
              widths: [90, 0.1, 90, 0.1, 70 , 0.1,  '*'],
              body: [
                [ 
                  { border: [false, false, false, false],
                  fillColor: '#d0eae9',
                  text: 'Saldo demas cuotas Bs.', style: 'tableHeader'
                  },
                  {},
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Costo MatrÍcula ', style: 'tableHeader'
                  },
                  {}, 
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Nro. Cuotas', style: 'tableHeader'
                  },
                  {}, 
                  { border: [false, false, false, false],
                    fillColor: '#d0eae9',
                    text: 'Monto a Pagar en Cuotas Bs.', style: 'tableHeader'
                  }
                ],
                [{text: numberFormat(saldoCuotas), fontSize: 8, alignment: 'right' }, {}, {text: numberFormat(contrato.monto_matricula_1), fontSize: 8, alignment:'right'}, {}, {text: contrato.nro_cuotas, fontSize: 8 }, {},  {text: numberFormat(contrato.monto_cuotas), fontSize: 8, alignment:'right'}]
              ] 
            },
            layout: 'noBorders'
          }, */
          {
            style: 'infoCuotas',
            table: {
              headerRows: 1,
              widths: [50, 50, 100, 70, '*'],
              body: filas
            },
            // layout: 'noBorders'
          },
          ],
          styles: {
            header: {
              fontSize: 15, 
              bold: true,
              alignment: 'center',
              margin: [0, 50, 0, 20]
            },
            info: {
              fontSize: 8,
              bold: true
            },
            tableDatos: {
              margin: [50, 30, 20, 20]
            },
            tableHeader: {
              fontSize: 8,
              bold: true,
              alignment: 'center'
            },
            tableParametros: {
              fontSize: 10,
              margin: [50, 0, 50, 0]
            },
            tableParametros2: {
              fontSize: 10,
              margin: [50, 0, 50, 20]
            },
            tableCuotas: {
              fontSize: 8,
              alignment: 'center',
              bold: true
            },
            infoCuotas: {
              fontSize: 10,
              alignment: 'center',
              margin: [50, 0, 50, 20]
            },
            numberCuota: {
              alignment: 'right',
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function pdfReciboReserva(req, res, next) {
    try {
      const {idUsuario, userName} = req.params;
      let datosUsuario = await UsuarioService.findById(idUsuario);
      let datos = await ContratoService.findAll({
        id_usuario: datosUsuario.data.id
      });
        if (datos.code === 1) {
        console.log('ingresa al if');
        let persona = datos.data.rows[0].persona;
        let contrato = datos.data.rows[0];
        let reserva = numberFormat(contrato.monto_reserva) + ' (' + contrato.literal_reserva + ' 00/100 ' + contrato.moneda_reserva + ')';
        let nroRecibo = 'Nº: ' + contrato.nro_recibo;
        console.log('datos del Contrato', contrato );
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let data = {  
          content: [
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 50, 
              height: 60,
              absolutePosition: {x: 76, y: 60}
            },
            {
              text: 'Universidad Mayor de San Andrés \n Postgrado en Ciencias del Desarrollo \n Calle 3 de Obrajes N° 515', margin: [40, 70, 0, 0], fontSize: 8, bold: true
            },
            { text: nroRecibo , alignment: 'right', margin: [0,-45,60,0], bold: true, fontSize: 11 },
            { text: 'ORIGINAL', alignment: 'right', margin: [0,0,60,20], bold: true, fontSize: 9},
            { text: 'RECIBO OFICIAL', style: 'header' },
          {
            style: 'tableDatos',
            table: {
              headerRows: 1,
              widths: [85, '*'],
              body: [
                [ { text: 'Recibimos de: ', style: 'info' }, {text: persona.nombre_completo, style: 'info'}],
                [{ text: 'La suma de: ', style: 'info' }, {text: reserva, style: 'info'}],
                [{ text: 'Por Concepto de: ', style: 'info'}, {text: contrato.concepto_reserva, style: 'info'}],
                [{ text: 'Forma de Pago: ', style: 'info'}, {text: 'EFECTIVO', style: 'info'}]
              ]
            },
            layout: 'lightHorizontalLines'
          },
          {
            text: 'La Paz, ' + fecha_emision, alignment: 'right', margin: [0,-10,60,0], bold: true, fontSize: 9
          },
          {
            text: '\n\n\n\n\n\n\n\n' },
          {
            image: path.join(global.appRoot, 'public/cides.png'),
            width: 50, 
            height: 60,
            absolutePosition: {x: 76, y: 430}
          },
          {
            text: 'Universidad Mayor de San Andrés \n Postgrado en Ciencias del Desarrollo \n Calle 3 de Obrajes N° 515', margin: [40, 70, 0, 0], fontSize: 8, bold: true
          },
          { text: nroRecibo , alignment: 'right', margin: [0,-45,60,0], bold: true, fontSize: 11 },
          { text: 'COPIA', alignment: 'right', margin: [0,0,60,20], bold: true, fontSize: 9},
          { text: 'RECIBO OFICIAL', style: 'header' },
        {
          style: 'tableDatos',
          table: {
            headerRows: 1,
            widths: [85, '*'],
            body: [
              [ { text: 'Recibimos de: ', style: 'info' }, {text: persona.nombre_completo, style: 'info'}],
              [{ text: 'La suma de: ', style: 'info' }, {text: reserva, style: 'info'}],
              [{ text: 'Por Concepto de: ', style: 'info'}, {text: contrato.concepto_reserva, style: 'info'}],
              [{ text: 'Forma de Pago: ', style: 'info'}, {text: 'EFECTIVO', style: 'info'}]
            ]
          },
          layout: 'lightHorizontalLines'
        },
        {
          text: 'La Paz, '+ fecha_emision, alignment: 'right', margin: [0,-10,60,0], bold: true, fontSize: 9
        }
          ],
          styles: {
            header: {
              fontSize: 15, 
              bold: true,
              alignment: 'center'
            },
            info: {
              bold: true,
              margin: [0, 5, 0, 0]
            },
            tableDatos: {
              fontSize: 9,
              margin: [60, 20, 60, 20]
            },
            tableHeader: {
              fontSize: 10,
              bold: true
            },
            tableParametros: {
              margin: [25, 0, 0, 20]
            },
            tableCuotas: {
              fontSize: 10,
              alignment: 'center',
              bold: true
            },
            infoCuotas: {
              margin: [25, 0, 5, 20],
              fontSize: 10,
              alignment: 'center'
            },
            numberCuota: {
              alignment: 'right',
            }
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }
  async function pdfContrato(req, res, next) {
    try {
      const { idUsuario} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      
      if (datos.code === 1) {
        let usuario = datos.data;
        let persona = usuario.persona;
        let programa = usuario.programa;
        let detallesp = usuario.detallesprograma;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} (${detallesp.version} VERSIÓN) `;
        let constante = await ConstanteService.findAll({ 
          codigo: programa.horarios 
        });
        let datosGenerales = await DatosgeneralService.findAll({ 
          gestion: detallesp.gestion 
        });
        //  console.log('horarios: ', constante.data.rows[0].nombre);
        let años = 0;
        if (programa.tipo === 'MAESTRIA') {
          años = 2;
        } else {
          años = 3;
        }

        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let data = { 
          footer: function(currentPage, pageCount) {
            return {
                margin:[10,0,10,50],
                columns: [ 
                {  
                  fontSize: 9,
                    text:[
                    {
                    text: '----------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };
          },
          //  pageSize: 'OFFICE', 
          
            // a string or { width: number, height: number }
            pageSize: 'LETTER',
          
            // by default we use portrait, you can change it to landscape if you wish
            pageOrientation: 'portrait',
          
            // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
            pageMargins: [ 60, 130, 60, 100 ],
          

          content: [
            { text: 'CONTRATO DE ESTUDIOS\n DEL PROGRAMA ACADÉMICO \n ' + '"' +programa.nombre +'"\n  VERSIÓN: '+ detallesp.version + ' (' + detallesp.periodo + ')', style: 'header'},
            {
              stack: [
                { text: ['Conste por el presente Contrato Privado de Estudios suscrito entre: el Postgrado en Ciencias del Desarrollo CIDES/UMSA,', 
                      ' representado por su Director/a ',{text: datosGenerales.data.rows[0].nom_director, style: 'info'} ,', que en adelante  se denominará solamente EL CIDES, por una parte,',
                      ' y por la otra el/la señor/a ' ,{text: persona.nombre_completo, style: 'info'}, ' con C.I.', {text: persona.nro_documento, style: 'info'} ,' que en adelante se denominará EL/LA POSTGRADUANTE,',
                      ' contrato que se suscribe en el marco de los Arts.: 519, 520 y 523 del Código Civil en vigencia, y que se regirá bajo las siguientes cláusulas:\n\n'
                      ]
                },
                {
                  text: 'PRIMERA: (De las Obligaciones académicas).\n\n', style: 'subtitle'
                },
                {
                  text: ['El/La POSTGRADUANTE se compromete y obliga a cumplir con todas las disposiciones académicas establecidas por el POSTGRADO',
                  ' EN CIENCIAS DEL DESARROLLO en su Reglamento General, cuyas disposiciones pertinentes se adjuntan. También deberá sujetarse a las siguientes normas:\n\n']
                },
                {
                  text: 'a)	Dedicación: \n\n', style: 'subtitle'
                },
                {
                  text: ['La asistencia a los cursos de ' +programa.nombre +' es obligatoria, el/la POSTGRADUANTE deberá tener una asistencia mínima del 80% para aprobar los cursos. Asimismo, para efectos',
                  ' de control el/a Postgraduante firmará las LISTAS DE ASISTENCIA los días de clases, antes del inicio de las sesiones académicas correspondientes.\n\n']
                },
                {
                  text: 'b)	Asistencia y Licencias: \n\n', style: 'subtitle'
                },
                {
                  text: ['Los/as estudiantes tienen derecho a 2 faltas sin solicitud de licencia, y excepcionalmente a 2 faltas adicionales con solicitud escrita y documentada.',
                  ' Esta justificación escrita deberá presentarse con anticipación a la falta o máximo una semana después de la misma.\n\n']
                },
                {
                  text: ['En caso que el/la alumno/a NO pueda asistir a un módulo completo deberá solicitar licencia por el módulo mediante carta dirigida al Coordinador/a del Programa',
                  ' con una semana de anticipación o máximo en el curso de la primera semana de iniciado el módulo.\n\n']
                },
                {
                  text: 'c)	Horario: \n\n', style: 'subtitle'
                },
                {
                  text: ['El programa ', {text: programa.nombre, style: 'info'}, ' pasa clases los dias:\n\n', {text: constante.data.rows[0].nombre, style: 'info', alignment: 'center'}, '\n\n']
                },
                {
                  text: ['El CIDES otorga una tolerancia máxima de 15 minutos  para que el alumno pueda ingresar al aula, pasado este tiempo, el alumno debe esperar al descanso.',
                  ' Este atraso se considera en las listas como una falta.\n\n']
                },
                {
                  text: 'd)	Evaluación: \n\n', style: 'subtitle'
                },
                {
                  text: ['En cada módulo, el docente respectivo realiza una evaluación, tomando como parámetros cualesquiera de las siguientes formas:\n\n']
                },
                {
                  text: [
                    '-	Control de Lectura\n',
                    '-	Exámenes parciales\n',
                    '-	Exposiciones en  grupo\n',
                    '-	Seminarios Talleres\n',
                    '-	Ensayo Individual\n\n'
                  ]
                },
                {
                  text: ['La selección de estos parámetros y su ponderación es decisión de cada docente. Esa selección de los parámetros y su ponderación,',
                  ' se encuentra en  el programa analítico del módulo que es entregado a los  alumnos al inicio de  los mismos.\n\n']
                },
                {
                  text: 'e)	Reprobación, repetición del Módulo: \n\n', style: 'subtitle'
                },
                {
                  text: ['EL/LA POSTGRADUANTE deberá necesariamente aprobar todos los módulos con una nota mínima de 70 sobre 100.  En caso que repruebe uno',
                  ' o  dos módulos, podrá repetirlos en la próxima versión del programa, previo pago adicional',
                  ' del costo del módulo a ser determinado por el CIDES.\n La recuperación se podrá hacer bajo la modalidad de tutoría cuando el/la estudiante haya concluido el programa y realizado',
                  ' satisfactoriamente los talleres de tesis y se encuentre con el borrador de tesis y la carta del tutor aprobando la misma, listo para su revisión del tribunal.\n\n']
                },
                {
                  text: ['La reprobación de un tercer módulo, según los Reglamentos del CIDES, representa el retiro del/la estudiante del Programa. En casos excepcionales',
                  ' un Comité Académico  podrá reconsiderar el retiro correspondiente.\n\n']
                },
                {
                  text: 'f) Del plagio en los trabajos académicos:\n\n', style: 'subtitle'
                },
                {
                  text: ['El/la estudiante debe tomar en cuenta que cualquier señal de plagio en los trabajos realizados para evaluación de módulo y otros trabajos',
                  ' académicos, será motivo de sanción, según la gravedad de los casos  y de acuerdo a las disposiciones normativas del CIDES (ver', {text: ' La ética en los textos académicos, ', style: 'italics'},
                  'CIDES-UMSA, Serie Universidad, Año 1, Nro. 1).\n\n']
                }, 
                {
                  text: 'g) De la finalización de la gestión:\n\n', style: 'subtitle'
                },
                {
                  text: ['Una gestión académica concluye ', {text: años }, ' años después de la finalización del último Taller de Tesis.  Si en ese plazo el/la estudiante no ha aprobado todas',
                  ' las asignaturas de su programa académico, incluida la defensa de tesis, deberá cursar y volver a pagar los módulos pendientes, lo mismo que los talleres',
                  ' de tesis, incluso si éstos ya fueron cursados, con el fin de actualizar su trabajo de investigación de tesis. Un Comité Académico evaluará las condiciones',
                  ' de esta reincorporación.\n\n']
                },
                {
                  text: 'SEGUNDA: (De la Obligación del CIDES).\n\n', style: 'subtitle'
                },
                {
                  text: ['El CIDES, por su parte, se compromete a proporcionar a el/la POSTGRADUANTE, al inicio de cada módulo, el programa analítico y su bibliografía; así',
                  ' como un dossier bibliográfico con el conjunto de las lecturas obligatorias. Este dossier está igualmente disponible en la biblioteca del CIDES.\n\n']
                },
                {
                  text: 'TERCERA: (Conformidad)\n\n', style: 'subtitle'
                },
                {
                  text: ['Estando de acuerdo ambas partes con todas las cláusulas establecidas en el presente Contrato Privado de Estudios, firmamos y suscribimos el mismo en señal',
                  ' de conformidad y para los fines consiguientes.\n\n\n\n\n']
                },
                { text: 'La Paz, ' + fecha_emision + '\n\n\n\n\n\n\n\n', alignment: 'center' },
                {
                  style: 'tableFirmas',
                  table: {
                    headerRows: 1,
                    widths: ['*',25,'*'],
                    body: [
                      [{text: datosGenerales.data.rows[0].nom_director, style: 'firma',  border: [false,true,false,false]}, {text:'', border:[false,false,false,false]}, {text: persona.nombre_completo, style: 'firma',border: [false,true,false,false]}],
                      [{text: 'DIRECTOR/A CIDES-UMSA', style: 'firma',border: [false,false,false,false]}, {text:'', border:[false,false,false,false]}, {text: 'POSTGRADUANTE', style: 'firma',border: [false,false,false,false]}]
                    ]
                  }
                }
              ],
              style: 'superMargin'
            }
          ],
          styles: {
            header: {
              fontSize: 15, 
              bold: true,
              alignment: 'center',
              margin: [0, 0, 0, 20]
            },
            superMargin: {
              margin: [30, 10, 30, 10],
              alignment: 'justify',
              fontSize: 10
		        },
            info: {
              bold: true
            },
            subtitle: {
              fontSize: 14,
              bold: true
            },
            firma: {
              fontSize: 10,
              italics: true,
              alignment: 'center',
              bold: true,
              //margin: [0, 0, 0, 0],
            },
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function pdfContratoPago(req, res, next) {
    try {
      const { idUsuario} = req.params;
      let datos = await UsuarioService.findById(idUsuario);
      if (datos.code === 1) {
        let usuario = datos.data;
        let persona = usuario.persona;
        let programa = usuario.programa;
        let detallesp = usuario.detallesprograma;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} (${detallesp.version} VERSIÓN) `;
        let contrato = await ContratoService.findAll({ 
          id: idUsuario 
        });
        let datosGenerales = await DatosgeneralService.findAll({ 
          gestion: detallesp.gestion 
        });
        console.log('datos del contrato: ', contrato.data.rows[0].costo_colegiatura);
        let costoColegiatura = contrato.data.rows[0].costo_colegiatura
        let fecha_emision = new Date();
        moment.locale('es');
        //let aux = '{text: programanombre, style: 'info'}';
        fecha_emision = moment(fecha_emision).format('LL');
        let formatNum = numberFormat(costoColegiatura);
        console.log('numeral', formatNum); 
        let montoLiteral = parseInt(costoColegiatura);
        costoColegiatura = costoColegiatura+'';
        const amountInWords = NumerosaLetras(23142);
        console.log('PRUEA DE NUMERO!!!', amountInWords);  
        let fechaLiteral = dateFormat();
        let años = 0;
        if (programa.tipo === 'MAESTRIA') {
          años = 2;
        } else {
          años = 3;
        }
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:[10,0,10,50],
                columns: [ 
                {  
                  fontSize: 9,
                    text:[
                    {
                    text: '----------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };
          }, 
          pageSize: 'LETTER',
          
            // by default we use portrait, you can change it to landscape if you wish
            pageOrientation: 'portrait',
          
            // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
            pageMargins: [ 60, 130, 60, 100 ],

          content: [
            { text: 'CONTRATO DE PAGOS DEL COSTO DE LA COLEGIATURA\n DEL PROGRAMA ACADEMICO \n ' + '"' +programa.nombre +'"', style: 'header'},
            {
              stack: [
                { text: ['En cumplimiento de las disposiciones legales en actual vigencia, la Universidad Mayor de San Andrés ha resulto celebrar el presente contrato',
                      ' de Servicios con valor de documento privado al amparo de lo dispuesto en la ', {text: `Resolución del Honorable Consejo Universitario N° ${programa.rhcu}`, style: 'info'} , 
                      ' donde se define el marco normativo y procedimientos para regular la Gestión Tributaria de los Postgrados, en conformidad a la ', {text: 'Ley 843 articulo 4 Inc. b)', style: 'info'},
                      ' que faculta realizar la suscripción de Compromisos de Pago por los servicios.\n\n',
                      'El Director de Postgrado en Ciencias del Desarrollo (CIDES/UMSA) ',{text: datosGenerales.data.rows[0].nom_director, style: 'info'} ,
                      ' y el/la Sr/a. ' ,{text: `Sr./a ${persona.nombre_completo}`, style: 'info'}, ' con C.I.', {text: persona.nro_documento, style: 'info'} ,
                      ' alumno/a de', {text: programa.nombre, style: 'info'},' suscriben el presente contrato bajo las siguientes condiciones.\n\n'
                      ]
                },
                {
                  text: [ {text: 'PRIMERA:', style: 'subtitle'}, ' El/la postgraduante al tiempo de inscribirse se compromete y obliga a ',
                  'cancelar el costo total de la Colegiatura del/la ', {text: programa.nombre, style: 'info' }, ' cuyo monto asciende al equivalente en Bolivianos ', {text: formatNum , style: 'info'}, {text: '('+ amountInWords.toUpperCase() +' 00/100 BOLIVIANOS),', style: 'info'}, 
                  'monto que será cancelado respetando las cuotas y vencimientos acordados, según el plan de pagos anexo al presente contrato.\n\n' 
                 ]
                },
                {
                  text: [ {text: 'SEGUNDA:', style: 'subtitle'}, ' (Condiciones del alumno)\n\n' ]
                },
                {
                  text: ['El/la señor/a, ', {text: persona.nombre_completo, style: 'info' }, ' es alumno/a maestrante del CIDES, bajo las siguientes condiciones:\n\n']
                },
                {
                  type: 'lower-alpha',
                  ol: [
                    'La cancelación de la cuota inicial le habilita como estudiante regular, lo cual le permite figurar en las listas oficiales de la Maestría.\n\n',
                    'En caso de que el/la postgraduante incumpla con el pago de sus cuotas en más de dos fechas, dejará de ser estudiante regular del CIDES, saliendo de las listas de control de asistencia y de la evaluación de los módulos correspondientes.\n\n',
                    'Para cursar los Talleres de tesis el/la postgraduante deberá tener al día sus pagos con el CIDES.\n\n',
                    'Los pagos de las cuotas deben realizarse en las entidades financieras autorizadas por la UMSA, las serán proporcionadas a los alumnos. \n\n'
                  ]   
                },
                {
                  text: [ {text: 'TERCERA:', style: 'subtitle'}, ' (Cumplimiento de normas Institucionales)\n\n' ]
                },
                {
                  text: ['El alumno/a está obligado a cumplir con los siguientes requisitos: \n\n']
                },
                {
                  type: 'lower-alpha',
                  ol: [
                    'Tener una asistencia mínima del 80 %, en cada uno de los módulos.\n\n',
                    'Aprobar los módulos con una nota mínima de 70 puntos y cumplir con la modalidad de graduación definitiva.\n\n',
                    'En caso de imposibilidad de cursar un módulo, el postgraduante debe enviar una carta de solicitud de licencia antes de inicio del mismo.\n\n',
                    'El abandono, sin justificación válida de los módulos, implicará la reprobación del mismo.\n\n',
                    'Según reglamentos del CIDES, en caso de que un postgraduante repruebe un módulo, este podrá ser recuperado; estando el/la postgraduante obligado/a, a cancelar el valor del módulo antes de la recuperación del mismo, monto a ser definido por el CIDES.\n\n',
                    'La reprobación de tres módulos implica el retiro definitivo del/la postgraduante del programa academico que está cursando.\n\n',
                    'El postgraduante está obligado al pago de las matrículas universitarias y la presentación de fotocopia del comprobante y matrícula al Área Desconcentrada del CIDES.\n\n'
                  ]
                },
                {
                  text: [ {text: 'CUARTA:', style: 'subtitle'}, ' (De las devoluciones financieras)\n\n' ]
                },
                {
                  text: ['En caso de que el Postgraduante incurra en abandono del Programa Académico por enfermedad, traslado de lugar de trabajo u otro de fuerza mayor,',
                  ' hará conocer esa situación por escrito y en forma oportuna comprometiéndose el CIDES a devolver el dinero pagado por el/la postgraduante en los siguientes casos:\n\n']
                },
                {
                  type: 'lower-alpha',
                  ol: [
                    'La cuota inicial cancelada por el postgraduante será devuelta en caso que el/la postgraduante no haya iniciado ninguna clase del Progama Académico, aplicando el 10% de gastos de administración y el costo de la factura emitida.\n\n',
                    'Si por alguna razón el/la postgraduante interrumpiera sus actividades académicas, el CIDES devolverá -si fuese el caso- al interesado la diferencia entre el monto cancelado y el costo de los módulos cursados, el 10% de gastos de administración y el costo de las facturas emitidas. El costo por cada uno de los módulos será determinado por el CIDES.\n\n',
                    'Si el/la postgraduante pagó el 100% ó 50% del costo del curso al momento de la inscripción y por alguna razón interrumpe sus actividades académicas, el CIDES se compromete a aplicar el punto a) y/o b) de la cláusula CUARTA.\n\n'
                  ]
                },
                {
                  text: [ {text: 'QUINTA:', style: 'subtitle'}, ' (Compromiso de Titulación)\n\n' ]
                },
                {
                  text: ['Durante el proceso de formación, el/la postgraduante deberá pagar anualmente la matrícula establecida por la Universidad Mayor de San Ándres - UMSA.\n Si el/la postgraduante, tarda en la defensa de su tesis más de un año,',
                  ' después de concluido el programa, deberá cancelar la matrícula de la gestión correspondiente a la defensa.\n',
                  'En caso de que la elaboración y defensa de la tesis exceda los', {text: años }, ' años después de culminar sus estudios, el/la postgraduante deberá pagar -según normas universitarias- la matrícula correspondiente',
                  ' al año de defensa, lo mismo que los costos de reincorporación al CIDES, según reglamento específico (Reglamento de reincorporación).\n',
                  'Los postgraduantes además se comprometen a cancelar los costos de titulación, según tarifas vigentes en la Universidad Mayor de San Andrés - UMSA.\n\n']
                },
                {
                  text: [ {text: 'SEXTA:', style: 'subtitle'}, ' (De la conformidad)\n\n' ]
                },
                {
                  text: ['En constancia de su aceptación y conformidad suscriben el presente documento, en la ciudad de La Paz, a los ' + fechaLiteral,'.\n\n\n\n\n\n\n\n']
                },
                {
                  style: 'tableFirmas',
                  table: {
                    headerRows: 1,
                    widths: ['*',25,'*'],
                    body: [
                      [{text: datosGenerales.data.rows[0].nom_director, style: 'firma',  border: [false,true,false,false]}, {text:'', border:[false,false,false,false]}, {text: persona.nombre_completo, style: 'firma',border: [false,true,false,false]}],
                      [{text: 'DIRECTOR/A CIDES-UMSA', style: 'firma',border: [false,false,false,false]}, {text:'', border:[false,false,false,false]}, {text: 'POSTGRADUANTE', style: 'firma',border: [false,false,false,false]}]
                    ]
                  }
                }
              ],
              style: 'superMargin'
            }
          ],
          styles: {
            header: {
              fontSize: 15, 
              bold: true,
              alignment: 'center',
              margin: [0, 0, 0, 20]
            },
            superMargin: {
              margin: [30, 10, 30, 10],
              alignment: 'justify',
              fontSize: 10
		        },
            info: {
              bold: true
            },
            subtitle: {
              fontSize: 14,
              bold: true
            },
            firma: {
              fontSize: 10,
              italics: true,
              alignment: 'center',
              bold: true,
              margin: [0, 0, 0, 0],
            },
          }
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }
  
  function numberFormat (num, decimals = 2) {
    num = Number(num).toFixed(decimals);
    let [ number, decimal ] = num.toString().split('.');
    number = number.split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "." );
    
    return [ number, decimal ].join(',');
  }

  function dateFormat() {
    let fecha = moment(new Date());
    var month = fecha.format('MMMM');
    var day   = fecha.format('D');
    var year  = fecha.format('YYYY');
    if (day+'' === '1') {
      day = 'un'
    } else {
      day = NumerosaLetras(day);
    }
    day = NumerosaLetras(31);
    year = NumerosaLetras(year);

    let fechaL =  day.toLowerCase() + ' días del mes de ' + month.toLowerCase() + ' de ' + year.toLowerCase() + 'años '; 
    return fechaL;
  }

  function NumerosaLetras(cantidad) {

    var numero = 0;
    cantidad = parseFloat(cantidad);

    if (cantidad == "0.00" || cantidad == "0") {
        return "CERO con 00/100 ";
    } else {
        var ent = cantidad.toString().split(".");
        var arreglo = separar_split(ent[0]);
        var longitud = arreglo.length;

        switch (longitud) {
            case 1:
                numero = unidades(arreglo[0]);
                break;
            case 2:
                numero = decenas(arreglo[0], arreglo[1]);
                break;
            case 3:
                numero = centenas(arreglo[0], arreglo[1], arreglo[2]);
                break;
            case 4:
                numero = unidadesdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3]);
                break;
            case 5:
                numero = decenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4]);
                break;
            case 6:
                numero = centenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4], arreglo[5]);
                break;
        }

        ent[1] = isNaN(ent[1]) ? '00' : ent[1];

        // return numero + " con " + ent[1] + "/100";
        return numero ;
    }
}

 function unidades(unidad) {
    var unidades = Array('UN ','DOS ','TRES ' ,'CUATRO ','CINCO ','SEIS ','SIETE ','OCHO ','NUEVE ');
   

    return unidades[unidad - 1];
}

 function decenas(decena, unidad) {
   console.log('decena en decenas', decena);
    var diez = Array('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE' ,'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ');
    var decenas = Array('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ','SESENTA ','SETENTA ','OCHENTA ','NOVENTA ');
    
    if (decena ==0 && unidad == 0) {
        return "";
    }

    if (decena == 0 && unidad > 0) {
        return unidades(unidad);
    }

    if (decena == 1) {
        if (unidad == 0) {
            return decenas[decena -1];
        } else {
            return diez[unidad -1];
        }
    } else if (decena == 2) {
        
        if (unidad == 0) {
            return decenas[decena -1];
        }
        else if (unidad == 1) {
            return  "VEINTI" + "UNO";
        } 
        else {
            return "VEINTI" + unidades(unidad);
        }
    } else {

        if (unidad == 0) {
            return decenas[decena -1] + " ";
        }
        if (unidad == 1) {
            return decenas[decena -1] + " Y " + "UNO";
        }

        return decenas[decena -1] + " Y " + unidades(unidad);
    }
}

 function centenas(centena, decena, unidad) {
    var centenas = Array( "CIENTO ", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ","QUINIENTOS ","SEISCIENTOS ","SETECIENTOS ", "OCHOCIENTOS ","NOVECIENTOS ");

    if (centena == 0 && decena == 0 && unidad == 0) {
        return "";
    }
    if (centena == 1 && decena == 0 && unidad == 0) {
        return "CIEN ";
    }

    if (centena == 0 && decena == 0 && unidad > 0) {
        return unidades(unidad);
    }

    if (decena == 0 && unidad == 0) {
        return centenas[centena - 1]  +  "" ;
    }

    if (decena == 0) {
        var numero = centenas[centena - 1] + "" + decenas(decena, unidad);
        return numero.replace(" Y ", " ");
    }
    if (centena == 0) {

        return  decenas(decena, unidad);
    }
     
    return centenas[centena - 1]  +  "" + decenas(decena, unidad);
    
}

 function unidadesdemillar(unimill, centena, decena, unidad) {
    var numero = unidades(unimill) + " MIL " + centenas(centena, decena, unidad);
    numero = numero.replace("UN  MIL ", "MIL " );
    if (unidad == 0) {
        return numero.replace(" Y ", " ");
    } else {
        return numero;
    }
}

function decenasdemillar(decemill, unimill, centena, decena, unidad) {
   console.log('decena', decemill );
   console.log('decena', unimill );
    var numero = decenas(decemill, unimill) + " MIL " + centenas(centena, decena, unidad);
    return numero;
}

function centenasdemillar(centenamill,decemill, unimill, centena, decena, unidad) {

    var numero = 0;
    numero = centenas(centenamill,decemill, unimill) + " MIL " + centenas(centena, decena, unidad);
    
    return numero;
}

function separar_split(texto){
    var contenido = new Array();
    for (var i = 0; i < texto.length; i++) {
        contenido[i] = texto.substr(i,1);
    }
    return contenido;
}
async function enviaPagos (req, res, next) {
  try {
    const {idContrato, idUsuario} = req.params;
    let datos = await ContratoService.findById(idContrato);
    if (datos.code === 1) {
      let ws_id_persona = '';
      let contrato = datos.data;
      let usuario = contrato.usuario;
      let persona = contrato.persona;
      let programa = contrato.programa;
      let detallesp =  contrato.detallesprograma;
      let plan_pagos = JSON.parse(JSON.stringify(contrato.plan_pagos));
      if (detallesp.colegiatura_sia) {
        if (!contrato.wsid_persona) {
          const xml = fs.readFileSync(path.join(__dirname,'xml', 'soap.xml'), 'utf-8');
          const respuesta = await getWebService(nano(
          xml, 
          { ci: persona.nro_documento },
          'getPersonaResponse',
          'getPersonaReturn'
          ))
          if (typeof respuesta.codigo !== 'undefined') {  
            ws_id_persona = respuesta.codigo.toString();
            console.log('CODIGO ID PERSONA 1st: ', ws_id_persona);
          } else {
            const xmlPersona = fs.readFileSync(path.join(__dirname,'xml', 'soapPersona.xml'), 'utf-8'); 
            const respuestaPersona = await setWebServicePersona(nano(
            xmlPersona, 
            { ci: persona.nro_documento, 
              primerApellido: persona.primer_apellido,
              segundoApellido: persona.segundo_apellido,
              nombres: persona.nombre_completo,
              fechaNacimiento: persona.fecha_nacimiento,
              genero: persona.genero,
              nacionalidad: 'BO', 
              direccion: persona.direccion,
              telefono: persona.telefono,
              correo: usuario.email
            },
            'setPersonaResponse',
            'setPersonaReturn'
            ))
            ws_id_persona = respuestaPersona.codigo.toString();
            console.log('CODIGO ID PERSONA 2nd: ', ws_id_persona);
          } 
            if (ws_id_persona) {
              let wsid_persona = await ContratoService.createOrUpdate({
                id: contrato.id,
                wsid_persona: ws_id_persona
              });
            }
            console.log('ws_id_estudiante: ', contrato.wsid_estudiante);
          if (!contrato.wsid_estudiante) {
            let ws_id_estudiante = '';
            const xmlGetEstudiante = fs.readFileSync(path.join(__dirname,'xml', 'soapGetEstudiante.xml'), 'utf-8');
            const respuestaGetEstudiante = await getWebServiceEstudiante(nano(
            xmlGetEstudiante, 
            { idPersona: ws_id_persona,
              idColegiatura: detallesp.colegiatura_sia
            },
            'getEstudianteResponse',
            'getEstudianteReturn'
            ))
            if (respuestaGetEstudiante.codigo !== '0') {  
              ws_id_estudiante = respuestaGetEstudiante.codigo.toString();
              console.log('CODIGO ESTUDIANTE 1st: ', ws_id_estudiante);
            } else {
              const xmlSetEstudiante = fs.readFileSync(path.join(__dirname,'xml', 'soapSetEstudiante.xml'), 'utf-8');
              const respuestaSetEstudiante = await setWebServiceEstudiante(nano(
              xmlSetEstudiante, 
              { idPersona: ws_id_persona,
                idColegiatura: detallesp.colegiatura_sia
              },
              'setEstudianteResponse',
              'setEstudianteReturn'
              ));
              ws_id_estudiante = respuestaSetEstudiante.codigo.toString();
              console.log('CODIGO ESTUDIANTE 2nd: ', ws_id_estudiante);
            }
            if (ws_id_estudiante) {
              let wsid_estudiante = await ContratoService.createOrUpdate({
                id: contrato.id,
                wsid_estudiante: ws_id_estudiante
              });
            }
          }
        } else {
          console.log('El usuario ya esta registrado en el sistema sia: ', contrato.wsid_persona);
          if (!contrato.wsid_estudiante) {
            let ws_id_estudiante = '';
            const xmlGetEstudiante = fs.readFileSync(path.join(__dirname,'xml', 'soapGetEstudiante.xml'), 'utf-8');
            const respuestaGetEstudiante = await getWebServiceEstudiante(nano(
            xmlGetEstudiante, 
            { idPersona: contrato.wsid_persona,
              idColegiatura: detallesp.colegiatura_sia
            },
            'getEstudianteResponse',
            'getEstudianteReturn'
            ))
            if (respuestaGetEstudiante.codigo !== '0') {  
              ws_id_estudiante = respuestaGetEstudiante.codigo.toString();
              console.log('CODIGO ESTUDIANTE 1st: ', ws_id_estudiante);
            } else {
              const xmlSetEstudiante = fs.readFileSync(path.join(__dirname,'xml', 'soapSetEstudiante.xml'), 'utf-8');
              const respuestaSetEstudiante = await setWebServiceEstudiante(nano(
              xmlSetEstudiante, 
              { idPersona: contrato.wsid_persona,
                idColegiatura: detallesp.colegiatura_sia
              },
              'setEstudianteResponse',
              'setEstudianteReturn'
              ));
              ws_id_estudiante = respuestaSetEstudiante.codigo.toString();
              console.log('CODIGO ESTUDIANTE 2nd: ', ws_id_estudiante);
            }
            if (ws_id_estudiante) {
              let wsid_estudiante = await ContratoService.createOrUpdate({
                id: contrato.id,
                wsid_estudiante: ws_id_estudiante
              });
            }
          }
        }

        if(contrato.wsid_persona && contrato.wsid_estudiante) {
          console.log('ingresa por que tiene ambos datos')
          if(plan_pagos) {
            console.log('plan pagos', plan_pagos);
            for (let item of plan_pagos ) {
              let aux_nro_cuota = parseInt(item.numero);
              let aux_monto_cuota = parseInt(item.monto);
              console.log('viendo el dato de fecha: ', item.fecha);
              let fecha = moment(item.fecha, 'DD/MM/YYYY').format('YYYY/MM/DD');
              console.log('ver el valor de fecha!!!!!!!!!!!!!: ', typeof fecha, ' fecha: ', fecha); 
              let pago = {
              nro_cuota: aux_nro_cuota,
              monto_cuota: aux_monto_cuota,
              id_contrato: contrato.id,
              _user_created: idUsuario,
              fecha_vencimiento: fecha 
              }
              console.log('pago:', pago);
              let pagos = await PagoService.createOrUpdate(
              pago
              // fecha_vencimiento: item.fecha
             );  
             console.log('pagos', pagos);
            }
          }  
        }
      } else {
        console.log('Se requiere el ID COLEGIATURA del PROGRAMA');
      }
    }
  } catch (e) {
    return next(e);
  } 
}
  
function getWebService (xml, getResponse = 'getPersonaResponse', getReturn = 'getPersonaReturn') {
  //  console.log('revisando el xml en get Web Service', xml);
  return new Promise(async (resolve, reject) => {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml, timeout: 300000 }); // Optional timeout parameter(milliseconds)
    const { headers, body, statusCode } = response;
    // console.log(headers);
    // console.log(body);
    parseString(body, function (err, result) {
      if (err) {
        return reject(err)
      }
      // console.dir(result['soapenv:Envelope']['soapenv:Body']);
      const res = result['soapenv:Envelope']['soapenv:Body'];
      for (let item of res) {
        // console.log(item['ns1:getPersonaResponse'])
        for (let data of item[`ns1:${getResponse}`]) {
          // console.log(data.getPersonaReturn[0]['_'])
          return resolve({
            codigo: data[getReturn][0]['_'],
            estado: statusCode
          })
        }
      }
      reject(new Error('No existe el dato'))
      // console.log('RESPUESTA', res[0]['ns1:getPersonaResponse'][0].getPersonaReturn[0]['_'])
    });
    // console.log(statusCode);
  })
}
function setWebServicePersona (xml, getResponse = 'setPersonaResponse', getReturn = 'setPersonaReturn') {

  return new Promise(async (resolve, reject) => {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml});
    const { headers, body, statusCode } = response;
    parseString(body, function (err, result) {
      if (err) {
        return reject(err)
      }
      const res = result['soapenv:Envelope']['soapenv:Body'];
      for (let item of res) {
          //console.log(item['ns1:setPersonaResponse'])
        for (let data of item[`ns1:${getResponse}`]) {
          //console.log(data.setPersonaReturn[0]['_'])
          return resolve({
            codigo: data[getReturn][0]['_'],
            estado: statusCode
          })
        }
      }
      reject(new Error('No existe el dato'))
      // console.log('RESPUESTA', res[0]['ns1:getPersonaResponse'][0].getPersonaReturn[0]['_'])
    });
    // console.log(statusCode);
  })
}
function getWebServiceEstudiante (xml, getResponse = 'getEstudianteResponse', getReturn = 'getEstudianteReturn') {

  return new Promise(async (resolve, reject) => {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml});
    const { headers, body, statusCode } = response;
    parseString(body, function (err, result) {
      if (err) {
        return reject(err)
      }
      const res = result['soapenv:Envelope']['soapenv:Body'];
      for (let item of res) {
          //console.log(item['ns1:setPersonaResponse'])
        for (let data of item[`ns1:${getResponse}`]) {
          //console.log(data.setPersonaReturn[0]['_'])
          return resolve({
            codigo: data[getReturn][0]['_'],
            estado: statusCode
          })
        }
      }
      reject(new Error('No existe el dato'))
      // console.log('RESPUESTA', res[0]['ns1:getPersonaResponse'][0].getPersonaReturn[0]['_'])
    });
    // console.log(statusCode);
  })
}
function setWebServiceEstudiante (xml, getResponse = 'setEstudianteResponse', getReturn = 'setEstudianteReturn') {

  return new Promise(async (resolve, reject) => {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml});
    const { headers, body, statusCode } = response;
    parseString(body, function (err, result) {
      if (err) {
        return reject(err)
      }
      const res = result['soapenv:Envelope']['soapenv:Body'];
      for (let item of res) {
        for (let data of item[`ns1:${getResponse}`]) {
          return resolve({
            codigo: data[getReturn][0]['_'],
            estado: statusCode
          })
        }
      }
      reject(new Error('No existe el dato'))
    });
  })
}

// function toDate (string) {
//   if (typeof(string) === 'string') {
//     console.log('ingrasa al metodo:', string);
//     let pos = string.indexOf('T');
//     console.log('pos: ', pos);
//     if (pos !== -1 && pos <= 10 && pos >= 8) {
//       let fecha = string.substring(0, pos);
//       if (!/[a-zA-Z]+/g.test(fecha) && /^-?[0-9.]+-?[0-9]+-?[0-9]*$/g.test(fecha)) {
//         console.log('ingresa fecha: ', fecha);
//         fecha = fecha.split('-');
//         return new Date(fecha[0], parseInt(fecha[1]) - 1, fecha[2]);
//       }
//     }
//   }
//   return string;
// }
  
  return {
    pdfCotizacion,
    pdfContrato,
    pdfContratoPago,
    pdfReciboReserva,
    enviaPagos
  };
};
