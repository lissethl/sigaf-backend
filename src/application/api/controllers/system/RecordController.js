'use strict';

const debug = require('debug')('app:controller:usuario');
const moment = require('moment');
const path = require('path');
const numeral = require('numeral');
const { userData, generateToken } = require('../../../lib/auth');
const { crearPdf } = require('../../../lib/util');
// const { NumerosaLetras } = require('../../../lib/NumeroALetras');
module.exports = function setupRecordController (services) {
  const { Iop, RecordService, ContratoService, UsuarioService, ModuloService, Parametro,ProgramaService, DetallesprogramaService, DatosgeneralService, ConstanteService } = services;

  const styles = {
    titulo: {
      bold: true,
      alignment: 'center',
      fontSize: 7,
      fillColor: '#cccccc'
    },
    header: {
      fontSize: 15, 
      bold: true,
      alignment: 'center'              
    },
    subheader: {
      fontSize: 12, 
      bold: true,
      alignment: 'center'              
    },
    info: {
      fontSize: 7,
      bold: true 
    },
    strong: {
      fontSize: 9,
      bold: true,
      margin: [20, 0, 10, 0] 
    },
    infoEstudiantes: {
      fontSize: 7,
      margin: [20, 0, 10, 0] 
    },
    tableDatos: {
      margin: [20, 0, 10, 20]
    },
    infoEscala: {
      fontSize: 7,
      margin: [20, 0, 10, 0],
      alignment: 'center',
      bold: true
    },
    rev: {
      fontSize: 7, 
      bold: true, 
      margin: [60, 5, 5, 5],
      italics: true
    }
  };
  
  async function pdfActaNotas(req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { idPrograma, idVersion, idBloque, idModulo, idDocente } = req.params;
      let datos = await RecordService.findAll({
        id_programa: idPrograma, 
        id_version: idVersion, 
        id_blqoue: idBloque, 
        id_modulo: idModulo, 
        id_docente: idDocente});
      console.log('Los datos de record son: ', datos.data.count);
      if (datos.code === 1) {
        let programa = datos.data.rows[0].programa;
        console.log('mostrar nombre de programa', programa); 
        let estudiante = datos.data.estudiante;
        let detallesp = datos.data.rows[0].version;
        let bloque = datos.data.rows[0].bloque;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} `;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let modulo = datos.data.rows[0].modulo;
        let docente = datos.data.rows[0].docente;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let modulo_ini = modulo.fecha_ini;
        console.log('datos de fecha: ', modulo_ini);
        let headers =[ {text: 'Nro.', style: 'titulo' }, {text: 'CÉDULA DE IDENTIDAD', style: 'titulo' },{text: 'NOMBRE COMPLETO', style: 'titulo' }, {text: 'NOTA', style: 'titulo' }, {text: 'NOTA LITERAL', style: 'titulo' }, {text: 'VALORACIÓN', style: 'titulo'}];
        let filas = [headers];
        let escala1 = [];
        let escala2 = [];
        let escalaMaestria1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 64'},{text: '66 - 70'},{text: '71 - 80'},{text: '81 - 90'},{text: '91 - 100'}];
        let escalaMaestria2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Aprobado'},{text: 'Bueno'},{text: 'Muy Bueno'},{text: 'Excelente'}];  
        let escalaDoctorado1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 69'},{text: '70 - 74'},{text: '75-80'},{text: '81 - 89'},{text: '90 - 100'}];
        let escalaDoctorado2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Suficiente'},{text: 'Bueno'},{text: 'Distinguido'},{text: 'Sobresaliente'}];
        if (programa.tipo === "MAESTRIA") {
          escala1 = escalaMaestria1;
          escala2 = escalaMaestria2;
        } else {
          escala1 = escalaDoctorado1;
          escala2 = escalaDoctorado2;
        }
        let count = 0;
        for ( let e of datos.data.rows ) {
          count = count + 1;
          e.nro = count; 
          e.nota_literal = NumerosaLetras(e.nota);
          filas.push([
            e.nro,
            e.estudiante.persona.nro_documento,
            e.estudiante.persona.nombre_completo,
            e.nota,
            e.nota_literal,
            e.est_valoracion 
          ]);
        }
        let data = {
          //  pageSize: 'EXECUTIVE',
          content: [
            { text: 'ACTA DE NOTAS', style: 'header', margin: [0, 100, 0, 0]},
            { text: nomPrograma, style: 'header'},
            { text: `(${detallesp.version} VERSIÓN)`, style: 'header', margin: [0, 0, 0, 20]},
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [70, 150, 70, 'auto'],
                body: [
                  [{ text: 'BLOQUE:', style: 'info' }, {text: bloque.nombre, fontSize: 7}, { text: 'MODULO:', style: 'info' }, {text: modulo.nombre, fontSize: 7}],
                  [{ text: 'TIPO PROGRAMA:', style: 'info' }, {text: programa.tipo, fontSize: 7}, {text: 'CÓDIGO:', style: 'info'}, {text: programa.codigo, fontSize: 7}],
                  [{ text: 'DOCENTE:', style: 'info' }, {text: docente.persona.nombre_completo, fontSize: 7}, {text: 'NRO. ALUMNOS:', style: 'info'}, {text: datos.data.count, fontSize: 7}],
                  [{text: 'FECHA INICIO:', style: 'info'}, {text: modulo.fecha_ini, fontSize: 7}, {text: 'FECHA CONCLUSIÓN:', style: 'info'}, {text: modulo.fecha_fin, fontSize: 7}],
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'infoEstudiantes',
              table: {
              headerRows: 1,
              widths: [15, 50, 150, 20,100, '*'],
              body: filas
              }
            },
            {
              text: 'Esta acta de calificaciones quedará nula y sin valor en caso de comprobarse enmiendo, borroneo o alteraciones de letras o números', fontSize: 6, margin: [20, 0, 10, 20]
            },
            {
              text: 'La Paz, ' + fecha_emision, fontSize: 8, alignment: 'right', margin: [0, 0, 20, 20]
            },
            {
              style: 'infoEscala',
              table: {
              headerRows: 1,
              widths: [100, 70, 70, 70, 70, 70],
              body: [escala1, escala2]
              },
              layout: 'noBorders'
            }
          ],
          styles
        }
        const pdf = await crearPdf(data);    
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
      
    } catch (e) {
      return next(e);
    }
  }
  async function pdfActaNotasConsolidado(req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { idPrograma, idVersion, idBloque, idModulo} = req.params;
      let datos = await RecordService.findAll({
        id_programa: idPrograma, 
        id_version: idVersion, 
        id_blqoue: idBloque, 
        id_modulo: idModulo
        });
      console.log('Los datos de record son: ', datos.data.count);
      if (datos.code === 1) {
        let programa = datos.data.rows[0].programa;
        console.log('mostrar nombre de programa', programa); 
        let estudiante = datos.data.estudiante;
        let detallesp = datos.data.rows[0].version;
        console.log('detallesp:', detallesp);
        let bloque = datos.data.rows[0].bloque;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} `;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let modulo = datos.data.rows[0].modulo;
        let docente = datos.data.rows[0].docente;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let modulo_ini = modulo.fecha_ini;
        console.log('datos de fecha: ', modulo_ini);
        let headers =[ {text: 'Nro.', style: 'titulo' }, {text: 'CÉDULA DE IDENTIDAD', style: 'titulo' },{text: 'NOMBRE COMPLETO', style: 'titulo' }, {text: 'NOTA', style: 'titulo' }, {text: 'NOTA LITERAL', style: 'titulo' }, {text: 'VALORACIÓN', style: 'titulo'}];
        let filas = [headers];
        let escala1 = [];
        let escala2 = [];
        let escalaMaestria1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 65'},{text: '66 - 70'},{text: '71 - 80'},{text: '81 - 90'},{text: '91 - 100'}];
        let escalaMaestria2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Aprobado'},{text: 'Bueno'},{text: 'Muy Bueno'},{text: 'Excelente'}];  
        let escalaDoctorado1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 69'},{text: '70 - 74'},{text: '75-80'},{text: '81 - 89'},{text: '90 - 100'}];
        let escalaDoctorado2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Suficiente'},{text: 'Bueno'},{text: 'Distinguido'},{text: 'Sobresaliente'}];
        if (programa.tipo === "MAESTRIA") {
          escala1 = escalaMaestria1;
          escala2 = escalaMaestria2;
        } else {
          escala1 = escalaDoctorado1;
          escala2 = escalaDoctorado2;
        }
        let count = 0;

        let estudiantes = {};
        for (const item of (datos.data.rows || [])) {
          if (!estudiantes[item.id_estudiante]) {
            estudiantes[item.id_estudiante] = {
              items: [],
              total: 0
            };
          }
          estudiantes[item.id_estudiante].items.push(item);
        }
        let i = 0;
        for (const id in estudiantes) {
          let total = 0;
          for (const el of estudiantes[id].items) {
            total += parseFloat(el.nota);
          }
          estudiantes[id].total = total / estudiantes[id].items.length;
          estudiantes[id].index = ++i;
        } 
        console.log('estudiantes: ', estudiantes);
        for ( const id in estudiantes) {
          estudiantes[id].items[0].nota_literal = NumerosaLetras(estudiantes[id].total);
          estudiantes[id].items[0].total_valoracion = verificar(programa.tipo,  programa.nota_minima, estudiantes[id].total);
          filas.push([
            estudiantes[id].index,
            estudiantes[id].items[0].estudiante.persona.nro_documento,
            estudiantes[id].items[0].estudiante.persona.nombre_completo,
            estudiantes[id].total,
            estudiantes[id].items[0].nota_literal,
            estudiantes[id].items[0].total_valoracion
          ]); 
          console.log('filas: ', filas);
        }
        let data = {
          //  pageSize: 'EXECUTIVE',
          content: [
            { text: 'ACTA DE NOTAS', style: 'header', margin: [0, 100, 0, 0]},
            { text: nomPrograma, style: 'header'},
            { text: `(${detallesp.version} VERSIÓN)`, style: 'header', margin: [0, 0, 0, 20]},
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [70, 150, 70, 'auto'],
                body: [
                  [{ text: 'BLOQUE:', style: 'info' }, {text: bloque.nombre, fontSize: 7}, { text: 'MODULO:', style: 'info' }, {text: modulo.nombre, fontSize: 7}],
                  [{ text: 'TIPO PROGRAMA:', style: 'info' }, {text: programa.tipo, fontSize: 7}, {text: 'CÓDIGO:', style: 'info'}, {text: programa.codigo, fontSize: 7}],
                  [{ text: 'COORDINADOR:', style: 'info' }, {text: detallesp.coordinador, fontSize: 7}, {text: 'NRO. ALUMNOS:', style: 'info'}, {text: i, fontSize: 7}],
                  [{text: 'FECHA INICIO:', style: 'info'}, {text: modulo.fecha_ini, fontSize: 7}, {text: 'FECHA CONCLUSIÓN:', style: 'info'}, {text: modulo.fecha_fin, fontSize: 7}],
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'infoEstudiantes',
              table: {
              headerRows: 1,
              widths: [15, 50, 150, 20,100, '*'],
              body: filas
              }
            },
            {
              text: 'Esta acta de calificaciones quedará nula y sin valor en caso de comprobarse enmiendo, borroneo o alteraciones de letras o números', fontSize: 6, margin: [20, 0, 10, 20]
            },
            {
              text: 'La Paz, ' + fecha_emision, fontSize: 8, alignment: 'right', margin: [0, 0, 20, 20]
            },
            {
              style: 'infoEscala',
              table: {
              headerRows: 1,
              widths: [100, 70, 70, 70, 70, 70],
              body: [escala1, escala2]
              },
              layout: 'noBorders'
            }
          ],
          styles
        }
        const pdf = await crearPdf(data);    
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
      
    } catch (e) {
      return next(e);
    }
  }
  async function pdfListaAsistencia(req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { idPrograma, idVersion, idBloque, idModulo, idDocente } = req.params;
      let datos = await RecordService.findAll({
        id_programa: idPrograma,
        id_version: idVersion, 
        id_bloque: idBloque,
        id_modulo: idModulo,
        id_docente: idDocente});
      // console.log('Los datos de record son: ', datos.data.rows);
      if (datos.code === 1) {
        let programa = datos.data.rows[0].programa;
        let horario = await ConstanteService.findAll({
          grupo: 'HORARIO',
          codigo: programa.horarios
        });
        console.log('datos del hoario: ', horario.data.rows[0].nombre);
        let estudiante = datos.data.estudiante;
        let detallesp = datos.data.rows[0].version;
        let bloque = datos.data.rows[0].bloque;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} `;
        let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a');
        let modulo = datos.data.rows[0].modulo;
        let docente = datos.data.rows[0].docente;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let modulo_ini = modulo.fecha_ini;
        console.log('datos de fecha: ', modulo_ini); 
        //modulo_ini =  moment((modulo_ini).format('DD/MM/YYYY'));
        //let modulo_fin =  moment ((modulo.fecha_fin).format('DD/MM/YYYY'));
        let headers =[ {text: 'Nro.', style: 'titulo' }, {text: 'C.I.', style: 'titulo' },{text: 'PATERNO', style: 'titulo' }, {text: 'MATERNO', style: 'titulo' }, {text: 'NOMBRES', style: 'titulo' }, {text: 'FIRMA ENTRADA', style: 'titulo'}, {text: 'FIRMA SALIDA', style: 'titulo'} ];
        let filas = [headers];
        let count = 0;
        let estudiantes = {};
        for (const item of (datos.data.rows || [])) {
          if (!estudiantes[item.id_estudiante]) {
            estudiantes[item.id_estudiante] = {
              items: [],
              total: 0
            };
          }
          estudiantes[item.id_estudiante].items.push(item);
        }
        let i = 0;
        for (const id in estudiantes) {
          estudiantes[id].index = ++i;
        }    
        console.log('estudiantes: ', estudiantes);
        for ( const id in estudiantes) {
          estudiantes[id].items[0].firma_entrada = '';
          estudiantes[id].items[0].firma_salida = '';
          filas.push([
            estudiantes[id].index,
            estudiantes[id].items[0].estudiante.persona.nro_documento,
            estudiantes[id].items[0].estudiante.persona.primer_apellido,
            estudiantes[id].items[0].estudiante.persona.segundo_apellido,
            estudiantes[id].items[0].estudiante.persona.nombres,
            estudiantes[id].items[0].firma_entrada,
            estudiantes[id].items[0].firma_salida
          ]);  
        }
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:10,
                columns: [
                {
                    fontSize: 9,
                    text:[
                    {
                    text: '--------------------------------------------------------------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };
          },  
          content: [
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x:80, y: 65}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 100,
              absolutePosition: {x: 450, y: 70}
            },
            { style: 'header',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [ {text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n', margin: [0, 45, 0, 0]}]
                ]
              },
              layout: 'noBorders'
            },
            { text: 'CONTROL DE ASISTENCIA DE ESTUDIANTES', style: 'header'},
            { text: 'FECHA:        /       /        ', style: 'header', decoration: 'underline', margin: [0, 0, 0, 30]},
            {
              style: 'tableDatosP',
              table: {
                headerRows: 1,
                widths: [70, 'auto'],
                body: [
                  [{ text: 'PROGRAMA:', style: 'info' }, {text: programa.nombre, fontSize: 7}],
                  [{ text: 'MODULO:', style: 'info' }, {text: modulo.nombre, fontSize: 7}]
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [70, 150, 70, 'auto'],
                body: [
                 
                  [{ text: 'BLOQUE:', style: 'info' }, {text: bloque.nombre, fontSize: 7}, { text: 'HORARIO:', style: 'info' }, {text: horario.data.rows[0].nombre, fontSize: 7}],
                  [{ text: 'TIPO PROGRAMA:', style: 'info' }, {text: programa.tipo, fontSize: 7}, {text: 'CÓDIGO:', style: 'info'}, {text: programa.codigo, fontSize: 7}],
                  [{ text: 'DOCENTE:', style: 'info' }, {text: docente.persona.nombre_completo, fontSize: 7}, {text: 'NRO. ALUMNOS:', style: 'info'}, {text: i, fontSize: 7}],
                  [{text: 'FECHA INICIO:', style: 'info'}, {text: modulo.fecha_ini, fontSize: 7}, {text: 'FECHA CONCLUSIÓN:', style: 'info'}, {text: modulo.fecha_fin, fontSize: 7}],
                ]
              },
              layout: 'noBorders'
            },
            {
              style: 'infoEstudiantes',
              table: {
              headerRows: 1,
              widths: [15, 50, 70, 70, 80, 65, 65],
              body: filas
              }
            },
            {
              text: 'La Paz, ' + fecha_emision, fontSize: 8, alignment: 'right', margin: [0, 0, 20, 20]
            }
          ],
          styles
        }
        const pdf = await crearPdf(data);    
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
      
    } catch (e) {
      return next(e);
    }
  }
  async function pdfInscritos (req, res, next) {
    try {
      const { idPrograma, idVersion, idBloque, idModulo, userName, idDocente } = req.params;
      let datos = await RecordService.findAll({
        id_programa: idPrograma, 
        id_version: idVersion, 
        id_bloque: idBloque, 
        id_modulo: idModulo,
        id_docente: idDocente});
      let user = await UsuarioService.findAll({
        usuario: userName
      });
      // let usuarioRep = user.data.rows[0].persona.nombres.substring(0, 1) + user.data.rows[0].persona.primer_apellido.substring(0, 1) + user.data.rows[0].persona.segundo_apellido.substring(0, 1) ;
      //  console.log('datos de usuarioRep: ', usuarioRep);
      let modulo = datos.data.rows[0].modulo;
      let docente = datos.data.rows[0].docente;
      let prog = await ProgramaService.findById(idPrograma);
      let programa = prog.data;
      let detallesPrograma = await DetallesprogramaService.findById(idVersion);
      let version = detallesPrograma.data;
      let fecha_emision = new Date();
      let count = 1;
      moment.locale('es');
      fecha_emision = moment(fecha_emision).format('LL');
      let fechaRep = moment (new Date()).format('DD/MM/YYYY h:mm:ss a')
      if (datos.code === 1) {
        let horario = await ConstanteService.findAll({
          grupo: 'HORARIO',
          codigo: programa.horarios
        });
        let nombrePrograma = `PROGRAMA: ${programa.nombre}\n`;
        let gestion = `GESTIÓN: ${version.gestion} \n`;
        let headers = [ {text: 'N°', style: 'titulo'  }, {text: 'C.I.', style: 'titulo'}, {text: 'NOMBRE COMPLETO', style: 'titulo'}, {text: 'PROFESIÓN', style: 'titulo'}, {text: 'EMAIL', style: 'titulo'},{text: 'CELULAR', style: 'titulo'}, {text: 'TELÉFONO', style: 'titulo'}];
        let filas = [headers];
        console.log(datos.data.rows)
        let estudiantes = {};
        for (const item of (datos.data.rows || [])) {
          if(!estudiantes[item.id_estudiante]) {
            estudiantes[item.id_estudiante] = {
              items: [],
              profesion: null
            }
          }
          estudiantes[item.id_estudiante].items.push(item);
        }
        let i = 0;
        for (const id in estudiantes) {
          estudiantes[id].index = ++i;
          estudiantes[id].profesion = await ConstanteService.findAll({
            grupo: 'CARRERA',
            codigo: estudiantes[id].items.carrera
          });
        }
        console.log('estudiantes: ', estudiantes);
        for ( const id in estudiantes) {
          filas.push([
            { text: estudiantes[id].index, fontSize: 7 },
            { text: estudiantes[id].items[0].estudiante.persona.nro_documento, fontSize: 7 },
            // { text: estudiantes[id].items[0].estudiante.persona.primer_apellido, fontSize: 7 },
            // { text: estudiantes[id].items[0].estudiante.persona.segundo_apellido, fontSize: 7 },
            // { text: estudiantes[id].items[0].estudiante.persona.nombres, fontSize: 7 },
            { text: estudiantes[id].items[0].estudiante.persona.nombre_completo, fontSize: 7 },
            { text: estudiantes[id].profesion.data.rows[0].nombre, fontSize: 7 },
            { text: estudiantes[id].items[0].estudiante.email, fontSize: 7 },
            { text: estudiantes[id].items[0].estudiante.persona.movil, fontSize: 7 },
            { text: estudiantes[id].items[0].estudiante.persona.telefono, fontSize: 7 }
          ]);  
        }
        
        let data = {
          footer: function(currentPage, pageCount) {
            return {
                margin:10,
                columns: [
                {
                    fontSize: 9,
                    text:[
                    {
                    text: '--------------------------------------------------------------------------' +
                    '\n',
                    margin: [0, 20]
                    },
                    {
                    text: currentPage.toString() + ' de ' + pageCount,
                    }
                    ],
                    alignment: 'center'
                }
                ]
            };      
        },
          content: [    
            {
              image: path.join(global.appRoot, 'public/umsaLogo.png'),
              width: 55,
              absolutePosition: {x: 100, y: 70}
            },
            {
              image: path.join(global.appRoot, 'public/cides.png'),
              width: 85, 
              height: 110,
              absolutePosition: {x: 430, y: 66}
            },            
            { style: 'header',
              table: {
                headerRows: 1, 
                widths: ['*'],
                body: [
                  [ {text: 'UNIVERSIDAD MAYOR DE SAN ANDRÉS\n POSTGRADO EN CIENCIAS DEL DESARROLLO\n CIDES - UMSA\n LISTA DE INSCRITOS\n'}]
                ]
              },
              layout: 'noBorders'
            },
            {
              margin: [10, 2, 40, 2],
              table: {
                widths: [ 110, '*' ],
                body: [
                  [{text: 'PROGRAMA:', style: 'datos' }, {text: programa.nombre, fontSize: 7}],
                  [{text: 'VERSIÓN:', style: 'datos' }, {text: version.version, fontSize: 7}],
                  [{text: 'MÓDULO:', style: 'datos' }, {text: modulo.nombre, fontSize: 7}]
                ]
              },
              layout:'noBorders'
            },
            {
              margin: [60, 0, 40, 0],
              table: {
                widths: [ 60, 120, 70, 'auto' ],
                body: [
                  [{text: 'DOCENTE:', fontSize: 7, bold: true }, {text: docente.persona.nombre_completo, fontSize: 7}, {text: 'HORARIO:', fontSize: 7, bold: true }, {text: horario.data.rows[0].nombre, fontSize: 7}],
                  [{text: 'FECHA INICIO:', fontSize: 7, bold: true }, {text: modulo.fecha_ini, fontSize: 7},{text: 'FECHA CONCLUSIÓN:', fontSize: 7, bold: true }, {text: modulo.fecha_fin, fontSize: 7}],
                  [{text: 'GESTIÓN:', fontSize: 7, bold: true}, {text: version.gestion, fontSize: 7}, {text: 'FECHA DE EMISIÓN:', fontSize: 7, bold: true}, {text: fecha_emision, fontSize: 7}]
                ]
              },
              layout:'noBorders'
            },
            { 
              margin: [60, 20, 50, 0], // optional              
              table: {
                headerRows: 1,
                widths: [ 10, 30, 100, 90, 90, 30, '*' ],
                body: filas
              }
            }
            /*,
            {
              text: `CIDES*${usuarioRep}*${fechaRep}`, style: 'rev'
            } */
          ],
          styles
        };
        const pdf = await crearPdf(data);
        
        // Devolver en la cabezara de la respuesta del pdf
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe el usuario' });
      }
    } catch (e) {
      return next(e);
    }
  }

  
  function numberFormat (num, decimals = 2) {
    num = Number(num).toFixed(decimals);
    let [ number, decimal ] = num.toString().split('.');
    number = number.split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "." );
    
    return [ number, decimal ].join(',');
  }

  function dateFormat() {
    let fecha = moment(new Date());
    var month = fecha.format('MMMM');
    var day   = fecha.format('D');
    var year  = fecha.format('YYYY');
    if (day+'' === '1') {
      day = 'un'
    } else {
      day = NumerosaLetras(day);
    }
    day = NumerosaLetras(31);
    year = NumerosaLetras(year);

    let fechaL =  day.toLowerCase() + ' días del mes de ' + month.toLowerCase() + ' de ' + year.toLowerCase() + 'años '; 
    return fechaL;
  }

  function NumerosaLetras(cantidad) {

    var numero = 0;
    cantidad = parseFloat(cantidad);

    if (cantidad == "0.00" || cantidad == "0") {
        return "CERO con 00/100 ";
    } else {
        var ent = cantidad.toString().split(".");
        var arreglo = separar_split(ent[0]);
        var longitud = arreglo.length;

        switch (longitud) {
            case 1:
                numero = unidades(arreglo[0]);
                break;
            case 2:
                numero = decenas(arreglo[0], arreglo[1]);
                break;
            case 3:
                numero = centenas(arreglo[0], arreglo[1], arreglo[2]);
                break;
            case 4:
                numero = unidadesdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3]);
                break;
            case 5:
                numero = decenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4]);
                break;
            case 6:
                numero = centenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4], arreglo[5]);
                break;
        }

        ent[1] = isNaN(ent[1]) ? '00' : ent[1];

        // return numero + " con " + ent[1] + "/100";
        return numero ;
    }
}

 function unidades(unidad) {
    var unidades = Array('UN ','DOS ','TRES ' ,'CUATRO ','CINCO ','SEIS ','SIETE ','OCHO ','NUEVE ');
   

    return unidades[unidad - 1];
}

 function decenas(decena, unidad) {
   console.log('decena en decenas', decena);
    var diez = Array('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE' ,'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ');
    var decenas = Array('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ','SESENTA ','SETENTA ','OCHENTA ','NOVENTA ');
    
    if (decena ==0 && unidad == 0) {
        return "";
    }

    if (decena == 0 && unidad > 0) {
        return unidades(unidad);
    }

    if (decena == 1) {
        if (unidad == 0) {
            return decenas[decena -1];
        } else {
            return diez[unidad -1];
        }
    } else if (decena == 2) {
        
        if (unidad == 0) {
            return decenas[decena -1];
        }
        else if (unidad == 1) {
            return  "VEINTI" + "UNO";
        } 
        else {
            return "VEINTI" + unidades(unidad);
        }
    } else {

        if (unidad == 0) {
            return decenas[decena -1] + " ";
        }
        if (unidad == 1) {
            return decenas[decena -1] + " Y " + "UNO";
        }

        return decenas[decena -1] + " Y " + unidades(unidad);
    }
}

 function centenas(centena, decena, unidad) {
    var centenas = Array( "CIENTO ", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ","QUINIENTOS ","SEISCIENTOS ","SETECIENTOS ", "OCHOCIENTOS ","NOVECIENTOS ");

    if (centena == 0 && decena == 0 && unidad == 0) {
        return "";
    }
    if (centena == 1 && decena == 0 && unidad == 0) {
        return "CIEN ";
    }

    if (centena == 0 && decena == 0 && unidad > 0) {
        return unidades(unidad);
    }

    if (decena == 0 && unidad == 0) {
        return centenas[centena - 1]  +  "" ;
    }

    if (decena == 0) {
        var numero = centenas[centena - 1] + "" + decenas(decena, unidad);
        return numero.replace(" Y ", " ");
    }
    if (centena == 0) {

        return  decenas(decena, unidad);
    }
     
    return centenas[centena - 1]  +  "" + decenas(decena, unidad);
    
}

 function unidadesdemillar(unimill, centena, decena, unidad) {
    var numero = unidades(unimill) + " MIL " + centenas(centena, decena, unidad);
    numero = numero.replace("UN  MIL ", "MIL " );
    if (unidad == 0) {
        return numero.replace(" Y ", " ");
    } else {
        return numero;
    }
}

function decenasdemillar(decemill, unimill, centena, decena, unidad) {
   console.log('decena', decemill );
   console.log('decena', unimill );
    var numero = decenas(decemill, unimill) + " MIL " + centenas(centena, decena, unidad);
    return numero;
}

function centenasdemillar(centenamill,decemill, unimill, centena, decena, unidad) {

    var numero = 0;
    numero = centenas(centenamill,decemill, unimill) + " MIL " + centenas(centena, decena, unidad);
    
    return numero;
}
function separar_split(texto){
    var contenido = new Array();
    for (var i = 0; i < texto.length; i++) {
        contenido[i] = texto.substr(i,1);
    }
    return contenido;
}

function verificar (tipo, minNota, nota) {
    nota = parseInt(nota);
    minNota = parseInt(minNota);
    console.log ('datos de la funcion: tipo', tipo, ' min ', minNota, ' nota ', nota);
    if (tipo === 'MAESTRIA') {
      if (nota < minNota) {
        return 'REPROBADO';
      } else {
        if (nota <= 69) {
          return 'APROBADO';
        } else {
          if (nota > 70 && nota <= 80) {
            return 'BUENO';
          } else {
            if (nota > 80 && nota <= 90) {
              return 'MUY BUENO';
            } else {
              if (nota > 90 && nota <= 100) {
                return 'EXCELENTE';
              }
            }
          }
        }
      }
    }
    if (tipo === 'DOCTORADO') {
      if (nota >= 0 && nota <= minNota) {
        return 'REPROBADO';
      } else {
        if (nota >= 70 && nota <= 74) {
          return 'SUFICIENTE';
        } else {
          if (nota > 74 && nota <= 80) {
            return 'BUENO';
          } else {
            if (nota > 80 && nota <= 89) {
              return 'DISTINGUIDO';
            } else {
              if (nota > 89 && nota <= 100) {
                return 'SOBRESALIENTE';
              }
            }
          }
        }
      }
    }
   
  }

  async function recordAcademico (req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { idEstudiante } = req.params;
      let datos = await RecordService.findAll({ id_estudiante: idEstudiante, est_valoracion: 'APROBADO' });
      console.log('Los datos de record son: ', datos.data.count);
      if (datos.code === 1) {
        let programa = datos.data.rows[0].programa;
        console.log('mostrar nombre de programa', programa); 
        let estudiante = datos.data.rows[0].estudiante;
        let detallesp = datos.data.rows[0].version;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} `;
        let modulo = datos.data.rows[0].modulo;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let modulo_ini = modulo.fecha_ini;
        console.log('datos de fecha: ', modulo_ini);
        let escala1 = [];
        let escala2 = [];
        let escalaMaestria1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 64'},{text: '66 - 70'},{text: '71 - 80'},{text: '81 - 90'},{text: '91 - 100'}];
        let escalaMaestria2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Aprobado'},{text: 'Bueno'},{text: 'Muy Bueno'},{text: 'Excelente'}];  
        let escalaDoctorado1 = [{text: 'ESCALA DE NOTAS'}, {text: '1 - 69'},{text: '70 - 74'},{text: '75-80'},{text: '81 - 89'},{text: '90 - 100'}];
        let escalaDoctorado2 = [{text: 'Rendimiento'}, {text: 'Reprobado'},{text: 'Suficiente'},{text: 'Bueno'},{text: 'Distinguido'},{text: 'Sobresaliente'}];
        if (programa.tipo === "MAESTRIA") {
          escala1 = escalaMaestria1;
          escala2 = escalaMaestria2;
        } else {
          escala1 = escalaDoctorado1;
          escala2 = escalaDoctorado2;
        }
        let count = 0;
        let items = {};
        for (const item of datos.data.rows) {
          const fecha = item.fecha_nota.split('-');
          const month = parseInt(fecha[1]);
          const year = parseInt(fecha[0]);
          let number = 'I';
          if (month >= 7 && month <= 12) {
            number = 'II';
          }
          const periodo = `${number}-${year}`;
          if (!items[periodo]) {
            items[periodo] = [[{text: 'Nro.', style: 'titulo' },{text: 'MODULO', style: 'titulo' }, {text: 'NOTA', style: 'titulo' }, {text: 'NOTA LITERAL', style: 'titulo' }, {text: 'VALORACIÓN', style: 'titulo'}, {text: 'FECHA', style: 'titulo'}]];
            count = 0;
          }
          items[periodo].push([
            ++count,
            item.modulo.nombre,
            item.nota,
            NumerosaLetras(item.nota),
            item.est_valoracion,
            item.fecha_nota 
          ]);
        }

        let tables = [];
        for (let key in items) {
          tables.push({ text: 'PERIODO: ' + key, style: 'strong' });
          tables.push({
            style: 'infoEstudiantes',
            table: {
              headerRows: 1,
              widths: [15, 200, 20, 100, 50, '*'],
              body: items[key]
            }
          });
        }
        let data = {
          //  pageSize: 'EXECUTIVE',
          content: [
            { text: 'RECORD ACADÉMICO', style: 'header', margin: [0, 100, 0, 0]},
            { text: nomPrograma, style: 'subheader' },
            { text: `(${detallesp.version} VERSIÓN)`, style: 'subheader', margin: [0, 0, 0, 20]},
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [70, 150, 70, 'auto'],
                body: [
                  [{ text: 'NOMBRE COMPLETO:', style: 'info' }, {text: estudiante.persona.nombre_completo, fontSize: 8}]
                ]
              },
              layout: 'noBorders'
            },
            ...tables,
            {
              text: 'La Paz, ' + fecha_emision, fontSize: 8, alignment: 'right', margin: [20, 20, 20, 20]
            },
            {
              style: 'infoEscala',
              table: {
              headerRows: 1,
              widths: [100, 70, 70, 70, 70, 70],
              body: [escala1, escala2]
              },
              layout: 'noBorders'
            }
          ],
          styles
        }
        const pdf = await crearPdf(data);    
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
      
    } catch (e) {
      return next(e);
    }
  }

  async function historialAcademico (req, res, next) {
    try {
      // llamar a un método de un servicio que devuelva los datos para el pdf
      // Procesar los datos del servicio con la libreria PDF
      const { idEstudiante } = req.params;
      let datos = await RecordService.findAll({ id_estudiante: idEstudiante });
      console.log('Los datos de record son: ', datos.data.count);
      if (datos.code === 1) {
        let programa = datos.data.rows[0].programa;
        console.log('mostrar nombre de programa', programa); 
        let estudiante = datos.data.rows[0].estudiante;
        let detallesp = datos.data.rows[0].version;
        let nomPrograma = `${programa.nombre} ${detallesp.periodo} `;
        let modulo = datos.data.rows[0].modulo;
        let fecha_emision = new Date();
        moment.locale('es');
        fecha_emision = moment(fecha_emision).format('LL');
        let modulo_ini = modulo.fecha_ini;
        console.log('datos de fecha: ', modulo_ini);
        let count = 0;
        let items = {};
        for (const item of datos.data.rows) {
          const fecha = item.fecha_nota.split('-');
          const month = parseInt(fecha[1]);
          const year = parseInt(fecha[0]);
          let number = 'I';
          if (month >= 7 && month <= 12) {
            number = 'II';
          }
          const periodo = `${number}-${year}`;
          if (!items[periodo]) {
            items[periodo] = [[{text: 'Nro.', style: 'titulo' },{text: 'MODULO', style: 'titulo' }, {text: 'NOTA', style: 'titulo' }, {text: 'NOTA LITERAL', style: 'titulo' }, {text: 'VALORACIÓN', style: 'titulo'}, {text: 'FECHA', style: 'titulo'}]];
            count = 0;
          }
          items[periodo].push([
            ++count,
            item.modulo.nombre,
            item.nota,
            NumerosaLetras(item.nota),
            item.est_valoracion,
            item.fecha_nota 
          ]);
        }

        let tables = [];
        for (let key in items) {
          tables.push({ text: 'PERIODO: ' + key, style: 'strong' });
          tables.push({
            style: 'infoEstudiantes',
            table: {
              headerRows: 1,
              widths: [15, 200, 20, 100, 50, '*'],
              body: items[key]
            }
          });
        }
        let data = {
          //  pageSize: 'EXECUTIVE',
          content: [
            { text: 'HISTORIAL ACADÉMICO', style: 'header', margin: [0, 100, 0, 0]},
            { text: nomPrograma, style: 'subheader' },
            { text: `(${detallesp.version} VERSIÓN)`, style: 'subheader', margin: [0, 0, 0, 20]},
            {
              style: 'tableDatos',
              table: {
                headerRows: 1,
                widths: [70, 150, 70, 'auto'],
                body: [
                  [{ text: 'NOMBRE COMPLETO:', style: 'info' }, {text: estudiante.persona.nombre_completo, fontSize: 8}]
                ]
              },
              layout: 'noBorders'
            },
            ...tables,
            {
              text: 'La Paz, ' + fecha_emision, fontSize: 8, alignment: 'right', margin: [20, 20, 20, 20]
            }
          ],
          styles
        }
        const pdf = await crearPdf(data);    
        res.contentType('application/pdf');
        res.send(pdf);
      } else {
        res.status(400).send({ error: 'No existe datos' });
      }
      
    } catch (e) {
      return next(e);
    }
  }

  return {
    pdfActaNotas,
    pdfListaAsistencia,
    pdfInscritos,
    pdfActaNotasConsolidado,
    recordAcademico,
    historialAcademico
  };
};
