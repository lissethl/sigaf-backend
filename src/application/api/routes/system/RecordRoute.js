'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupUsuario (api, controllers) {
  const { RecordController } = controllers;
  api.post('/actaNotas/pdf/:idPrograma/:idVersion/:idBloque/:idModulo/:idDocente', RecordController.pdfActaNotas);
  api.post('/actaNotasConsolidado/pdf/:idPrograma/:idVersion/:idBloque/:idModulo/', RecordController.pdfActaNotasConsolidado);
  api.post('/listaAsistencia/pdf/:idPrograma/:idVersion/:idBloque/:idModulo/:idDocente', RecordController.pdfListaAsistencia);
  api.post('/inscritos/pdfInscritos/pdf/:idPrograma/:idVersion/:idBloque/:idModulo/:userName/:idDocente', RecordController.pdfInscritos);
  api.post('/inscritos/pdfInscritos/pdf/:idPrograma/:idVersion/:idBloque/:idModulo/:userName/:idDocente', RecordController.pdfInscritos);
  api.post('/recordAcademico/:idEstudiante', RecordController.recordAcademico);
  api.post('/historialAcademico/:idEstudiante', RecordController.historialAcademico);
  return api;
};
