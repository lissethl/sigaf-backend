'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupUsuario (api, controllers) {
  const { UsuarioController } = controllers;

  api.get('/persona-segip/:ci', guard.check(['personas:read']), UsuarioController.personaSegip);
  api.patch('/cambiar_pass', guard.check(['usuarios:update']), UsuarioController.cambiarPass);
  api.patch('/desactivar_cuenta', guard.check(['usuarios:update']), UsuarioController.desactivarCuenta);
  // api.get('/menu', guard.check(['modulos:read']), UsuarioController.obtenerMenu);
  api.get('/menu', UsuarioController.obtenerMenu);
  api.get('/regenerar_password/:id', guard.check(['usuarios:read']), UsuarioController.regenerarPassword);
  api.post('/entrevistas/pdf/:estado_est/:idPrograma/:idVersion/:userName', UsuarioController.generarPdfEntrevistas);
  api.post('/carta_aceptacion/pdf/:idUsuario', UsuarioController.generarPdfCartaAceptacion);
  api.post('/formulario_inscripcion/pdf/:idUsuario', UsuarioController.generarPdfFormularioInscripcion);
  api.post('/requisitos/pdf/:idUsuario/:userName', UsuarioController.generarPdfRequisitos);
  api.post('/doc_requisitos/pdf/:idUsuario/:userName', UsuarioController.generarPdfDocRequisitos);
  api.post('/kardex_docente/pdf/:idUsuario', UsuarioController.generarPdfKardexDocente);
  api.post('/requisitos/pdfrequisitos/:estado_est/:idPrograma/:idVersion/:userName', UsuarioController.generarPdfSegRequisitos);
  api.post('/usuario/foto/upload', UsuarioController.uploadFoto);
  return api;
};
