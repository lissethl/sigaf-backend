'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupUsuario (api, controllers) {
  const { ContratoController } = controllers;
  api.post('/cotizacion/pdf/:idContrato', ContratoController.pdfCotizacion);
  api.post('/contrato/pdf/:idUsuario', ContratoController.pdfContrato);
  api.post('/contratoPago/pdf/:idUsuario', ContratoController.pdfContratoPago);
  api.post('/reserva/pdf/:idUsuario/:userName', ContratoController.pdfReciboReserva);
  api.post('/pagos/ws/:idContrato/:idUsuario', ContratoController.enviaPagos);
  return api;
};
