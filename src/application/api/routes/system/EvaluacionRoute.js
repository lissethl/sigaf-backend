'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupUsuario (api, controllers) {
  const { EvaluacionController } = controllers;
  
  api.post('/evaluacion/docente/:idEvaluacion', EvaluacionController.evaluacionDocente);
  api.post('/evaluacion/alumno/:idEvaluacion', EvaluacionController.evaluacionAlumno);
  api.post('/evaluacion/consolidado/', EvaluacionController.consolidado);

  return api;
};
