'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ArchivoService } = services;

  return {
    Query: {
      archivos: async (_, args, context) => {
        permissions(context, 'archivos:read');

        let items = await ArchivoService.findAll(args);
        return response(items);
      },
      archivo: async (_, args, context) => {
        permissions(context, 'archivos:read');

        let item = await ArchivoService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      archivoAdd: async (_, args, context) => {
        permissions(context, 'archivos:create');

        args.archivo._user_created = context.id_usuario;
        let item = await ArchivoService.createOrUpdate(args.archivo);
        return response(item);
      },
      archivoEdit: async (_, args, context) => {
        permissions(context, 'archivos:update');

        args.archivo._user_updated = context.id_usuario;
        args.archivo._updated_at = new Date();
        args.archivo.id = args.id;
        let item = await ArchivoService.createOrUpdate(args.archivo);
        return response(item);
      },
      archivoDelete: async (_, args, context) => {
        permissions(context, 'archivos:delete');

        let deleted = await ArchivoService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
