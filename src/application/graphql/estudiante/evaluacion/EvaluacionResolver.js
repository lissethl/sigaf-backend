'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { EvaluacionService } = services;

  return {
    Query: {
      evaluaciones: async (_, args, context) => {
        permissions(context, 'usuarios:read');

        const items = await EvaluacionService.findAll(args, context.rol, context.id_evaluacion);
        return items.data;
      },
      evaluacion: async (_, args, context) => {
        permissions(context, 'usuarios:read');

        const item = await EvaluacionService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      evaluacionAdd: async (_, args, context) => {
        permissions(context, 'usuarios:create');

        args.evaluacion._user_created = context.id_usuario;
        const item = await EvaluacionService.createOrUpdate(args.evaluacion);
        return item.data;
      },
      evaluacionEdit: async (_, args, context) => {
        permissions(context, 'usuarios:update');

        args.evaluacion._user_updated = context.id_usuario;
        args.evaluacion._updated_at = new Date();
        args.evaluacion.id = args.id;
        const item = await EvaluacionService.createOrUpdate(args.evaluacion);
        return item.data;
      },
      evaluacionDelete: async (_, args, context) => {
        permissions(context, 'usuarios:delete');

        const deleted = await EvaluacionService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
