'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { KardexService } = services;

  return {
    Query: {
      kardexs: async (_, args, context) => {
        permissions(context, 'usuarios:read');

        const items = await KardexService.findAll(args, context.rol, context.id_kardex);
        return items.data;
      },
      kardex: async (_, args, context) => {
        permissions(context, 'usuarios:read');

        const item = await KardexService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      kardexAdd: async (_, args, context) => {
        permissions(context, 'usuarios:create');

        args.kardex._user_created = context.id_usuario;
        const item = await KardexService.createOrUpdate(args.kardex);
        return item.data;
      },
      kardexEdit: async (_, args, context) => {
        permissions(context, 'usuarios:update');

        args.kardex._user_updated = context.id_usuario;
        args.kardex._updated_at = new Date();
        args.kardex.id = args.id;
        const item = await KardexService.createOrUpdate(args.kardex);
        return item.data;
      },
      kardexDelete: async (_, args, context) => {
        permissions(context, 'usuarios:delete');

        const deleted = await KardexService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
