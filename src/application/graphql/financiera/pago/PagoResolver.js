'use strict';

const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { PagoService } = services;

  return {
    Query: {
      pagos: async (_, args, context) => {
        permissions(context, 'pagos:read|usuarios:read');

        let lista = await PagoService.findAll(args, context.rol, context.id_usuario);
        return lista.data;
      },
      pago: async (_, args, context) => {
        permissions(context, 'pagos:read');

        let item = await PagoService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      pagoAdd: async (_, args, context) => {
        permissions(context, 'pagos:create');
        console.log('ingresa Aqui a pagoResolver',args.pago);
        args.pago._user_created = context.id_usuario;
        let item = await PagoService.createOrUpdate(args.pago, context.rol, context.id_entidad);
        return item.data;
      },
      pagoEdit: async (_, args, context) => {
        console.log(' editar datos del pago');
        permissions(context, 'pagos:update');

        args.pago._user_updated = context.id_usuario;
        args.pago._updated_at = new Date();
        args.pago.id = args.id;
        let item = await PagoService.createOrUpdate(args.pago);
        return item.data;
      },
      pagoUpdate: async (_, args, context) => {
        permissions(context, 'pagos:update');

        args.pago._user_updated = context.id_usuario;
        args.pago._updated_at = new Date();
        args.pago.id = args.id;
        let item = await PagoService.update(args.pago);
        return item.data;
      },
      pagoDelete: async (_, args, context) => {
        permissions(context, 'pagos:delete');

        let deleted = await PagoService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
