'use strict';

const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ContratoService } = services;

  return {
    Query: {
      contratos: async (_, args, context) => {
        permissions(context, 'contratos:read|usuarios:read');

        let lista = await ContratoService.findAll(args, context.rol, context.id_usuario);
        return lista.data;
      },
      contrato: async (_, args, context) => {
        permissions(context, 'contratos:read');

        let item = await ContratoService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      contratoAdd: async (_, args, context) => {
        permissions(context, 'contratos:create');
        console.log('ingresa Aqui a contrartoResolver',args.contrato);
        args.contrato._user_created = context.id_usuario;
        let item = await ContratoService.createOrUpdate(args.contrato, context.rol, context.id_entidad);
        return item.data;
      },
      contratoEdit: async (_, args, context) => {
        console.log(' editar datos del contrato');
        permissions(context, 'contratos:update');

        args.contrato._user_updated = context.id_usuario;
        args.contrato._updated_at = new Date();
        args.contrato.id = args.id;
        let item = await ContratoService.createOrUpdate(args.contrato);
        return item.data;
      },
      contratoUpdate: async (_, args, context) => {
        permissions(context, 'contratos:update');

        args.contrato._user_updated = context.id_usuario;
        args.contrato._updated_at = new Date();
        args.contrato.id = args.id;
        let item = await ContratoService.update(args.contrato);
        return item.data;
      },
      contratoDelete: async (_, args, context) => {
        permissions(context, 'contratos:delete');

        let deleted = await ContratoService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
