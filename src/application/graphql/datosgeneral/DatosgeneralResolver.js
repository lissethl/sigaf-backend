'use strict';
const { permissions } = require('../../lib/auth');

module.exports = function setupResolver (services) {
  const { DatosgeneralService } = services;

  return {
    Query: {
      datosgenerales: async (_, args, context) => {
        permissions(context, 'datosgenerales:read|usuarios:read');

        let items = await DatosgeneralService.findAll(args, context.rol, context.id_datosgeneral);
        return items.data;
      },
      datosgeneral: async (_, args, context) => {
        permissions(context, 'datosgenerales:read');

        let item = await DatosgeneralService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      datosgeneralAdd: async (_, args, context) => {
        permissions(context, 'datosgenerales:create');

        args.datosgeneral._user_created = context.id_usuario;
        let item = await DatosgeneralService.createOrUpdate(args.datosgeneral);
        return item.data;
      },
      datosgeneralEdit: async (_, args, context) => {
        permissions(context, 'datosgenerales:update');

        args.datosgeneral._user_updated = context.id_usuario;
        args.datosgeneral._updated_at = new Date();
        args.datosgeneral.id = args.id;
        let item = await DatosgeneralService.createOrUpdate(args.datosgeneral);
        return item.data;
      },
      datosgeneralDelete: async (_, args, context) => {
        permissions(context, 'datosgenerales:delete');

        let deleted = await DatosgeneralService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
