'use strict';
const { permissions } = require('../../lib/auth');

module.exports = function setupResolver (services) {
  const { ConstanteService } = services;

  return {
    Query: {
      constantes: async (_, args, context) => {
        permissions(context, 'carreras:read|requisitos:read|tipoprogramas:read|gradosacademicos:read|usuarios:read');

        let items = await ConstanteService.findAll(args, context.rol, context.id_constante);
        return items.data;
      },
      constante: async (_, args, context) => {
        permissions(context, 'carreras:read|requisitos:read|tipoprogramas:read|gradosacademicos:read');

        let item = await ConstanteService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      constanteAdd: async (_, args, context) => {
        permissions(context, 'carreras:create|requisitos:create|tipoprogramas:create|gradosacademicos:create');

        args.constante._user_created = context.id_usuario;
        let item = await ConstanteService.createOrUpdate(args.constante);
        return item.data;
      },
      constanteEdit: async (_, args, context) => {
        permissions(context, 'carreras:update|requisitos:update|tipoprogramas:update|gradosacademicos:update');

        args.constante._user_updated = context.id_usuario;
        args.constante._updated_at = new Date();
        args.constante.id = args.id;
        let item = await ConstanteService.createOrUpdate(args.constante);
        return item.data;
      },
      constanteDelete: async (_, args, context) => {
        permissions(context, 'carreras:delete|requisitos:delete|tipoprogramas:delete|gradosacademicos:delete');

        let deleted = await ConstanteService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
