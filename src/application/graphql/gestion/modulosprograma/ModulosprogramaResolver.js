'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ModulosprogramaService } = services;

  return {
    Query: {
      modulosprogramas: async (_, args, context) => {
        permissions(context, 'modulosprogramas:read|usuarios:read');

        const items = await ModulosprogramaService.findAll(args, context.rol, context.id_modulosprograma);
        return items.data;
      },
      modulosprograma: async (_, args, context) => {
        permissions(context, 'modulosprogramas:read');

        const item = await ModulosprogramaService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      modulosprogramaAdd: async (_, args, context) => {
        permissions(context, 'modulosprogramas:create');

        args.modulosprograma._user_created = context.id_usuario;
        const item = await ModulosprogramaService.createOrUpdate(args.modulosprograma);
        return item.data;
      },
      modulosprogramaEdit: async (_, args, context) => {
        permissions(context, 'modulosprogramas:update');

        args.modulosprograma._user_updated = context.id_usuario;
        args.modulosprograma._updated_at = new Date();
        args.modulosprograma.id = args.id;
        const item = await ModulosprogramaService.createOrUpdate(args.modulosprograma);
        return item.data;
      },
      modulosprogramaDelete: async (_, args, context) => {
        permissions(context, 'modulosprogramas:delete');

        const deleted = await ModulosprogramaService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
