'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { RecordService } = services;

  return {
    Query: {
      records: async (_, args, context) => {
        permissions(context, 'records:read|usuarios:read');
        
        let items = await RecordService.findAll(args, context.rol, context.id_record);
        return items.data;
      },
      record: async (_, args, context) => {
        permissions(context, 'records:read');

        let item = await RecordService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      recordAdd: async (_, args, context) => {
        permissions(context, 'records:create');

        args.record._user_created = context.id_usuario;
        let item = await RecordService.createOrUpdate(args.record);
        return item.data;
      },
      recordEdit: async (_, args, context) => {
        permissions(context, 'records:update');
        args.record._user_updated = context.id_usuario;
        args.record._updated_at = new Date();
        args.record.id = args.id;
        console.log('Datos records: ', args);
        let item = await RecordService.createOrUpdate(args.record);
        console.log('Datos records: ', item);
        return item.data;
      },
      recordDelete: async (_, args, context) => {
        permissions(context, 'records:delete');

        let deleted = await RecordService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
