'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { UniversidadService } = services;

  return {
    Query: {
      universidades: async (_, args, context) => {
        permissions(context, 'universidades:read|usuarios:read');

        let items = await UniversidadService.findAll(args, context.rol, context.id_universidad);
        return items.data;
      },
      universidad: async (_, args, context) => {
        permissions(context, 'universidades:read');

        let item = await UniversidadService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      universidadAdd: async (_, args, context) => {
        permissions(context, 'universidades:create');

        args.universidad._user_created = context.id_usuario;
        let item = await UniversidadService.createOrUpdate(args.universidad);
        return item.data;
      },
      universidadEdit: async (_, args, context) => {
        permissions(context, 'universidades:update');

        args.universidad._user_updated = context.id_usuario;
        args.universidad._updated_at = new Date();
        args.universidad.id = args.id;
        let item = await UniversidadService.createOrUpdate(args.universidad);
        return item.data;
      },
      universidadDelete: async (_, args, context) => {
        permissions(context, 'universidades:delete');

        let deleted = await UniversidadService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
