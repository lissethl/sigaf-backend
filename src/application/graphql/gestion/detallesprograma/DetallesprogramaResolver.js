'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { DetallesprogramaService } = services;

  return {
    Query: {
      detallesprogramas: async (_, args, context) => {
        permissions(context, 'detallesprogramas:read|usuarios:read');

        let items = await DetallesprogramaService.findAll(args, context.rol, context.id_detallesprograma);
        return items.data;
      },
      detallesprograma: async (_, args, context) => {
        permissions(context, 'detallesprogramas:read');

        let item = await DetallesprogramaService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      detallesprogramaAdd: async (_, args, context) => {
        permissions(context, 'detallesprogramas:create');

        args.detallesprograma._user_created = context.id_usuario;
        let item = await DetallesprogramaService.createOrUpdate(args.detallesprograma);
        return item.data;
      },
      detallesprogramaEdit: async (_, args, context) => {
        permissions(context, 'detallesprogramas:update');

        args.detallesprograma._user_updated = context.id_usuario;
        args.detallesprograma._updated_at = new Date();
        args.detallesprograma.id = args.id;
        let item = await DetallesprogramaService.createOrUpdate(args.detallesprograma);
        return item.data;
      },
      detallesprogramaDelete: async (_, args, context) => {
        permissions(context, 'detallesprogramas:delete');

        let deleted = await DetallesprogramaService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
