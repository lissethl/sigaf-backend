'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ProgramaService } = services;

  return {
    Query: {
      programas: async (_, args, context) => {
        permissions(context, 'programas:read|usuarios:read');
       
        let items = await ProgramaService.findAll(args, context.rol, context.id_programa);
        
        return items.data;
      },
      programa: async (_, args, context) => {
        permissions(context, 'programas:read');

        let item = await ProgramaService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      programaAdd: async (_, args, context) => {
        permissions(context, 'programas:create');

        args.programa._user_created = context.id_usuario;
        let item = await ProgramaService.createOrUpdate(args.programa);
        return item.data;
      },
      programaEdit: async (_, args, context) => {
        permissions(context, 'programas:update');

        args.programa._user_updated = context.id_usuario;
        args.programa._updated_at = new Date();
        args.programa.id = args.id;
        let item = await ProgramaService.createOrUpdate(args.programa);
        return item.data;
      },
      programaDelete: async (_, args, context) => {
        permissions(context, 'programas:delete');

        let deleted = await ProgramaService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};