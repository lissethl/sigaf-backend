'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { AsignacionDocenteService } = services;

  return {
    Query: {
      asignacionDocentes: async (_, args, context) => {
        permissions(context, 'asignacionDocentes:read|usuarios:read');

        let items = await AsignacionDocenteService.findAll(args, context.rol, context.id_asignacionDocente);
        console.log('DATOS', items.data);
        return items.data;
      },
      asignacionDocente: async (_, args, context) => {
        permissions(context, 'asignacionDocentes:read');

        let item = await AsignacionDocenteService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      asignacionDocenteAdd: async (_, args, context) => {
        permissions(context, 'asignacionDocentes:create');

        args.asignacionDocente._user_created = context.id_usuario;
        let item = await AsignacionDocenteService.createOrUpdate(args.asignacionDocente);
        return item.data;
      },
      asignacionDocenteEdit: async (_, args, context) => {
        permissions(context, 'asignacionDocentes:update');
        args.asignacionDocente._user_updated = context.id_usuario;
        args.asignacionDocente._updated_at = new Date();
        args.asignacionDocente.id = args.id;
        console.log('Datos asignacionDocentes: ', args);
        let item = await AsignacionDocenteService.createOrUpdate(args.asignacionDocente);
        console.log('Datos asignacionDocentes: ', item);
        return item.data;
      },
      asignacionDocenteDelete: async (_, args, context) => {
        permissions(context, 'asignacionDocentes:delete');

        let deleted = await AsignacionDocenteService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
