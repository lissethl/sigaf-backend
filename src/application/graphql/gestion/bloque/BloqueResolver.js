'use strict';
const { permissions } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { BloqueService } = services;

  return {
    Query: {
      bloques: async (_, args, context) => {
        permissions(context, 'bloques:read|usuarios:read');

        const items = await BloqueService.findAll(args, context.rol, context.id_bloque);
        return items.data;
      },
      bloque: async (_, args, context) => {
        permissions(context, 'bloques:read');

        const item = await BloqueService.findById(args.id);
        return item.data;
      }
    },
    Mutation: {
      bloqueAdd: async (_, args, context) => {
        permissions(context, 'bloques:create');

        args.bloque._user_created = context.id_usuario;
        const item = await BloqueService.createOrUpdate(args.bloque);
        return item.data;
      },
      bloqueEdit: async (_, args, context) => {
        permissions(context, 'bloques:update');

        args.bloque._user_updated = context.id_usuario;
        args.bloque._updated_at = new Date();
        args.bloque.id = args.id;
        const item = await BloqueService.createOrUpdate(args.bloque);
        return item.data;
      },
      bloqueDelete: async (_, args, context) => {
        permissions(context, 'bloques:delete');

        const deleted = await BloqueService.deleteItem(args.id);
        return { deleted: deleted.data };
      }
    }
  };
};
