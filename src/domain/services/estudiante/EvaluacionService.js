'use strict';

const debug = require('debug')('app:service:evaluacion');
const Service = require('../Service');

module.exports = function evaluacionService (repositories, valueObjects, res) {
  const { EvaluacionRepository } = repositories;

  async function findAll (params = {}) {
    debug('Lista de evaluaciones|filtros');

    return Service.findAll(params, EvaluacionRepository, res, 'Evaluaciones');
  }

  async function findById (id) {
    debug('Buscando evaluacion por ID');

    return Service.findById(id, EvaluacionRepository, res, 'Evaluaciones');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar evaluacion', data);

    let result;
    try {
      result = await EvaluacionRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando evaluacion');

    return Service.deleteItem(id, EvaluacionRepository, res, 'Evaluacion');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
