'use strict';

const debug = require('debug')('app:service:kardex');
const Service = require('../Service');

module.exports = function kardexService (repositories, valueObjects, res) {
  const { KardexRepository } = repositories;

  async function findAll (params = {}) {
    debug('Lista de kardexs|filtros');

    return Service.findAll(params, KardexRepository, res, 'Kardexs');
  }

  async function findById (id) {
    debug('Buscando kardex por ID');

    return Service.findById(id, KardexRepository, res, 'Kardexs');
  }

  async function createOrUpdate (data, idUsuario, generateKardexInfinite) {
    debug('Crear o actualizar kardex', data);

    let result;
    try {
      result = await KardexRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando kardex');

    return Service.deleteItem(id, KardexRepository, res, 'Kardex');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
