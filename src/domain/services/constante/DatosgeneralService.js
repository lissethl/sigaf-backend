'use strict';

const debug = require('debug')('app:service:Datosgeneral');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function constanteService (repositories, valueObjects, res) {
  const { DatosgeneralRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de datosgenerales|filtros');

    return Service.findAll(params, DatosgeneralRepository, res, 'Datosgenerales');
  }

  async function findById (id) {
    debug('Buscando datogeneral por ID');

    return Service.findById(id, DatosgeneralRepository, res, 'Datosgeneral');
  }

  async function createOrUpdate (data, idUsuario, generateDatosgeneralInfinite) {
    debug('Crear o actualizar datosgeneral', data);
    console.log('ingresa al al metodo: ', data);
    let datos = null;
    let result;
    try {
      result = await DatosgeneralRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }
    console.log('ejecuta el query de actualización');
    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando constante');

    return Service.deleteItem(id, DatosgeneralRepository, res, 'Datosgeneral');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
