'use strict';

const debug = require('debug')('app:service:constante');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function constanteService (repositories, valueObjects, res) {
  const { ConstanteRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de constantes|filtros');

    return Service.findAll(params, ConstanteRepository, res, 'Constantes');
  }

  async function findById (id) {
    debug('Buscando constante por ID');

    return Service.findById(id, ConstanteRepository, res, 'Constante');
  }

  async function createOrUpdate (data, idUsuario, generateConstanteInfinite) {
    debug('Crear o actualizar constante', data);

    let datos = null;
    let result;
    try {
      result = ConstanteRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando constante');

    return Service.deleteItem(id, ConstanteRepository, res, 'Constante');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
