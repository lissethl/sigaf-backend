'use strict';

const debug = require('debug')('app:service:usuario');
const moment = require('moment');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const { text } = require('../../../common');
const { generateToken } = require('../../../application/lib/auth');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');
const { mail } = require('../../../common');

module.exports = function userService (repositories, valueObjects, res) {
  const ModuloService = require('./ModuloService')(repositories, valueObjects, res);

  const { transaction, Iop, UsuarioRepository, PersonaRepository, EntidadRepository, Parametro, Log, ProgramaRepository, ArchivoRepository } = repositories;
  const {
    UsuarioUsuario,
    UsuarioContrasena,
    UsuarioEmail,
    UsuarioCargo,
    UsuarioEstado,
    PersonaNombres,
    PersonaPrimerApellido,
    PersonaSegundoApellido,
    PersonaNombreCompleto,
    PersonaTipoDocumento,
    PersonaTipoDocumentoOtro,
    PersonaNroDocumento,
    PersonaFechaNacimiento,
    PersonaMovil,
    PersonaNacionalidad,
    PersonaPaisNacimiento,
    PersonaGenero,
    PersonaTelefono,
    PersonaEstado
  } = valueObjects;

  async function findAll (params = {}, rol, idEntidad) {
    debug('Lista de usuarios|filtros');

    switch (rol) {
      case 'ADMIN':
        params.id_entidad = idEntidad;
        params.roles = ['ADMIN', 'USUARIO'];
        break;
      case 'USUARIO':
        params.id_entidad = idEntidad;
        params.roles = ['USUARIO'];
        break;
    }
    return Service.findAll(params, UsuarioRepository, res, 'Usuarios');
  }

  async function findById (id) {
    debug('Buscando usuario por ID');

    return Service.findById(id, UsuarioRepository, res, 'Usuario');
  }

  async function registrar (correo, contrasena) {
    let user;

    try {
      let usuario = {
        id_entidad: 1,
        id_rol: 5,
        usuario: correo,
        contrasena: contrasena,
        email: correo,
        estado: 'ACTIVO',
        _user_created: 1,
        _created_at: new Date()
      };

      Service.validate(usuario, {
        usuario: UsuarioEmail,
        contrasena: UsuarioContrasena,
        email: UsuarioEmail
      });

      user = await UsuarioRepository.createOrUpdate(usuario);
      user = await UsuarioRepository.findById(user.id);
    } catch (error) {
      return res.error(error);
    }

    return res.success(user);
  }

  async function createOrUpdate (data, rol = null, idEntidad = null) {
    debug('Crear o actualizar usuario', data);
    console.log('dataos usuario', data);

    let user;
    try {
      let persona = {
        id: data.id_persona,
        nombres: data.nombres,
        primer_apellido: data.primer_apellido,
        segundo_apellido: data.segundo_apellido,
        nombre_completo: data.nombre_completo,
        tipo_documento: data.tipo_documento,
        tipo_documento_otro: data.tipo_documento_otro,
        nro_documento: data.nro_documento,
        fecha_nacimiento: data.fecha_nacimiento,
        movil: data.movil,
        lugar_exp: data.lugar_exp,
        lugar_nacimiento: data.lugar_nacimiento,
        direccion: data.direccion,
        nacionalidad: data.nacionalidad,
        pais_nacimiento: data.pais_nacimiento,
        genero: data.genero,
        telefono: data.telefono
      };

      validatePerson(persona);

      if (data.id_persona) { // Actualizando persona
        persona._user_updated = data._user_updated;
        persona._updated_at = data._updated_at;
        if (data.estado_persona !== undefined) persona.estado = new PersonaEstado(data.estado_persona).value;
      } else {
        persona.estado = 'ACTIVO';
        persona._user_created = data._user_created || data._user_updated;
      }

      persona = await PersonaRepository.createOrUpdate(persona);

      let usuario = {
        id: data.id,
        id_entidad: data.id_entidad,
        id_rol: data.id_rol,
        id_persona: persona.id,
        usuario: data.usuario,
        contrasena: data.contrasena,
        email: data.email,
        cargo: data.cargo,
        g_academico: data.g_academico,
        mencion: data.mencion,
        acronimo: data.acronimo,
        tipo_docente: data.tipo_docente,
        id_universidad: data.id_universidad,
        carrera: data.carrera,
        ug_academico: data.ug_academico,
        id_programa: data.id_programa,
        id_version: data.id_version,
        ref_oferta: data.ref_oferta,
        ref_estudios: data.ref_estudios,
        idea_tesis: data.idea_tesis,
        estado_estudiante: data.estado_estudiante,
        tipo_contacto: data.tipo_contacto,
        fecha_entrevista: data.fecha_entrevista,
        e_institucional: data.e_institucional,
        c_institucional: data.c_institucional,
        requisitos: data.requisitos,
        estado: data.estado
      };

      validateUser(usuario);

      if (data.id) {
        usuario._user_updated = data._user_updated;
        usuario._updated_at = data._updated_at;
      } else {
        usuario._user_created = data._user_created;
      }
      
      user = await UsuarioRepository.createOrUpdate(usuario);
      if (data.id && user.id_rol === 5) {
        user = await UsuarioRepository.findById(data.id);
        const result = await getResponse(user);
        if (result) {
          user.token = result.token;
        }
      }
      
      if (!data.id && parseInt(user.id_rol) === 5) {
        console.log('Ingresa al If de Usuarioservice para envio de correo');
        let programa = await  ProgramaRepository.findById(user.id_programa)
        const email2 = {
          para: user.email,
          titulo: 'Postulación-CIDES',
          html: `<br> Sr(a). ${persona.nombres} ${persona.primer_apellido}, su postulación al programa <strong>${programa.nombre}</strong> fue realizada satisfactoriamente, <strong>CIDES</strong> se contactará con usted. Gracias.`
          // attachments: data.attachment
        };
        console.log('Ingresa Correo', user.email);
        try {
          let correo = await mail.enviar(email2); 
          console.log('correo', correo)
        } catch (e) {
          return(res.warning(new Error('No se pudo enviar el correo de confirmación. Sin embargo, se registró su postulación. CIDES se contactará con usted.')));
          // Aqui quiero salir de la sesión :D
        }
        /*return res.success(correo);*/
      }

      if (user.e_institucional && user.c_institucional) {
        console.log('Ingresa al If de Usuarioservice para envio de correo');
        let programa = await  ProgramaRepository.findById(user.id_programa)
        const email3 = {
          para: user.email,
          titulo: 'Credeciales CIDES - Google Meet ',
          html: `<br> Sr(a). ${persona.nombres} ${persona.primer_apellido}, las credenciales de Google Meet del programa <strong>${programa.nombre}</strong> son: <br><strong>Correo Institucional: </strong> ${user.e_institucional}</br> <br><strong>Contraseña: </strong> ${user.c_institucional}</br>`
          // attachments: data.attachment
        };
        console.log('Ingresa Correo', user.email);
        let correo = await mail.enviar(email3);
        console.log('correo', correo)
        debug('Respuesta envio correo', correo);
        if (!correo) {
          return res.error(new Error('No se pudo enviar el correo'));
        }
      } 
      
    } catch (e) {
      if (e.message.indexOf('Este valor ya fue registrado anteriormente') !== -1) {
        console.log('ERROR', e.message);
        return res.warning(e);
      }
      return res.error(e);
    }
    
    if (!user) {
      return res.warning(new Error(`El usuario no pudo ser creado`));
    }    
    return res.success(user);
  }
  
  async function update (data) {
    debug('Actualizar usuario');

    if (!data.id) {
      return res.error(new Error(`Se necesita el ID del usuario para actualizar el registro`));
    }

    let user;
    try {
      validateUser(data);
      user = await UsuarioRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.warning(new Error(`El usuario no pudo ser actualizado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando usuario');

    return Service.deleteItem(id, UsuarioRepository, res, 'Usuario');
  }

  async function userExist (usuario, contrasena, nit) {
    debug(`Comprobando si el usuario ${usuario} existe`);

    let result;
    let t;
    try {
      result = nit ? await EntidadRepository.findByNit(nit) : await UsuarioRepository.findByUsername(usuario);
      if (!nit && !result) {
        return res.error(new Error(`No existe el usuario ${usuario}`));
      }
      if (nit && !result) {
        return res.error(new Error(`La entidad con el NIT ${nit} no esta registrada.`));
      }
      if (nit && result) {
        await verifySIN(usuario, contrasena, nit);
        if (result.id_usuario) {
          result = await UsuarioRepository.findById(result.id_usuario);
        } else {
          debug('Creando usuario administrador para la entidad', result.sigla);
          t = await transaction.create();
          const idEntidad = result.id;
          const sigla = result.sigla;

          // Creando usuario administrador de la entidad
          const dataUser = {
            usuario: sigla + '-' + Math.random().toString(36).slice(2),
            contrasena: sigla + '-' + Math.random().toString(36).slice(2),
            id_entidad: idEntidad,
            id_rol: 2,
            _user_created: 1
          };
          result = await UsuarioRepository.createOrUpdate(dataUser, t);

          await transaction.commit(t); // Creando usuario

          // Actualizando nuevo usuario administrador de la entidad
          const dataEntidad = {
            id: idEntidad,
            id_usuario: result.id,
            usuario
          };
          await EntidadRepository.createOrUpdate(dataEntidad, t);

          result = await UsuarioRepository.findByUsername(dataUser.usuario);
        }
      }

      let minutos = await Parametro.getParam('TIEMPO_BLOQUEO');
      minutos = minutos.valor && !isNaN(minutos.valor) ? parseInt(minutos.valor) : 0;

      let nroMaxIntentos = await Parametro.getParam('NRO_MAX_INTENTOS');
      nroMaxIntentos = nroMaxIntentos.valor && !isNaN(nroMaxIntentos.valor) ? parseInt(nroMaxIntentos.valor) : 3;

      if (result.fecha_bloqueo) {
        let tiempo = moment(result.fecha_bloqueo).valueOf();
        let now = moment().valueOf();
        debug('FECHA BLOQUEO', moment(tiempo).format('YYYY-MM-DD HH:mm:ss'), 'FECHA ACTUAL', moment(now).format('YYYY-MM-DD HH:mm:ss'));
        if (now < tiempo) {
          return res.error(new Error(`El usuario <strong>${usuario}</strong> se encuentra bloqueado por <strong>${minutos} minutos</strong> por demasiados intentos fallidos.`));
        } else {
          await update({ id: result.id, nro_intentos: 0, fecha_bloqueo: null });
        }
      }

      if (!nit && result.contrasena !== text.encrypt(contrasena)) {
        if (result.nro_intentos !== undefined && !isNaN(result.nro_intentos)) {
          let intentos = parseInt(result.nro_intentos) + 1;
          debug('NRO. INTENTO', intentos, 'MAX. NRO. INTENTOS', nroMaxIntentos);
          if (intentos >= nroMaxIntentos) {
            await update({
              id: result.id,
              nro_intentos: intentos,
              fecha_bloqueo: moment().add(minutos, 'minutes').format('YYYY-MM-DD HH:mm:ss')
            });
          } else {
            await update({ id: result.id, nro_intentos: intentos });
          }
        }
        return res.error(new Error(`La contraseña del usuario ${usuario} es incorrecta`));
      }

      if (result.estado === 'INACTIVO') {
        return res.error(new Error(`El usuario ${usuario} se encuentra deshabilitado. Consulte con el administrador del sistema.`));
      }

      return res.success(result);
    } catch (e) {
      if (t) {
        await transaction.rollback(t);
      }

      return res.error(e);
    }
  }

  async function getUser (usuario, include = true) {
    debug('Buscando usuario por nombre de usuario');

    let user;
    try {
      user = await UsuarioRepository.findByUsername(usuario, include);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.warning(new Error(`Usuario ${usuario} not found`));
    }

    return res.success(user);
  }

  async function verifySIN (usuario, contrasena, nit) {
    try {
      const loginSIN = await Iop.sin.login(nit, usuario, contrasena);
      if (loginSIN.data.Estado === 'ACTIVO HABILITADO') {
        return loginSIN;
      }
      throw new Error('El NIT se encuentra INACTIVO en la Plataforma del Sistema de Impuestos Nacionales.');
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async function getResponse (user, info = {}) {
    let respuesta;

    try {
      const usuario = user.usuario;
      // Actualizando el último login
      const now = moment().format('YYYY-MM-DD HH:mm:ss');
      await update({
        id: user.id,
        ultimo_login: now
      });
      let text = '';
      if (info.location) {
        text += `Location: ${info.location.country} -- ${info.location.city} <br />`;
      }
      if (info.navigator) {
        text += `Navigator: ${info.navigator}`;
      }
      Log.info(`El usuario: ${usuario} ingresó al sistema a las ${now}`, 'LOGIN', text, usuario, info.ip);

      // Obteniendo menu
      let menu = await ModuloService.getMenu(user.id_rol);
      let permissions = menu.data.permissions;
      menu = menu.data.menu;

      // Generando token
      let token = await generateToken(Parametro, usuario, permissions);

      // Formateando permisos
      let permisos = {};
      permissions.map(item => (permisos[item] = true));

      respuesta = {
        menu,
        token,
        permisos,
        usuario: {
          'id': user.id,
          'usuario': user.usuario,
          'nombres': user.persona ? user.persona.nombres : '',
          'primer_apellido': user.persona ? user.persona.primer_apellido : '',
          'segundo_apellido': user.persona ? user.persona.segundo_apellido : '',
          'email': user.email,
          'id_entidad': user.id_entidad,
          'entidad': user.entidad.nombre,
          'rol': user.rol.nombre,
          'lang': 'es',
          'estado_estudiante': user.estado_estudiante
        },
        redirect: user.rol.path
      };
      return respuesta;
    } catch (e) {
      console.log(e);
      throw new Error(e.message);
    }
  }

  async function regenerar (id, idUsuario) {
    debug('Regenerar contraseña');
    try {
      let datos = await UsuarioRepository.findById(id);
      console.log('Regenarar contrasenia: ', datos);
      if (!datos && !datos.id) {
        return res.warning(new Error('El usuario no esta registrado'));
      }
      if (!datos.email) {
        return res.warning(new Error('El usuario no cuenta con un email registrado'));
      }
      const contrasena = crypto.randomBytes(4).toString('hex');
      const data = {
        id,
        contrasena,
        _user_updated: idUsuario
      };
      await UsuarioRepository.createOrUpdate(data);

      const email = {
        para: datos.email,
        titulo: 'Nueva contraseña - CIDES - SIGAF',
        html: `<br> Los datos para el ingreso a nuestro Sistema de Gestión Academica son:<br> usuario: <strong>${datos.usuario}</strong> <br> nueva contraseña: <strong>${contrasena}</strong> <br>`
        // attachments: data.attachments
      };
      let correo = await mail.enviar(email);
      debug('Respuesta envio correo', correo);
      if (!correo) {
        return res.error(new Error('No se pudo enviar el correo'));
      }
      return res.success(correo);
    } catch (e) {
      return res.error(e);
    }
  }

  function validatePerson (data) {
    Service.validate(data, {
      nombres: PersonaNombres,
      primer_apellido: PersonaPrimerApellido,
      segundo_apellido: PersonaSegundoApellido,
      nombre_completo: PersonaNombreCompleto,
      tipo_documento: PersonaTipoDocumento,
      tipo_documento_otro: PersonaTipoDocumentoOtro,
      nro_documento: PersonaNroDocumento,
      fecha_nacimiento: PersonaFechaNacimiento,
      movil: PersonaMovil,
      nacionalidad: PersonaNacionalidad,
      pais_nacimiento: PersonaPaisNacimiento,
      genero: PersonaGenero,
      telefono: PersonaTelefono
    });
  }

  function validateUser (data) {
    Service.validate(data, {
      usuario: UsuarioUsuario,
      //contrasena: UsuarioContrasena,
      email: UsuarioEmail,
      cargo: UsuarioCargo,
      estado: UsuarioEstado
    });
  }

  async function findStudentUsers (params = {}, estado_estudiante, idPrograma,idVersion) {
    console.log('prueba ------> ', estado_estudiante);
    debug('Lista de estudiantes|filtros');
    params.estado_estudiante = estado_estudiante;
    params.id_programa = idPrograma;
    params.id_version = idVersion;
    console.log('prueba ------> ', params.estado_estudiante)
    return Service.findAll(params, UsuarioRepository, res, 'Usuarios');
  }

  async function uploadFoto (file, idUser, idUsuario) {
    debug('Adjuntando foto');

    let archivo;
    try {
      const datetimestamp = Date.now();
      const filename = `foto-usuario-${datetimestamp}.${file.name.split('.').pop()}`;
      const filepath = `uploads/${filename}`;
      const filesize = (file.size / 1024).toFixed(2);

      await file.mv(path.join(global.appRoot, filepath));
      if (!fs.existsSync(path.join(global.appRoot, filepath))) {
        throw new Error('Se produjo un error al subir el archivo.');
      }

      // crear archivo en la base de datos
      archivo = await ArchivoRepository.createOrUpdate({
        filename: filename,
        path: filepath,
        es_imagen: filepath.indexOf('.pdf') === -1,
        size: filesize,
        _user_created: idUser,
        _created_at: new Date()
      });

      if (!archivo) {
        return res.error(new Error('Ocurrió un error al subir la información de archivo!'));
      }

      if (idUsuario) {
        await UsuarioRepository.createOrUpdate({ id: idUsuario, id_foto: archivo.id });
        archivo.id_usuario = idUsuario;
      }
    } catch (e) {
      return res.error(e);
    }

    if (!archivo) {
      return res.error(new Error('Archivo no importado'));
    }

    return res.success(archivo);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    userExist,
    getUser,
    update,
    getResponse,
    regenerar,
    findStudentUsers,
    registrar,
    uploadFoto
  };
};
