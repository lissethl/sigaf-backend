'use strict';

const debug = require('debug')('app:service:usuario');
const moment = require('moment');
const crypto = require('crypto');
const { text } = require('../../../common');
const Service = require('../Service');
const { mail } = require('../../../common');

module.exports = function pagoService (repositories, valueObjects, res) {
  const { ContratoRepository, PagoRepository } = repositories;
  

  async function findAll (params = {}) {
    debug('Lista de pagos|filtros');
    return Service.findAll(params, PagoRepository, res, 'Pagos');
  }

  async function findById (id) {
    debug('Buscando usuario por ID');

    return Service.findById(id, PagoRepository, res, 'Pago');
  }

  async function createOrUpdate (data) {
    console.log('Crear o actualizar pago', data);
    let result;
   
    try {
      result = await PagoRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
      console.log('error:', error);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando pago');

    return Service.deleteItem(id, PagoRepository, res, 'Pago');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
