'use strict';

const debug = require('debug')('app:service:usuario');
const moment = require('moment');
const crypto = require('crypto');
const { text } = require('../../../common');
const Service = require('../Service');
const { mail } = require('../../../common');

module.exports = function contratoService (repositories, valueObjects, res) {
  const { ContratoRepository, PersonaRepository, UsuarioRepository, DatosgeneralRepository,PagoRepository } = repositories;
  

  async function findAll (params = {}) {
    debug('Lista de contratos|filtros');
    if (params.pago) {
      let contratos = await ContratoRepository.findAll(params);
      for (let item of contratos.rows) {
        let pagos = await PagoRepository.findAll({id_contrato: item.id});
        if (pagos.count > 0) {
          item.pago  =  [];
          for(let p of pagos.rows) {
            item.pago.push([     
                p.nro_cuota,
                p.monto_cuota,
                p.fecha_vencimiento
            ]);
          }     
          console.log(item.pago);
        }
      }
      return res.success(contratos);
    } else {
      return Service.findAll(params, ContratoRepository, res, 'Contratos');s
    }
  }

  async function findById (id) {
    debug('Buscando usuario por ID');

    return Service.findById(id, ContratoRepository, res, 'Contrato');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar contrato', data);

    let contract;
    let datosPersona;

    let contrato = {
      id: data.id,
      // id_persona: persona.id,
      id_usuario: data.id_usuario,
      id_programa: data.id_programa,
      id_detallesprograma: data.id_detallesprograma,
      director_cides: data.director_cides,
      coordinador_programa: data.coordinador_programa,
      opcion_pago: data.opcion_pago,
      descuento: data.descuento,
      monto_descuento: data.monto_descuento,
      nro_cuotas: data.nro_cuotas,
      estado: data.estado,
      costo_colegiatura_total: data.costo_colegiatura_total,
      costo_colegiatura: data.costo_colegiatura,
      cuota_inicial: data.cuota_inicial,
      monto_cuotas: data.monto_cuotas,
      monto_reserva: data.monto_reserva,
      fecha_reserva: data.fecha_reserva,
      nro_matricula_1: data.nro_matricula_1,
      monto_matricula_1: data.monto_matricula_1,
      nro_matricula_2: data.nro_matricula_2,
      monto_matricula_2: data.monto_matricula_2,
      nro_matricula_3: data.nro_matricula_3,
      monto_matricula_3: data.monto_matricula_3,
      concepto_reserva: data.concepto_reserva,
      monto_devolucion: data.monto_devolucion,
      fecha_devolucion: data.fecha_devolucion,
      plan_pagos: data.plan_pagos,
      observacion: data.observacion,
      moneda_reserva: data.moneda_reserva,
      literal_reserva: data.literal_reserva,
      nro_recibo: data.nro_recibo,
      wsid_persona: data.wsid_persona,
      wsid_estudiante: data.wsid_estudiante
    };

    try {
      if (data.id) { 
        // Actualizar contrato
        contrato._user_updated = data._user_updated;
        contrato._updated_at = data._updated_at;
        // Actualizar datos del contrato
      } else { // Crear contrato
        // buscar a usuario para obtener su id_persona de usuario
        console.log('Crear un contrato nuevo:', contrato);
        let datosUsuario = await UsuarioRepository.findById(contrato.id_usuario);
        console.log('llega a createOrUpdate',  contrato.id_usuario);
        // Crear nueva persona con los datos encontrados
        datosUsuario = datosUsuario.persona;
        let persona = {
          nombres: datosUsuario.nombres,
          primer_apellido: datosUsuario.primer_apellido,
          segundo_apellido: datosUsuario.segundo_apellido,
          nombre_completo: datosUsuario.nombre_completo,
          tipo_documento: datosUsuario.tipo_documento,
          tipo_documento_otro: datosUsuario.tipo_documento_otro,
          nro_documento: datosUsuario.nro_documento,
          fecha_nacimiento: datosUsuario.fecha_nacimiento,
          movil: datosUsuario.movil,
          lugar_exp: datosUsuario.lugar_exp,
          lugar_nacimiento: datosUsuario.lugar_nacimiento,
          direccion: datosUsuario.direccion,
          nacionalidad: datosUsuario.nacionalidad,
          pais_nacimiento: datosUsuario.pais_nacimiento,
          genero: datosUsuario.genero,
          telefono: datosUsuario.telefono
        };
        persona.estado = 'ACTIVO';
        persona._user_created = data._user_created;
        persona._created_at = new Date();
        console.log('persons1: ', persona);
        datosPersona = await PersonaRepository.createOrUpdate(persona);
        console.log('persons2: ', datosPersona);
        // asignar el nuevo id de la persona creada
        contrato.id_persona = datosPersona.id
        console.log('contrato.id_persona: ', datosPersona.id);
        // data.id_persona = id;
        // Crear el contrato con el id_persona
        contrato._user_created = data._user_created;
        console.log('user_created_at', contrato._user_created);
        contrato._created_at = new Date();
        if(data.monto_reserva) {
          let gestionActual = new Date().getFullYear();
          console.log('anio actual: ', gestionActual);
          console.log('fecha actual', new Date() );
          let mesActual = new Date().getMonth() + 1;
          console.log('obtiene mes: ', mesActual);
          let datosGenerales = await DatosgeneralRepository.findAll({ 
            gestion: gestionActual
          });
          console.log('PRUEBA DE CORRELATIVO: ', datosGenerales.rows[0].cnro_recibo );
          contrato.nro_recibo = 'R' + gestionActual + mesActual+ '00' + datosGenerales.rows[0].cnro_recibo + '';
          let correlativo_actual = datosGenerales.rows[0].cnro_recibo + 1;
          let updateDatosGenerales = await DatosgeneralRepository.createOrUpdate({
            id: datosGenerales.rows[0].id,
            cnro_recibo: correlativo_actual
          });
          console.log('se incrementa el correlativo del recibo');
        }
      } 
      contract = await ContratoRepository.createOrUpdate(contrato);
    } catch (e) {
      return res.error(e);
      console.log('error:', error);
    }

    if (!contract) {
      return res.warning(new Error(`El contrato no pudo ser creado`));
    }

    return res.success(contract);
  }

  async function deleteItem (id) {
    debug('Eliminando contrato');

    return Service.deleteItem(id, ContratoRepository, res, 'Contrato');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
