'use strict';

const debug = require('debug')('app:service:asignacionDocente');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function asignacionDocenteService (repositories, valueObjects, res) {
  const { AsignacionDocenteRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de asignacionDocentes|filtros');
    console.log('Service',  params )
    return Service.findAll(params, AsignacionDocenteRepository, res, 'AsigancionDocentes');
  }

  async function findById (id) {
    debug('Buscando asignacionDocente por ID');

    return Service.findById(id, AsignacionDocenteRepository, res, 'AsignacionDocente');
  }

  async function createOrUpdate (data, idUsuario, generateDetallesprogramaInfinite) {
    debug('Crear o actualizar asignacionDocente', data);
    console.log('Crear o actualizar asignacionDocente', data);

    let datos = null;
    let result;
    try {
      result = await AsignacionDocenteRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando asignacionDocente');
    return Service.deleteItem(id, AsignacionDocenteRepository, res, 'AsignacionDocente');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
