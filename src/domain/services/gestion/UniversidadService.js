'use strict';

const debug = require('debug')('app:service:universidad');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function universidadService (repositories, valueObjects, res) {
  const { UniversidadRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de universidades|filtros');

    return Service.findAll(params, UniversidadRepository, res, 'Universidades');
  }

  async function findById (id) {
    debug('Buscando universidad por ID');

    return Service.findById(id, UniversidadRepository, res, 'Universidad');
  }

  async function createOrUpdate (data, idUsuario, generateUniversidadInfinite) {
    debug('Crear o actualizar universidad', data);

    let datos = null;
    let result;
    try {
      result = await UniversidadRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando universidad');

    return Service.deleteItem(id, UniversidadRepository, res, 'Universidad');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
