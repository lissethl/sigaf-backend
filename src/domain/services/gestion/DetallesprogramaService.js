'use strict';

const debug = require('debug')('app:service:detallesprograma');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function detallesprogramaService (repositories, valueObjects, res) {
  const { DetallesprogramaRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de detallesprogramas|filtros');
    console.log('Service',  params )
    return Service.findAll(params, DetallesprogramaRepository, res, 'Modulosprogramas');
  }

  async function findById (id) {
    debug('Buscando detallesprograma por ID');

    return Service.findById(id, DetallesprogramaRepository, res, 'Modulosprograma');
  }

  async function createOrUpdate (data, idUsuario, generateDetallesprogramaInfinite) {
    debug('Crear o actualizar detallesprograma', data);

    let datos = null;
    let result;
    try {
      result = await DetallesprogramaRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando detallesprograma');

    return Service.deleteItem(id, DetallesprogramaRepository, res, 'Programa');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
