'use strict';

const debug = require('debug')('app:service:programa');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function programaService (repositories, valueObjects, res) {
  const { ProgramaRepository, DetallesprogramaRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de programas|filtros');

    if (params.version) {
      let programas = await ProgramaRepository.findAll(params);
      for (let item of programas.rows) {
        let versiones = await DetallesprogramaRepository.findAll({ id_programa: item.id });
        if (versiones.count > 0) {
          item.version = versiones.rows[0];
        }
      }
      return res.success(programas);
    } else {
      return Service.findAll(params, ProgramaRepository, res, 'Programas');
    }
  }

  async function findById (id) {
    debug('Buscando programa por ID');

    return Service.findById(id, ProgramaRepository, res, 'Programa');
  }

  async function createOrUpdate (data, idUsuario, generateProgramaInfinite) {
    debug('Crear o actualizar programa', data);

    let datos = null;
    let result;
    try {
      result = await ProgramaRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando programa');

    return Service.deleteItem(id, ProgramaRepository, res, 'Programa');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
