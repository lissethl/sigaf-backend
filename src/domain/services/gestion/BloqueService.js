'use strict';

const debug = require('debug')('app:service:bloque');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function bloqueService (repositories, valueObjects, res) {
  const { BloqueRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de bloques|filtros');
    
    return Service.findAll(params, BloqueRepository, res, 'Bloques');
  }

  async function findById (id) {
    debug('Buscando bloque por ID');

    return Service.findById(id, BloqueRepository, res, 'Bloque');
  }

  async function createOrUpdate (data, idUsuario, generateBloqueInfinite) {
    debug('Crear o actualizar bloque', data);

    let datos = null;
    let result;
    try {
      result = await BloqueRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando bloque');

    return Service.deleteItem(id, BloqueRepository, res, 'Bloque');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
