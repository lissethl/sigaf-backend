'use strict';

const debug = require('debug')('app:service:record');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function recordService (repositories, valueObjects, res) {
  const { RecordRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de records|filtros');
    console.log('Service',  params )
    return Service.findAll(params, RecordRepository, res, 'Records');
  }

  async function findById (id) {
    debug('Buscando record por ID');

    return Service.findById(id, RecordRepository, res, 'Record');
  }

  async function createOrUpdate (data, idUsuario, generateRecordInfinite) {
    let datos = null;
    let result;
    try {
      result = await RecordRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    return Service.deleteItem(id, RecordRepository, res, 'Record');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
