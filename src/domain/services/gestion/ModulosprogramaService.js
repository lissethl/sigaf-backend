'use strict';

const debug = require('debug')('app:service:modulosprograma');
const ClienteNotificaciones = require('app-notificaciones');
const Service = require('../Service');

module.exports = function modulosprogramaService (repositories, valueObjects, res) {
  const { ModulosprogramaRepository } = repositories

  async function findAll (params = {}) {
    debug('Lista de modulosprogramas|filtros');
    
    return Service.findAll(params, ModulosprogramaRepository, res, 'Modulosprogramas');
  }

  async function findById (id) {
    debug('Buscando modulosprograma por ID');

    return Service.findById(id, ModulosprogramaRepository, res, 'Modulosprograma');
  }

  async function createOrUpdate (data, idUsuario, generateModuloprogramaInfinite) {
    debug('Crear o actualizar modulosprograma', data);

    let datos = null;
    let result;
    try {
      result = await ModulosprogramaRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    return res.success(result);
  }

  async function deleteItem (id) {
    debug('Eliminando modulosprograma');

    return Service.deleteItem(id, ModulosprogramaRepository, res, 'Modulosprograma');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
