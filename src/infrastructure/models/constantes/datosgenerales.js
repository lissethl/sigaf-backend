
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    dir_acronimo:{
      type: DataTypes.STRING(10),
      allowNull: false
    },
    nom_director:{
      type: DataTypes.STRING(150),
      allowNull: false
    },
    dir_descripcion:{
      type: DataTypes.TEXT
    },
    subd_acronimo:{
      type: DataTypes.STRING(10),
      allowNull: false
    },
    nom_subdirector: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    sdir_descripcion: {
      type: DataTypes.TEXT
    },
    aux_1: {
        type: DataTypes.STRING(150),
        allowNull: true
      },
    aux_2: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gestion: {
      type: DataTypes.STRING
    },
    ccn_financiera: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    ccm_academica: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    ccn_academica: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    cr_notas: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    cc_aceptacion: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    cnro_recibo: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Datosgenerales = sequelize.define('datosgenerales', fields, {
    timestamps: false,
    tableName: 'cons_datosgenerales'
  });
  return Datosgenerales;
};
