
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    grupo:{
      type: DataTypes.STRING(50),
      allowNull: false
    },
    codigo:{
      type: DataTypes.STRING(20),
      allowNull:false
    },
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT
    },
    orden: {
      type: DataTypes.INTEGER
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO'],
      defaultValue: 'ACTIVO'
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Constantes = sequelize.define('constantes', fields, {
    timestamps: false,
    tableName: 'sys_constantes'
  });

  return Constantes;
};
