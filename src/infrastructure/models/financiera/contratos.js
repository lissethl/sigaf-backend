
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    director_cides:{
      type: DataTypes.STRING(255)
    },
    coordinador_programa: {
      type: DataTypes.STRING(255)
    }, 
    opcion_pago: {
      type: DataTypes.STRING(100)
    },
    descuento: {
      type: DataTypes.INTEGER
    },
    monto_descuento: {
      type: DataTypes.DECIMAL(10,2)
    },
    nro_cuotas: {
      type: DataTypes.INTEGER
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['VIGENTE','PAGADO','ANULADO','RECHAZADO','PENDIENTE','MORA','INICIAL'],
      defaultValue: 'INICIAL'
    },
    costo_colegiatura_total: {
      type: DataTypes.DECIMAL(10,2)
    },
    costo_colegiatura: {
      type: DataTypes.DECIMAL(10,2)
    },
    cuota_inicial: {
      type: DataTypes.DECIMAL(10,2)
    },
    monto_cuotas: {
      type: DataTypes.DECIMAL(10,2)
    },
    monto_reserva: {
      type: DataTypes.DECIMAL(10,2)
    },
    literal_reserva: {
      type: DataTypes.STRING(200)
    },
    moneda_reserva: {
      type: DataTypes.STRING(20)
    },
    fecha_reserva: {
      type: DataTypes.DATEONLY
    },
    nro_recibo: {
      type: DataTypes.STRING(20)
    },
    nro_matricula_1: {
      type: DataTypes.STRING(20)
    },
    monto_matricula_1: {
      type: DataTypes.DECIMAL(10,2)
    },
    nro_matricula_2: {
      type: DataTypes.STRING(20)
    },
    monto_matricula_2: {
      type: DataTypes.DECIMAL(10,2)
    },
    nro_matricula_3: {
      type: DataTypes.STRING(20)
    },
    monto_matricula_3: {
      type: DataTypes.DECIMAL(10,2)
    },
    concepto_reserva: {
      type: DataTypes.STRING(255)
    },
    monto_devolucion: {
      type: DataTypes.DECIMAL(10,2)
    },
    fecha_devolucion: {
      type: DataTypes.DECIMAL(10,2)
    },
    plan_pagos: {
      type: DataTypes.JSON
    },
    observacion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.observacion')
    }, 
    sia: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      xlabel: lang.t('fields.sia')
    },
    sia_id_persona: {
      type: DataTypes.STRING(20),
      defaultValue: false,
      xlabel: lang.t('fields.sia_id_persona')
    },
    sia_id_estudiante: {
      type: DataTypes.STRING(20),
      defaultValue: false,
      xlabel: lang.t('fields.sia')
    }, 
    wsid_persona: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.wsid_persona')
    },
    wsid_estudiante: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.wsid_estudiante')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Contratos = sequelize.define('contratos', fields, {
    timestamps: false,
    tableName: 'fin_contratos'
  });

  return Contratos;
};
