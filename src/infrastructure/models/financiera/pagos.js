
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nro_cuota:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    monto_cuota: {
      type: DataTypes.DECIMAL(10,2)
    },
    saldo_actual: {
      type: DataTypes.DECIMAL(10,2)
    }, 
    saldo_anterior: {
      type: DataTypes.DECIMAL(10,2)
    },
    fecha_vencimiento: {
      type: DataTypes.DATEONLY
    }, 
    sia_fecha_pago: {
      type: DataTypes.DATEONLY
    },
    sia_factura: {
      type: DataTypes.STRING(25)
    },
    sia_codigo_control: {
      type: DataTypes.STRING(50)
    },
    sia_razon_social: {
      type: DataTypes.STRING(300)
    },
    sia_monto: {
      type: DataTypes.STRING(20)
    }     
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Pagos = sequelize.define('pagos', fields, {
    timestamps: false,
    tableName: 'fin_planpagos'
  });
  
  return Pagos;
};
