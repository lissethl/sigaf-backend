
'use strict';

const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    evaluacion: {
      type: DataTypes.JSONB
    },
    tipo: {
      type: DataTypes.ENUM('ALUMNO', 'DOCENTE')
    },
    estado: {
      type: DataTypes.ENUM('PENDIENTE', 'EVALUADO')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  const Evaluaciones = sequelize.define('evaluaciones', fields, {
    timestamps: false,
    tableName: 'est_evaluaciones'
  });

  return Evaluaciones;
};
