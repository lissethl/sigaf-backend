
'use strict';

const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nro_matricula: {
      type: DataTypes.STRING
    },
    fecha_inscripcion: {
      type: DataTypes.DATEONLY
    },
    area_nacimiento: {
      type: DataTypes.ENUM,
      values: ['URBANO', 'RURAL', 'URBANO_PROVINCIAL']
    },
    lugar_residencia: {
      type: DataTypes.STRING
    },
    nombre_colegio: {
      type: DataTypes.STRING
    },
    edad: {
      type: DataTypes.INTEGER
    },
    idiomas: {
      type: DataTypes.JSON
    },
    estado_civil: {
      type: DataTypes.ENUM,
      values: ['SOLTERO', 'CASADO', 'DIVORCIADO', 'VIUDO', 'OTROS']
    },
    residencia: {
      type: DataTypes.ENUM,
      values: ['SI', 'NO', 'NOCORRESPONDE']
    },
    casilla: {
      type: DataTypes.STRING(255)
    },
    razon_social: {
      type: DataTypes.STRING(255)
    },
    t_direccion: {
      type: DataTypes.STRING(255)
    },
    t_telefono: {
      type: DataTypes.STRING(100)
    },
    t_email: {
      type: DataTypes.STRING(255)
    },
    anio_egreso: {
      type: DataTypes.STRING(4)
    },
    area_colegio: {
      type: DataTypes.ENUM,
      values: ['URBANO', 'RURAL', 'URBANO_PROVINCIAL']
    },
    admin_colegio: {
      type: DataTypes.ENUM,
      values: ['FISCAL', 'PARTICULAR', 'CEMA']
    },
    turno_colegio: {
      type: DataTypes.ENUM,
      values: ['DIURNO', 'NOCTURNO']
    },
    univ_lugar: {
      type: DataTypes.STRING(100)
    },
    univ_anio_inicio: {
      type: DataTypes.STRING(4)
    },
    univ_anio_fin: {
      type: DataTypes.STRING(4)
    },
    id_universidad: {
      type: DataTypes.INTEGER
    },
    u_titulacion: {
      type: DataTypes.ENUM,
      values: ['LICENCIATURA', 'MAESTRIA', 'MAESTRIA_EN_ARTES', 'BACHEL_BRASIL', 'BACHEL_CS_USA)', 'BACHEL_ARTES)', 'DOCTORADO', 'OTROS']
    },
    estado_titulo: {
      type: DataTypes.ENUM,
      values: ['PROVISION_NACIONAL', 'ACADEMICO', 'REVALIDADO', 'OTROS']
    },
    profesion: {
      type: DataTypes.STRING(255)
    },
    estudios: {
      type: DataTypes.JSON
    },
    d_titulos: {
      type: DataTypes.JSONB
    },
    d_pregrados: {
      type: DataTypes.JSONB
    },
    d_postgrados: {
      type: DataTypes.JSONB
    },
    d_pintelectual: {
      type: DataTypes.JSONB
    },
    d_cursosr: {
      type: DataTypes.JSONB
    },
    d_cursosd: {
      type: DataTypes.JSONB
    },
    d_experiencia: {
      type: DataTypes.JSONB
    },
    d_lineas: {
      type: DataTypes.JSONB
    },
    d_tematica: {
      type: DataTypes.STRING
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  const Kardexs = sequelize.define('kardexs', fields, {
    timestamps: false,
    tableName: 'est_kardexs'
  });

  return Kardexs;
};
