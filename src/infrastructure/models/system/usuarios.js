'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    usuario: {
      type: DataTypes.STRING(100),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.usuario')
    },
    contrasena: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.contrasena')
    },
    email: {
      type: DataTypes.STRING(100),
      unique: true,
      xlabel: lang.t('fields.email')
    },
    email_verified_at: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.email_verified_at')
    },
    cargo: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cargo')
    },
    g_academico: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.g_academico')  
    },
    mencion: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.mencion')
    },
    acronimo: {
      type: DataTypes.STRING(10),
      xlabel: lang.t('fields.acronimo')
    },
    tipo_docente: {
      type: DataTypes.ENUM,
      xlabel: lang.t('field.tipo_docente'),
      defaultValue: 'N',
      values: ['N','E']
    },
    id_universidad: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      xlabel: lang.t('fields.id_universidad')
    },
    carrera: {
      type: DataTypes.STRING(10),
      xlabel: lang.t('fields.carrera')
    },
    ug_academico: {
      type: DataTypes.STRING(10),
      xlabel: lang.t('fields.academico')
    },    
    ref_oferta: {
      type: DataTypes.ENUM,
      defaultValue: 'O',
      values: ['I','R','M','P','O'],
      xlabel: lang.t('fields.ref_oferta')      
    },
    ref_estudios: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.ref_estudios')
    },
    idea_tesis: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.idea_tesis')
    },
    estado_estudiante: {
      type: DataTypes.ENUM,
      defaultValue: 'NINGUNO',
      values: ['POSTULANTE','PREINSCRITO','INSCRITO','REGULAR','RESAGADO','RECHAZADO','CONGELADO','CONCLUIDO', 'NINGUNO', 'BAJA', 'CIERREPROGRAMA'], 
      xlabel: lang.t('fields.estado_estudiante')
    },
    tipo_contacto:{
      type: DataTypes.ENUM,
      defaultValue: 'C',
      values: ['T','C'], 
      xlabel: lang.t('fields.tipo_contacto')
    },
    fecha_entrevista: {
      type: DataTypes.DATEONLY,
      xlabel: lang.t('fields.fecha_entrevista')
    },
    hora_entrevista: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.hora_entrevista')
    },
    lugar_entrevista: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.lugar_entrevista')
    },
    requisitos: {
      type: DataTypes.JSON,
      xlabel: lang.t('fields.requisitos')
    },
    e_institucional: {
      type: DataTypes.STRING(100),
      unique: true,
      xlabel: lang.t('fields.e_institucional')
    },
    c_institucional: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.c_institucional')
    },
    nro_ccaceptacion: {
      type: DataTypes.STRING(10)      
    },
    fecha_inscripcion: {
      type: DataTypes.DATEONLY      
    },
    ultimo_login: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.ultimo_login')
    },
    nro_intentos: {
      type: DataTypes.INTEGER,
      xlabel: lang.t('fields.nro_intentos'),
      defaultValue: 0
    },
    fecha_bloqueo: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_bloqueo')
    },
    fecha_reseteo_pass: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_bloqueo')
    },
    token: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.token')
    },
    extra: {
      type: DataTypes.JSON,
      xlabel: lang.t('fields.tour')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO', 'PENDIENTE', 'BLOQUEADO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Users = sequelize.define('usuarios', fields, {
    timestamps: false,
    tableName: 'sys_usuarios'
  });

  return Users;
};
