
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nota: {
      type: DataTypes.INTEGER
    },
    fecha_nota: {
      type: DataTypes.DATEONLY
    },
    nota_docente: {
      type: DataTypes.INTEGER
    },
    fecha_docente: {
      type: DataTypes.DATEONLY
    }, 
    est_evaluacion: {
      type: DataTypes.STRING(70)
    },
    est_valoracion: {
      type: DataTypes.STRING(70)
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Records = sequelize.define('records', fields, {
    timestamps: false,
    tableName: 'ges_records'
  });

  return Records;
};
