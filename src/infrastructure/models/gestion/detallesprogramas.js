
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    version: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['POSTULACION','INICIO','ENCURSO','CONCLUIDO','CERRADO'],
      defaultValue: 'POSTULACION'
    },
    fecha_ini: {
      type: DataTypes.DATEONLY
    },
    fecha_fin: {
      type: DataTypes.DATEONLY
    },
    cupo: {
      type: DataTypes.INTEGER
    },
    gestion: {
      type: DataTypes.STRING(4)
    },
    periodo: {
      type: DataTypes.STRING(9)
    },
    coordinador: {
      type: DataTypes.STRING(100)
    },
    perfil_postgraduado: {
      type: DataTypes.TEXT
    },
    perfil_postulante: {
      type: DataTypes.TEXT
    },
    pub_convocatoria: {
      type: DataTypes.DATEONLY
    },
    postulacion_ini: {
      type: DataTypes.DATEONLY
    },
    postulacion_fin: {
      type: DataTypes.DATEONLY
    }, 
    inscripcion_ini: {
      type: DataTypes.DATEONLY
    },
    inscripcion_fin: {
      type: DataTypes.DATEONLY
    },
    id_sia: {
      type: DataTypes.STRING(10)
    },
    colegiatura_sia: {
      type: DataTypes.INTEGER
    },
    version_sia:{
      type: DataTypes.INTEGER
    }, 
    costo_total: {
      type: DataTypes.INTEGER,      
    },
    costo_matricula: {
      type: DataTypes.INTEGER,      
    },
    nro_matriculas: {
      type: DataTypes.INTEGER,
    },
    costo_colegiatura: {
      type: DataTypes.INTEGER,      
    },
    descuento_contado: {
      type: DataTypes.INTEGER,      
    },
    descuento_horario: {
      type: DataTypes.INTEGER,      
    },
    descuento_otros: {
      type: DataTypes.INTEGER,      
    },  
    opciones_pagos: {
      type: DataTypes.JSON
    }  
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Detallesprogramas = sequelize.define('detallesprogramas', fields, {
    timestamps: false,
    tableName: 'ges_detalles'
  });

  return Detallesprogramas;
};
