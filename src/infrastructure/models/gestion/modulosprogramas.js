
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nro_modulo: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    codigo: {
      type: DataTypes.STRING(50),
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['SIMPLE','MIXTO']
    },
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT
    },
    hrs_presenciales: {
        type:DataTypes.INTEGER
    },
    hrsno_presenciales: {
        type:DataTypes.INTEGER
    },
    total_carga: {
      type:DataTypes.INTEGER
    },
    total_creditos: {
      type:DataTypes.INTEGER
    },
    fecha_ini: {
      type:DataTypes.DATEONLY
    },
    fecha_fin: {
      type:DataTypes.DATEONLY
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Modulosprogramas = sequelize.define('modulosprogramas', fields, {
    timestamps: false,
    tableName: 'ges_modulos'
  });

  return Modulosprogramas;
};
