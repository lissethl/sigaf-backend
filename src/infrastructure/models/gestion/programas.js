
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
    let fields = {
        id: util.pk,
        codigo: {
            type: DataTypes.STRING(50)
        },
        nombre: {
            type: DataTypes.STRING(150),
            allowNull: false
        },
        descripcion: {
            type: DataTypes.TEXT
        },
        objetivo: {
            type: DataTypes.TEXT
        },
        mencion: {
            type: DataTypes.STRING(100)
        },
        lineas_investigacion: {
            type:DataTypes.JSON
        },
        horarios: {
            type:DataTypes.STRING(255)
        },
        grado_academico:{
            type: DataTypes.STRING(100)
        },
        nro_modulos:{
            type: DataTypes.INTEGER
        },
        modalidad: {
            type: DataTypes.ENUM,
            values: ['PRESENCIAL', 'SEMIPRESENCIAL','VIRTUAL'],
            defaultValue: 'PRESENCIAL'
        },
        tipo: {
            type: DataTypes.STRING(100)
        },
        duracion: {
            type: DataTypes.INTEGER
        },
        nota_minima: {
            type: DataTypes.INTEGER
        },
        rhcu: {
            type: DataTypes.STRING(20)
        },  
        v_virtual: {
            type: DataTypes.INTEGER
        },
        v_colaborativo: {
            type: DataTypes.INTEGER
        },
        v_reflexiva: {
            type: DataTypes.INTEGER
        },
        v_linea: {
            type: DataTypes.INTEGER
        },
        v_evaluacion: {
            type: DataTypes.INTEGER
        },
        hrs_presenciales: {
            type:DataTypes.INTEGER
        },
        hrsno_presenciales: {
            type:DataTypes.INTEGER
        },
        total_carga: {
            type:DataTypes.INTEGER
        },
        total_creditos: {
            type:DataTypes.INTEGER
        },
        requisitos: {
            type: DataTypes.JSON
        },
        estado: {
            type: DataTypes.ENUM,
            values: ['POSTULACION','INICIO','ENCURSO','CONCLUIDO','CERRADO'],
            defaultValue: 'POSTULACION'
        }
    };

    // Agregando campos para el log
    fields = util.setTimestamps(fields);

    let Programas = sequelize.define('programas', fields, {
        timestamps: false,
        tableName: 'ges_programas'
    });

    return Programas;
};
