
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    modulo_ini: {
        type: DataTypes.DATEONLY
    },
    modulo_fin: {
      type: DataTypes.DATEONLY
    },
    nota_docente: {
      type: DataTypes.INTEGER
    },
    ampliacion_notas: {
      type: DataTypes.DATEONLY
    },
    thacad_docente: {
      type: DataTypes.INTEGER
    },
    porceneval_docente: {
      type: DataTypes.INTEGER
    },
    finaleval_docente: {
      type: DataTypes.INTEGER
    },
    fechaeval_docente: {
      type: DataTypes.DATEONLY
    },
    emision_notas: {
      type: DataTypes.BOOLEAN,
    },
    emision_actas: {
      type: DataTypes.BOOLEAN
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let AsignacionDocentes = sequelize.define('asignacionDocentes', fields, {
    timestamps: false,
    tableName: 'ges_asignacion_docentes'
  });

  return AsignacionDocentes;
};
