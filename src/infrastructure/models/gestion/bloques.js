'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    numero: {
      type: DataTypes.INTEGER,
      allownull : false
    },
    nombre: {
      type: DataTypes.STRING(250),
      nullable: false,
      xlabel: lang.t('fields.nombre')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Bloques = sequelize.define('bloques', fields, {
    timestamps: false,
    tableName: 'ges_bloques'
  });

  return Bloques;
};
