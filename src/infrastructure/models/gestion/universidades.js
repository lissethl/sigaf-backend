
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT
    },
    sigla: {
      type: DataTypes.STRING(15)
    },
    pais: {
      type: DataTypes.STRING(100)
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['PUBLICA', 'PRIVADA'],
      defaultValue: 'PUBLICA'
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO'],
      defaultValue: 'ACTIVO'
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Universidades = sequelize.define('universidades', fields, {
    timestamps: false,
    tableName: 'ges_universidades'
  });

  return Universidades;
};
