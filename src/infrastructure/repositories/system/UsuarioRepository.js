'use strict';

const { getQuery, errorHandler, toJSON } = require('../../lib/util');
const Repository = require('../Repository');
const { text } = require('../../../common');

module.exports = function usuariosRepository (models, Sequelize) {
  const { usuarios, archivos, roles, personas, entidades, programas, kardexs, detallesprogramas } = models;
  const Op = Sequelize.Op;

  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: ['nombre', 'path'],
        model: roles,
        as: 'rol'
      },
      {
        attributes: ['nombre'],
        model: entidades,
        as: 'entidad'
      },
      {
        attributes: [
          'nombres',
          'primer_apellido',
          'segundo_apellido',
          'nombre_completo',
          'tipo_documento',
          'lugar_exp',
          'lugar_nacimiento',
          'direccion',
          'tipo_documento_otro',
          'nro_documento',
          'fecha_nacimiento',
          'telefono',
          'movil',
          'nacionalidad',
          'pais_nacimiento',
          'genero',
          'estado'
        ],
        model: personas,
        as: 'persona'
      },
      {
        attributes: ['id', 'filename'],
        model: archivos,
        as: 'foto'
      }
    ];

    if (params.nombre_completo) {
      query.where[Op.or] = [
        {
          '$persona.nombres$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        },
        {
          '$persona.primer_apellido$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        },
        {
          '$persona.segundo_apellido$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        }
      ];
    }

    if (params.usuario) {
      query.where.usuario = {
        [Op.iLike]: `%${params.usuario}%`
      };
    }

    if (params.email) {
      query.where.email = {
        [Op.iLike]: `%${params.email}%`
      };
    }

    if (params.cargo) {
      query.where.cargo = {
        [Op.iLike]: `%${params.cargo}%`
      };
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if(params.estado_estudiante){
      query.where.estado_estudiante =params.estado_estudiante
    }

    if (params.estados) {
      query.where.estado = {
        [Op.in]: params.estados
      };
    }

    if (params.id_rol) {
      query.where.id_rol = params.id_rol;
    }

    if (params.id_entidad) {
      query.where.id_entidad = params.id_entidad;
    }

    if (params.id_persona) {
      query.where.id_persona = params.id_persona;
    }

    if (params.hora_entrevista) {
      query.where.hora_entrevista = params.hora_entrevista;
    }

    if (params.fecha_entrevista) {
      query.where.fecha_entrevista = params.fecha_entrevista;
    }

    if (params.id_version) {
      query.where.id_version = params.id_version;
    }

    if (params.id_version) {
      query.where.id_version = params.id_version;
    }

    if (params.id_roles) {
      query.where.id_rol = {
        [Op.in]: params.id_roles
      };
    }

    if (params.roles) {
      query.where['$rol.nombre$'] = {
        [Op.in]: params.roles
      };
    }

    const result = await usuarios.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await usuarios.findByPk(id, {
      include: [
        {
          attributes: ['nombre', 'path'],
          model: roles,
          as: 'rol'
        },
        {
          attributes: ['nombre'],
          model: entidades,
          as: 'entidad'
        },
        {
          attributes: [
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'nombre_completo',
            'tipo_documento',
            'lugar_exp',
            'lugar_nacimiento',
            'direccion',
            'tipo_documento_otro',
            'nro_documento',
            'fecha_nacimiento',
            'telefono',
            'movil',
            'nacionalidad',
            'pais_nacimiento',
            'genero',
            'estado'
          ],
          model: personas,
          as: 'persona'
        },
        {
          attributes: [
            'nombre',
            'rhcu',
            'horarios',
            'tipo'
          ],
          model: programas,
          as: 'programa'
        },
        {
          attributes: [
            'nro_matricula',
            'fecha_inscripcion',
            'area_nacimiento',
            'residencia',
            'casilla',
            'razon_social',
            't_direccion',
            't_telefono',
            't_email',
            'anio_egreso',
            'area_colegio',
            'admin_colegio',
            'turno_colegio',
            'univ_lugar',
            'univ_anio_inicio',
            'univ_anio_fin',
            'u_titulacion',
            'estado_titulo',
            'profesion',
            'id_universidad',
            'edad',
            'estado_civil',
            'estudios',
            'idiomas',
            'd_titulos',
            'd_pregrados',
            'd_postgrados',
            'd_pintelectual',
            'd_cursosr',
            'd_cursosd',
            'd_experiencia',
            'd_lineas',
            'd_tematica',
            'lugar_residencia',
            'nombre_colegio'
          ],
          model: kardexs,
          as: 'kardex'
        },
        {
          attributes: [
            'opciones_pagos',
            'costo_total',
            'costo_matricula',
            'costo_colegiatura',
            'coordinador',
            'fecha_ini',
            'nro_matriculas',
            'version',
            'periodo',
            'coordinador',
            'gestion'
          ],
          model: detallesprogramas,
          as: 'detallesprograma'
        },
        {
          attributes: ['id', 'filename'],
          model: archivos,
          as: 'foto'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }
  // find user
  async function findByUsername (usuario, include = true) {
    let cond = {
      where: {
        usuario
      }
    };

    if (include) {
      cond.include = [
        {
          attributes: ['nombre', 'path'],
          model: roles,
          as: 'rol'
        },
        {
          attributes: ['nombre'],
          model: entidades,
          as: 'entidad'
        },
        {
          attributes: [
            'nombres',
            'primer_apellido',
            'segundo_apellido',
            'nombre_completo',
            'tipo_documento',
            'lugar_exp',
            'lugar_nacimiento',
            'direccion',
            'tipo_documento_otro',
            'nro_documento',
            'fecha_nacimiento',
            'telefono',
            'movil',
            'nacionalidad',
            'pais_nacimiento',
            'genero',
            'estado'
          ],
          model: personas,
          as: 'persona'
        }
      ];
    } else {
      cond.include = [
        {
          attributes: ['nombre', 'path'],
          model: roles,
          as: 'rol'
        }
      ];
    }

    const result = await usuarios.findOne(cond);

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  // find userByCi
  async function findByCi (persona) {
    let cond = {
      attributes: ['id', 'usuario'],
      include: [{
        attributes: ['id'],
        model: personas,
        as: 'persona',
        where: persona,
        require: true
      }]
    };

    const result = await usuarios.findOne(cond);
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  async function createOrUpdate (usuario, t) {
    const cond = {
      where: {
        id: usuario.id || null
      }
    };

    const item = await usuarios.findOne(cond);

    if (item) {
      let updated;
      try {
        if (usuario.contrasena) {
          usuario.contrasena = text.encrypt(usuario.contrasena);
        }
        if (t) {
          cond.transaction = t;
        }
        updated = await usuarios.update(usuario, cond);
      } catch (e) {
        errorHandler(e);
      }
      const result = updated ? await usuarios.findOne(cond) : item;

      if (result) {
        return result.toJSON();
      }
      return null;
    }

    let result;
    try {
      if (usuario.contrasena) {
        usuario.contrasena = text.encrypt(usuario.contrasena);
      }
      result = await usuarios.create(usuario, t ? { transaction: t } : {});
    } catch (e) {
      errorHandler(e);
    }
    return result.toJSON();
  }

  return {
    findAll,
    findById,
    findByUsername,
    findByCi,
    createOrUpdate,
    deleteItem: (id, t) => Repository.deleteItem(id, usuarios, t)
  };
};
