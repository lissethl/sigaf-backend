'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function asignacionDocentesRepository (models, Sequelize) {
  const { asignacionDocentes, usuarios, personas, programas } = models;
  const { Op } = Sequelize;

  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        model: usuarios,
        attributes: ['usuario'],
        as: 'docente',
        include: [
          {
            model: personas, 
            attributes: ['nombre_completo'],
            as: 'persona'
          }
        ]
      },
      {
        model: programas,
        attributes: [
          'nombre', 
          'modalidad', 
          'nota_minima',
          'tipo',
          'codigo',
          'horarios'
        ],
        as: 'programa' 
      }
    ];

    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    if (params.id_version) {
      query.where.id_version = params.id_version;
    }

    if (params.id_modulo) {
      query.where.id_modulo = params.id_modulo;
    }

    if (params.id_bloque) {
      query.where.id_bloque = params.id_bloque;
    }
    if (params.id_docente) {
      query.where.id_docente = params.id_docente;
    }
    
    const result = await asignacionDocentes.findAndCountAll(query);
    return toJSON(result);
  }
   
  return {
    findAll,
    findById: (id) => Repository.findById(id, asignacionDocentes),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, asignacionDocentes, t),
    deleteItem: (id, t) => Repository.deleteItem(id, asignacionDocentes, t)
  };
};
