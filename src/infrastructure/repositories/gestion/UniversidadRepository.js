'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function universidadesRepository (models, Sequelize) {
  const { universidades } = models;
  const { Op } = Sequelize;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      }
    }

    if (params.sigla) {
      query.where.sigla = {
        [Op.iLike]: `%${params.sigla}%`
      }
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    return universidades.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, universidades),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, universidades, t),
    deleteItem: (id, t) => Repository.deleteItem(id, universidades, t)
  };
};
