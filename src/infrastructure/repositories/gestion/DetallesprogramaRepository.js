'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function detallesprogramasRepository (models, Sequelize) {
  const { detallesprogramas } = models;
  const { Op } = Sequelize;
 
  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.version) {
      query.where.version = {
        [Op.iLike]: `%${params.version}%`
      }
    }

    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    return detallesprogramas.findAndCountAll(query);
  }
 
  return {
    findAll,
    findById: (id) => Repository.findById(id, detallesprogramas),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, detallesprogramas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, detallesprogramas, t)
  };
};
