'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function programasRepository (models, Sequelize) {
  const { programas } = models;
  const { Op } = Sequelize;
  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      }
    }
    
    if (params.estado) {
      query.where.estado = params.estado;
    }
    return programas.findAndCountAll(query);
  }
  
  return {
    findAll,
    findById: (id) => Repository.findById(id, programas),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, programas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, programas, t)
  };
};
