'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function modulosprogramasRepository (models, Sequelize) {
  const { modulosprogramas } = models;
  const { Op } = Sequelize;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      }
    }
    
    if (params.id_bloque) {
      query.where.id_bloque = params.id_bloque;
    }

    if (params.nro_modulo) {
      query.where.nro_modulo = params.nro_modulo;
    }
    
    console.log('el query es: ', query.findAndCountAll)
    return modulosprogramas.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, modulosprogramas),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, modulosprogramas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, modulosprogramas, t)
  };
};
