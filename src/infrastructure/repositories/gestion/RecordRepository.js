'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function recordsRepository (models, Sequelize) {
  const { records, usuarios, personas, bloques, programas, detallesprogramas, modulosprogramas } = models;
  const { Op } = Sequelize;

  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        model: usuarios,
        attributes: ['usuario', 'acronimo'],
        as: 'docente',
        include: [
          {
            model: personas, 
            attributes: ['nombre_completo'],
            as: 'persona'
          }
        ]
      },
      {
        model: usuarios,
        attributes: ['usuario', 'email', 'fecha_inscripcion'],
        as: 'estudiante',
        include: [
          {
            model: personas, 
            attributes: [
              'primer_apellido',
              'segundo_apellido',
              'nombres',
              'nombre_completo', 
              'nro_documento', 
              'movil',
              'telefono',
              'lugar_exp'],
            as: 'persona'
          }
        ]
      },
      {
        model: programas,
        attributes: [
          'nombre', 
          'modalidad', 
          'nota_minima',
          'tipo',
          'codigo',
          'horarios'
        ],
        as: 'programa' 
      },
      {
        model: detallesprogramas,
        attributes: [
          'coordinador',
          'periodo',
          'version'],
        as: 'version' 
      },
      {
        model: bloques,
        attributes: ['nombre'],
        as: 'bloque' 
      },
      {
        model: modulosprogramas,
        attributes: [
          'nombre',
          'fecha_ini',
          'fecha_fin'
        ],
        as: 'modulo' 
      }
    ];

    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    if (params.id_version) {
      query.where.id_version = params.id_version;
    }

    if (params.id_modulo) {
      query.where.id_modulo = params.id_modulo;
    }

    if (params.id_bloque) {
      query.where.id_bloque = params.id_bloque;
    }

    if (params.id_estudiante) {
      query.where.id_estudiante = params.id_estudiante;
    }

    if (params.id_docente) {
      query.where.id_docente = params.id_docente;
    }
    
    if (params.est_evaluacion) {
      query.where.est_evaluacion = {
        [Op.iLike]: `%${params.est_evaluacion}%`
      };
    }
    
    if (params.est_valoracion) {
      query.where.est_valoracion = {
        [Op.iLike]: `%${params.est_valoracion}%`
      };
    }
    
    const result = await records.findAndCountAll(query);
    return toJSON(result);
  }
   
  return {
    findAll,
    findById: (id) => Repository.findById(id, records),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, records, t),
    deleteItem: (id, t) => Repository.deleteItem(id, records, t)
  };
};
