'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function bloquesRepository (models, Sequelize) {
  const { bloques } = models;
  const { Op } = Sequelize;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      }
    }
    
    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    if (params.numero) {
      query.where.numero = params.numero;
    }
    
    console.log('el query es: ', query.findAndCountAll)
    return bloques.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, bloques),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, bloques, t),
    deleteItem: (id, t) => Repository.deleteItem(id, bloques, t)
  };
};
