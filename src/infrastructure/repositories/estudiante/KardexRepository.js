'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function kardexsRepository (models, Sequelize) {
  const { kardexs } = models;
  // const { Op } = Sequelize;

  function findAll (params = {}) {
    const query = getQuery(params);
    query.where = {};

    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }
    console.log('el query es: ', query.findAndCountAll);
    return kardexs.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, kardexs),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, kardexs, t),
    deleteItem: (id, t) => Repository.deleteItem(id, kardexs, t)
  };
};
