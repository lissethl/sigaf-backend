'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function evaluacionesRepository (models, Sequelize) {
  const { 
    evaluaciones, 
    usuarios,
    personas,
    programas,
    detallesprogramas,
    bloques,
    modulosprogramas 
  } = models;
  // const { Op } = Sequelize;

  const include = [
    {
      model: usuarios,
      attributes: ['usuario', 'acronimo'],
      as: 'docente',
      include: [
        {
          model: personas, 
          attributes: ['nombre_completo'],
          as: 'persona'
        }
      ]
    },
    {
      model: usuarios,
      attributes: ['usuario', 'email', 'fecha_inscripcion'],
      as: 'estudiante',
      include: [
        {
          model: personas, 
          attributes: [
            'primer_apellido',
            'segundo_apellido',
            'nombres',
            'nombre_completo', 
            'nro_documento', 
            'movil',
            'telefono',
            'lugar_exp'],
          as: 'persona'
        }
      ]
    },
    {
      model: programas,
      attributes: [
        'nombre', 
        'modalidad', 
        'nota_minima',
        'tipo',
        'codigo',
        'horarios'
      ],
      as: 'programa' 
    },
    {
      model: detallesprogramas,
      attributes: [
        'coordinador',
        'periodo',
        'version'],
      as: 'version' 
    },
    {
      model: bloques,
      attributes: ['nombre'],
      as: 'bloque' 
    },
    {
      model: modulosprogramas,
      attributes: [
        'nombre',
        'fecha_ini',
        'fecha_fin'
      ],
      as: 'modulo' 
    }
  ];

  function findAll (params = {}) {
    const query = getQuery(params);
    query.where = {};
    query.include = include;

    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }

    if (params.id_bloque) {
      query.where.id_bloque = params.id_bloque;
    }

    if (params.id_docente) {
      query.where.id_docente = params.id_docente;
    }

    if (params.id_estudiante) {
      query.where.id_estudiante = params.id_estudiante;
    }

    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    if (params.id_version) {
      query.where.id_version = params.id_version;
    }

    if (params.id_modulo) {
      query.where.id_modulo = params.id_modulo;
    }
    if (params.estado) {
      query.where.estado = params.estado;
    }
    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    return evaluaciones.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await evaluaciones.findByPk(id, {
      include
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, evaluaciones, t),
    deleteItem: (id, t) => Repository.deleteItem(id, evaluaciones, t)
  };
};
