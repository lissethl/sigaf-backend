'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function constantesRepository (models, Sequelize) {
  const { datosgenerales } = models;
  const { Op } = Sequelize;

  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.gestion) {
      query.where.gestion = {
        [Op.iLike]: `%${params.gestion}%`
      }
    }

    const result = await datosgenerales.findAndCountAll(query);
    return toJSON(result);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, datosgenerales),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, datosgenerales, t),
    deleteItem: (id, t) => Repository.deleteItem(id, datosgenerales, t)
  };
};
