'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function constantesRepository (models, Sequelize) {
  const { constantes } = models;
  const { Op } = Sequelize;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      }
    }

    if (params.grupo) {
      query.where.grupo = params.grupo;
    }

    if (params.codigo) {
      query.where.codigo = {
        [Op.iLike]: `%${params.codigo}%`
      }
    }

    return constantes.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, constantes),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, constantes, t),
    deleteItem: (id, t) => Repository.deleteItem(id, constantes, t)
  };
};
