'use strict';

const { getQuery, errorHandler, toJSON } = require('../../lib/util');
const Repository = require('../Repository');
const { text } = require('../../../common');

module.exports = function contratosRepository (models, Sequelize) {
  const { contratos, usuarios, personas,  programas,kardexs, detallesprogramas } = models;
  const Op = Sequelize.Op;
  const include = [
    {
      attributes: [
        'nombres',
        'primer_apellido',
        'segundo_apellido',
        'nombre_completo',
        'tipo_documento',
        'lugar_exp',
        'lugar_nacimiento',
        'direccion',
        'tipo_documento_otro',
        'nro_documento',
        'fecha_nacimiento',
        'telefono',
        'movil',
        'nacionalidad',
        'pais_nacimiento',
        'genero',
        'estado',
        'lugar_exp'
      ],
      model: personas,
      as: 'persona'
    },
    {
      attributes: [
        'email'
      ],
      model: usuarios,
      as: 'usuario'
    },
    {
      attributes: [
        'nombre',
        'modalidad',
        'nota_minima'
      ],
      model: programas,
      as: 'programa'
    },
    {
      attributes: [
        'version',
        'periodo',
        'fecha_ini',
        'fecha_fin', 
        'postulacion_ini',
        'postulacion_fin',
        'estado',
        'costo_matricula',
        'costo_colegiatura',
        'colegiatura_sia'
      ],
      model: detallesprogramas,
      as: 'detallesprograma'
    },
    {
      attributes: [
        'usuario',
        'cargo',
        'id'
      ],
      model: usuarios,
      as: 'usuario_reserva',
      include: [
        {
          attributes: [
            'nombres',
            'primer_apellido',
            'segundo_apellido'
          ],
          model: personas,
          as: 'persona'
        }
      ]
    }
  ];
  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = include;

    if (params.nombre_completo) {
      query.where[Op.or] = [
        {
          '$persona.nombres$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        },
        {
          '$persona.primer_apellido$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        },
        {
          '$persona.segundo_apellido$': {
            [Op.iLike]: `%${params.nombre_completo}%`
          }
        }
      ];
    }

    if (params.ci) {
      query.where['$persona.nro_documento$'] = {
        [Op.iLike]: `%${params.ci}%`
      }
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.estados) {
      query.where.estado = {
        [Op.in]: params.estados
      };
    }

    if (params.id_persona) {
      query.where.id_persona = params.id_persona;
    }
    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }

    if (params.id_programa) {
      query.where.id_programa = params.id_programa;
    }

    if (params.id_destallesprograma) {
      query.where.id_destallesprograma = params.id_destallesprograma;
    }

    const result = await contratos.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await contratos.findByPk(id, {
      include
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }
  
  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, contratos, t),
    deleteItem: (id, t) => Repository.deleteItem(id, contratos, t)
  };
};
