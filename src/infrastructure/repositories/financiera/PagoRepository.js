'use strict';

const { getQuery, errorHandler, toJSON } = require('../../lib/util');
const Repository = require('../Repository');
const { text } = require('../../../common');

module.exports = function pagosRepository (models, Sequelize) {
  const { pagos, contratos } = models;
  const Op = Sequelize.Op;
  const include = [
    {
      attributes: [
        'opcion_pago',
        'descuento',
        'monto_descuento'
      ],
      model: contratos,
      as: 'contrato'
    }
  ]; 
  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.id_contrato) {
      query.where.id_contrato = params.id_contrato;
    }

    const result = await pagos.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await pagos.findByPk(id, {
      include
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }
  
  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, pagos, t),
    deleteItem: (id, t) => Repository.deleteItem(id, pagos, t)
  };
};
