'use strict';

// Definiendo asociaciones de las tablas
module.exports = function associations (models) {
  const {
    roles,
    usuarios,
    entidades,
    modulos,
    permisos,
    personas,
    tokens,
    programas,
    modulosprogramas,
    detallesprogramas,
    kardexs,
    contratos,
    pagos,
    bloques,
    asignacionDocentes,
    records,
    archivos,
    evaluaciones
  } = models;

  // MODULO USUARIOS
  // Asociaciones tabla usuarios
  usuarios.belongsTo(entidades, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });
  entidades.hasMany(usuarios, { foreignKey: { name: 'id_entidad', allowNull: false }, as: 'entidad' });

  usuarios.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(usuarios, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });

  usuarios.belongsTo(personas, { foreignKey: { name: 'id_persona' }, as: 'persona' });
  personas.hasMany(usuarios, { foreignKey: { name: 'id_persona' }, as: 'persona' });

  usuarios.belongsTo(programas, { foreignKey: { name: 'id_programa' }, as: 'programa' });
  programas.hasOne(usuarios, { foreignKey: { name: 'id_programa' } });

  usuarios.belongsTo(detallesprogramas, { foreignKey: { name: 'id_version' }, as: 'detallesprograma' });
  detallesprogramas.hasOne(usuarios, { foreignKey: { name: 'id_version' } });

  usuarios.belongsTo(kardexs, { foreignKey: { name: 'id_kardex' }, as: 'kardex' });
  kardexs.hasOne(usuarios, { foreignKey: { name: 'id_kardex' } });

  usuarios.belongsTo(archivos, { foreignKey: { name: 'id_foto' }, as: 'foto' });
  archivos.hasOne(usuarios, { foreignKey: { name: 'id_foto' } });

  // Asociaciones tablas permisos - roles
  permisos.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(permisos, { foreignKey: { name: 'id_rol', allowNull: false } });

  // Asociaciones tablas permisos - modulos
  permisos.belongsTo(modulos, { foreignKey: { name: 'id_modulo', allowNull: false }, as: 'modulo' });
  modulos.hasMany(permisos, { foreignKey: { name: 'id_modulo', allowNull: false } });

  // Asociaciones tablas modulos - sección
  modulos.belongsTo(modulos, { foreignKey: 'id_modulo' });
  modulos.hasMany(modulos, { foreignKey: 'id_modulo' });
  modulos.belongsTo(modulos, { foreignKey: 'id_seccion' });
  modulos.hasMany(modulos, { foreignKey: 'id_seccion' });

  // Asociaciones tabla tokens
  tokens.belongsTo(usuarios, { foreignKey: { name: 'id_usuario' }, as: 'usuario' });
  usuarios.hasMany(tokens, { foreignKey: { name: 'id_usuario' } });

  tokens.belongsTo(entidades, { foreignKey: { name: 'id_entidad' }, as: 'entidad' });
  entidades.hasMany(tokens, { foreignKey: { name: 'id_entidad' } });

  // Asociaciones tabla programas
  // modulosprogramas.belongsTo(programas, { foreignKey: { name: 'id_programa', allowNull: false }, as: 'programa' });
  // programas.hasMany(modulosprogramas, { foreignKey: { name: 'id_programa', allowNull: false } });

  bloques.belongsTo(programas, { foreignKey: { name: 'id_programa', allowNull: false }, as: 'programa' });
  programas.hasMany(bloques, { foreignKey: { name: 'id_programa', allowNull: false } });

  modulosprogramas.belongsTo(bloques, { foreignKey: { name: 'id_bloque', allowNull: false }, as: 'bloque' });
  bloques.hasMany(modulosprogramas, { foreignKey: { name: 'id_bloque', allowNull: false } });

  detallesprogramas.belongsTo(programas, { foreignKey: { name: 'id_programa', allowNull: false }, as: 'programa' });
  programas.hasMany(detallesprogramas, { foreignKey: { name: 'id_programa', allowNull: false } });

  /* programas.belongsTo(detallesprogramas, { foreignKey: { name: 'id_detallesprograma' }, as: 'detallesprograma' });
  detallesprogramas.hasOne(programas, { foreignKey: { name: 'id_detallesprograma' } }); */

  // Asociaciones tabla contratos

  contratos.belongsTo(personas, { foreignKey: { name: 'id_persona' }, as: 'persona' });
  personas.hasOne(contratos, { foreignKey: { name: 'id_persona' } });

  contratos.belongsTo(usuarios, { foreignKey: { name: 'id_usuario' }, as: 'usuario' });
  usuarios.hasOne(contratos, { foreignKey: { name: 'id_usuario' } });

  contratos.belongsTo(usuarios, { foreignKey: { name: 'id_usuario_reserva' }, as: 'usuario_reserva' });
  usuarios.hasOne(contratos, { foreignKey: { name: 'id_usuario_reserva' } });

  contratos.belongsTo(programas, { foreignKey: { name: 'id_programa' }, as: 'programa' });
  programas.hasOne(contratos, { foreignKey: { name: 'id_programa' } });

  contratos.belongsTo(detallesprogramas, { foreignKey: { name: 'id_detallesprograma' }, as: 'detallesprograma' });
  detallesprogramas.hasOne(contratos, { foreignKey: { name: 'id_detallesprograma' } });

  pagos.belongsTo(contratos, { foreignKey: { name: 'id_contrato' }, as: 'contrato' });
  contratos.hasMany(pagos, { foreignKey: { name: 'id_contrato' } });

  // Asociaciones asignacionDocentes
  asignacionDocentes.belongsTo(usuarios, { foreignKey: { name: 'id_docente' }, as: 'docente' });
  usuarios.hasMany(asignacionDocentes, { foreignKey: { name: 'id_docente' } });

  asignacionDocentes.belongsTo(programas, { foreignKey: { name: 'id_programa' }, as: 'programa' });
  programas.hasMany(asignacionDocentes, { foreignKey: { name: 'id_programa' } });

  asignacionDocentes.belongsTo(detallesprogramas, { foreignKey: { name: 'id_version' }, as: 'version' });
  detallesprogramas.hasMany(asignacionDocentes, { foreignKey: { name: 'id_version' } });

  asignacionDocentes.belongsTo(bloques, { foreignKey: { name: 'id_bloque' }, as: 'bloque' });
  bloques.hasMany(asignacionDocentes, { foreignKey: { name: 'id_bloque' } });

  asignacionDocentes.belongsTo(modulosprogramas, { foreignKey: { name: 'id_modulo' }, as: 'modulo' });
  modulosprogramas.hasMany(asignacionDocentes, { foreignKey: { name: 'id_modulo' } });

  // Records Academicos

  records.belongsTo(usuarios, { foreignKey: { name: 'id_estudiante' }, as: 'estudiante' });
  usuarios.hasMany(records, { foreignKey: { name: 'id_estudiante' } });

  records.belongsTo(programas, { foreignKey: { name: 'id_programa' }, as: 'programa' });
  programas.hasMany(records, { foreignKey: { name: 'id_programa' } });

  records.belongsTo(detallesprogramas, { foreignKey: { name: 'id_version' }, as: 'version' });
  detallesprogramas.hasMany(records, { foreignKey: { name: 'id_version' } });

  records.belongsTo(bloques, { foreignKey: { name: 'id_bloque' }, as: 'bloque' });
  bloques.hasMany(records, { foreignKey: { name: 'id_bloque' } });

  records.belongsTo(modulosprogramas, { foreignKey: { name: 'id_modulo' }, as: 'modulo' });
  modulosprogramas.hasMany(records, { foreignKey: { name: 'id_modulo' } });

  records.belongsTo(usuarios, { foreignKey: { name: 'id_docente' }, as: 'docente' });
  usuarios.hasMany(records, { foreignKey: { name: 'id_docente' } });

  // Evaluaciones

  evaluaciones.belongsTo(programas, { foreignKey: { name: 'id_programa' }, as: 'programa' });
  programas.hasMany(evaluaciones, { foreignKey: { name: 'id_programa' } });

  evaluaciones.belongsTo(detallesprogramas, { foreignKey: { name: 'id_version' }, as: 'version' });
  detallesprogramas.hasMany(evaluaciones, { foreignKey: { name: 'id_version' } });

  evaluaciones.belongsTo(bloques, { foreignKey: { name: 'id_bloque' }, as: 'bloque' });
  bloques.hasMany(evaluaciones, { foreignKey: { name: 'id_bloque' } });

  evaluaciones.belongsTo(modulosprogramas, { foreignKey: { name: 'id_modulo' }, as: 'modulo' });
  modulosprogramas.hasMany(evaluaciones, { foreignKey: { name: 'id_modulo' } });

  evaluaciones.belongsTo(usuarios, { foreignKey: { name: 'id_estudiante' }, as: 'estudiante' });
  usuarios.hasMany(evaluaciones, { foreignKey: { name: 'id_estudiante' } });

  evaluaciones.belongsTo(usuarios, { foreignKey: { name: 'id_docente' }, as: 'docente' });
  usuarios.hasMany(evaluaciones, { foreignKey: { name: 'id_docente' } });

  return models;
};
