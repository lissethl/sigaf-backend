'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  // CONSTANTES
  {
    label: 'Configuraciones',
    ruta: 'config',
    icono: 'settings',
    orden: 191,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Entidades',
    ruta: 'entidades',
    orden: 192,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 1
  },
  {
    label: 'Personas',
    ruta: 'personas',
    orden: 193,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 1
  },
  {
    label: 'Usuarios',
    ruta: 'usuarios',
    orden: 194,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 1
  },
  {
    label: 'Módulos y permisos',
    ruta: 'modulos',
    orden: 195,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 1
  },
  {
    label: 'Preferencias',
    ruta: 'parametros',
    orden: 196,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 1
  },
  {
    label: 'Permisos',
    ruta: 'permisos',
    orden: 197,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 1
  },
  {
    label: 'Roles',
    ruta: 'roles',
    orden: 198,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 1
  },
  {
    label: 'Logs del sistema',
    ruta: 'logs',
    orden: 199,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 1
  },
  {
    label: 'Servicios Iop',
    ruta: 'serviciosIop',
    orden: 200,
    estado: 'ACTIVO',
    visible: false,
    id_modulo: 1
  },
  {
    label: 'Parámetros',
    ruta: 'parametros-sys',
    icono: 'build',
    orden: 181,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Universidades',
    ruta: 'universidades',
    orden: 182,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Carreras',
    ruta: 'carreras',
    orden: 183,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Tipo de Programa',
    ruta: 'tipoprograma',
    orden: 184,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Grados Academicos',
    ruta: 'gradosacademicos',
    orden: 185,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Requisitos',
    ruta: 'requisitos',
    orden: 186,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Datos Generales',
    ruta: 'datosgenerales',
    orden: 187,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Gestión Académica',
    ruta: 'gestionacademica',
    icono: 'tab',
    orden: 71,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Programas',
    ruta: 'programas',
    orden: 69,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Mudolos Programa',
    ruta: 'modulosprogramas',
    orden: 73,
    estado: 'INACTIVO',
    visible: false
  },
  {
    label: 'Detalles Programa',
    ruta: 'detallesprogramas',
    orden: 74,
    estado: 'INACTIVO',
    visible: false
  },
  {
    label: 'Docentes',
    ruta: 'docentes',
    orden: 72,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Gestión Estudiantil',
    ruta: 'gestionestudiantil',
    icono: 'account_circle',
    orden: 61,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Postulantes',
    ruta: 'postulantes',
    orden: 62,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Entrevistas',
    ruta: 'entrevistas',
    orden: 63,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Preinscritos',
    ruta: 'preinscritos',
    orden: 65,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Asignación Docente/Módulo',
    ruta: 'asignacionDocentes',
    orden: 78, // 66
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Horarios',
    ruta: 'horarios',
    orden: 188,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Opciones Pago',
    ruta: 'opcionespago',
    orden: 189,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Gestión de Pagos',
    ruta: 'contratos',
    icono: '',
    orden: 41,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 31
  },
  {
    label: 'Gestión Financiera',
    ruta: 'gestionfinanciera',
    icono: 'monetization_on',
    orden: 160,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Informes/Estadísticas',
    ruta: 'informes',
    icono: 'assignment',
    orden: 170,
    estado: 'ACTIVO',
    visible: true
  },
  {
    label: 'Bloque',
    ruta: 'bloques',
    orden: 76,
    estado: 'INACTIVO',
    visible: false
  },
  {
    label: 'Tematicas',
    ruta: 'tematicas',
    orden: 190,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Admitir Postulación',
    ruta: 'postulantesprograma',
    orden: 64,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Asignaciones',
    ruta: 'asignaciones',
    orden: 300,
    estado: 'ACTIVO',
    visible: false
  },
  {
    label: 'Asignar Correos Inst.',
    ruta: 'asignacioncorreo',
    orden: 67,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Reserva de Programas',
    ruta: 'reservas',
    orden: 161,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 31
  },
  {
    label: 'Baja Preinscritos',
    ruta: 'preinscritobajas',
    orden: 68,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Administración de Programas',
    ruta: 'adminprogramas',
    orden: 75,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Docentes/Requisitos',
    ruta: 'drequisitos',
    orden: 77,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 11
  },
  {
    label: 'Traspaso de Pagos SIA',
    ruta: 'siapagos',
    orden: 162,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 31
  },
  {
    label: 'Lista de Inscritos',
    ruta: 'inscritos',
    orden: 79,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 32
  },
  {
    label: 'Acta de Notas por Módulo',
    ruta: 'records',
    orden: 178,//180
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Pagos',
    ruta: 'pagos',
    orden: 163,
    estado: 'ACTIVO',
    visible: false
  },
  {
    label: 'Lista de Postulantes',
    ruta: 'lpostulantes',
    orden: 70,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 32
  },
  {
    label: 'Lista de Asistencia',
    ruta: 'lasistencia',
    orden: 80,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 32
  },
  {
    label: 'Seguimiento de Requisitos',
    ruta: 'srequisitos',
    orden: 164,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 32
  },
  {
    label: 'Evaluación docente',
    ruta: 'evaluaciondocente',
    icono: '',
    orden: 171,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: '32'
  },
  {
    label: 'Evaluación alumno',
    ruta: 'evaluacionalumno',
    icono: '',
    orden: 172,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: '32'
  },
  {
    label: 'Consolidado evaluación',
    ruta: 'consolidadoevaluacion',
    icono: '',
    orden: 173,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: '32'
  },
  {
    label: 'Historial académico',
    ruta: 'historialacademico',
    icono: '',
    orden: 174,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: '32'
  },
  {
    label: 'Record Académico',
    ruta: 'recordacademico',
    icono: '',
    orden: 175,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: '32'
  },
  {
    label: 'Realizar Inscripción',
    ruta: 'inscripcion',
    orden: 66,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  },
  {
    label: 'Asignación de Módulos',
    ruta: 'asignacionmodulos',
    orden: 180, // 178
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Acta Notas por Docente',
    ruta: 'notas',
    orden: 177,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 18
  },
  {
    label: 'Inscripción Rezagados',
    ruta: 'rezagados',
    orden: 179,
    estado: 'ACTIVO',
    visible: true,
    id_modulo: 23
  }

];
// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_modulos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
