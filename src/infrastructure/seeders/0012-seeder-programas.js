'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { 
    codigo: 'MDE-2020',
    nombre: 'MAESTRÍA EN DESARROLLO ECONÓMICO',
    descripcion: 'MAESTRÍA EN DESARROLLO ECONÓMICO',
    objetivo: 'CONTRIBUIR A LA FORMACIÓN DE RECURSOS HUMANOS ESPECIALIZADOS EN TEMAS DE DESARROLLO ECONÓMICO, CON CAPACIDADES ANALÍTICAS SÓLIDAS PARA COMPRENDER, EXPLICAR Y CONTRIBUIR AL ACTUAL DEBATE SOBRE CRECIMIENTO, DESARROLLO PRODUCTIVO Y SOSTENIBLE ASÍ COMO AL DISEÑO Y EVALUACIÓN DE POLÍTICAS PÚBLICAS EN BOLIVIA.',
    mencion: 'MAESTRÍA EN DESARROLLO ECONÓMICO',
    horarios: 'H1',
    grado_academico:'P2',
    tipo: 'P2',
    nro_modulos: '13',
    modalidad: 'PRESENCIAL',
    tipo: 'MAESTRIA',
    duracion: '24',
    nota_minima: 70,
    rhcu: 'RHCU-100',
    hrs_presenciales: 960,
    hrsno_presenciales: 1440,
    total_carga: 2400,
    total_creditos: 60,
    lineas_investigacion: '[]',
    estado: 'POSTULACION'
  },
  { 
    codigo: 'DCD-2020',
    nombre: 'DOCTORADO MULTIDISCIPLINARIO EN CIENCIAS DEL DESARROLLO: ECONOMÍA, SOCIEDAD Y GLOBALIZACIÓN',
    descripcion: 'DOCTORADO MULTIDISCIPLINARIO EN CIENCIAS DEL DESARROLLO:ECONOMÍA, SOCIEDAD Y GLOBALIZACIÓN',
    objetivo: 'CONTRIBUIR AL PAÍS, A LA UMSA Y A LA COMUNIDAD ACADÉMICA DEL CIDES CON EL DESARROLLO DE UN PROGRAMA DE FORMACIÓN ACADÉMICA ORIENTADA A LA CONSTRUCCIÓN Y FORTALECIMIENTO DE CAPACIDADES DE INVESTIGACIÓN EN ÁREAS ESTRATÉGICAS DEL DESARROLLO ECONÓMICO, SOCIAL Y DEL PROCESO DE GLOBALIZACIÓN.',
    mencion: 'DOCTORADO MULTIDISCIPLINARIO EN CIENCIAS DEL DESARROLLO: ECONOMÍA, SOCIEDAD Y GLOBALIZACIÓN',
    horarios: 'H1',
    grado_academico:'P3',
    tipo: 'P3',
    nro_modulos: '11',
    modalidad: 'PRESENCIAL',
    tipo: 'DOCTORADO',
    duracion: '36',
    nota_minima: 70,
    rhcu: 'RHCU-200',
    hrs_presenciales: 568,
    hrsno_presenciales: 2392,
    total_carga: 2960,
    total_creditos: 74,
    lineas_investigacion: '[]',
    estado: 'POSTULACION'
}
  
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('ges_programas', items, {});
  },

  down (queryInterface, Sequelize) { }
};
