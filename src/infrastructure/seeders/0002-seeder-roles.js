'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { nombre: 'SUPERADMIN', descripcion: 'Super Administrador', path: '' },
  { nombre: 'ADMIN', descripcion: 'Administrador', path: '' },
  { nombre: 'PREREGISTRO', descripcion: 'Preregistro', path: 'account' },
  { nombre: 'DOCENTE', descripcion: 'Docente', path: 'usuarios' },
  { nombre: 'ESTUDIANTE', descripcion: 'Estudiante', path: 'usuarios' },
  { nombre: 'AREA DESCONCENTRADA', descripcion: 'Area desconcentrada', path: ''},
  { nombre: 'COORDINADOR', descripcion: 'Coordinador', path: '' },
  { nombre: 'SECRETARIA', descripcion: 'Secretaria', path: '' },
  { nombre: 'SISTEMAS', descripcion: 'Sistemas', path: '' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_roles', items, {});
  },

  down (queryInterface, Sequelize) { }
};
