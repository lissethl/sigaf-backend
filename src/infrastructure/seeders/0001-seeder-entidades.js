'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

// Datos de producción
let items = [
  {
    nombre: 'POSTGRADO EN CIENCIAS DEL DESARROLLO CIDES - UMSA.',
    descripcion: 'Su misión es formar recursos humanos para el desarrollo y contribuir al debate intelectual sobre los desafíos que experimenta el país, con rigor académico, pluralismo teórico y político, y en el marco de la autonomía universitaria y los compromisos democráticos y emancipatorios.',
    sigla: 'CIDES',
    email: 'cides@cides.edu.bo',
    telefonos: '(+591 2 )2786169 - (+591 2 ) 2784207 - (+591 2 )2782361',
    direccion: 'Calle Rosasani No. 55. Obrajes - Calle 3 de Obrajes N° 515',
    web: 'http://www.cides.edu.bo/',
    estado: 'ACTIVO',
    nit: '111111111'
  }
];

// Agregando datos aleatorios para desarrollo
/* if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  let personas = Array(9).fill().map((_, i) => {
    let item = {
      nombre: casual.company_name,
      descripcion: casual.text,
      sigla: casual.company_suffix,
      email: casual.email,
      telefonos: `${casual.phone},${casual.phone}`,
      direccion: casual.address,
      web: casual.url,
      estado: 'ACTIVO',
      subdomain: casual.domain
    };

    return item;
  });

  items = items.concat(personas);
} */

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_entidades', items, {});
  },

  down (queryInterface, Sequelize) { }
};
