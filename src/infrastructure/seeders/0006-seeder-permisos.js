'use strict';

const { setTimestampsSeeder } = require('../lib/util');


let itemsbefore21 = [
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "17"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "1",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "2",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "3"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "56"
  }
];
let itemsbefore22 = [
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "17"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "1",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "2",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "3"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "56"
  }
];
let items = [
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "21"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "22"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "2"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "6"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "7"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "9"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "16"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "17"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "25"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "1",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "2",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "31"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "3"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "10"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "11"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "12"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "13"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "14"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "15"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "23"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "24"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "27"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "30"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "33"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "37"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "40"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "41"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "42"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "1"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "4"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "5"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "8"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "18"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "19"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "20"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "26"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "28"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "29"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "32"
  },
  {
    "create": false,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "34"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "35"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "36"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "38"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "39"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "1"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "2"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "3"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "4"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "5"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "6"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "7"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "8"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "9"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "10"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "11"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "12"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "13"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "14"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "15"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "16"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "17"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "18"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "19"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "20"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "21"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "22"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "23"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "24"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "25"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "26"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "27"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "28"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "29"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "30"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "31"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "32"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "33"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "34"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "35"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "36"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "37"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "38"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "39"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "40"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "41"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "42"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "43"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "43"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "44"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "44"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "45"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "45"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "46"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "46"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "47"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "47"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "48"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "48"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "5",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "49"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "49"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "50"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "50"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "51"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "51"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "52"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "52"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "3",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": true,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "53"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "53"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "6",
    "id_modulo": "54"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "7",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "54"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "55"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "55"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "4",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "56"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "8",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": false,
    "id_rol": "9",
    "id_modulo": "56"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "1",
    "id_modulo": "57"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "2",
    "id_modulo": "57"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "3",
    "id_modulo": "57"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "4",
    "id_modulo": "57"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "5",
    "id_modulo": "57"
  },
  {
    "create": false,
    "read": false,
    "update": false,
    "delete": false,
    "id_rol": "6",
    "id_modulo": "57"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "7",
    "id_modulo": "57"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "8",
    "id_modulo": "57"
  },
  {
    "create": true,
    "read": true,
    "update": true,
    "delete": true,
    "id_rol": "9",
    "id_modulo": "57"
  }
];
// Este bloque se debe reemplazar cuando se tengan los permisos definidos para cada módulo por rol
/* const iniModules = 1;
const nroModules = 27;
const nroRoles = 5;

for (let rol = 1; rol <= nroRoles; rol++) {
  for (let modulo = iniModules; modulo <= nroModules; modulo++) {
    items.push({
      create: true,
      read: true,
      update: true,
      delete: true,
      firma: false,
      csv: false,
      id_modulo: modulo,
      id_rol: rol
    });
  }
} */

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_permisos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
