'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { 
    numero: 1,
    nombre: 'BLOQUE INTRODUCTORIO',
    id_programa: 1
  }, 
  { 
    numero: 2,
    nombre: 'BLOQUE ESPECIALIDAD',
    id_programa: 1
  },
  { 
    numero: 3,
    nombre: 'BLOQUE TALLERES DE TESIS',
    id_programa: 1
  },
  { 
    numero: 1,
    nombre: 'BLOQUE COMÚN',
    id_programa: 2
  }, 
  { 
    numero: 2,
    nombre: 'ECONOMÍA DEL DESARROLLO',
    id_programa: 2
  },
  { 
    numero: 3,
    nombre: 'CUESTIONES DEL DESARROLLO SOCIAL',
    id_programa: 2
  },
  { 
    numero: 4,
    nombre: 'GLOBALIZACIÓN',
    id_programa: 2
  },
  { 
    numero: 5,
    nombre: 'AVANCES DE TESIS',
    id_programa: 2
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('ges_bloques', items, {});
  },

  down (queryInterface, Sequelize) { }
};
