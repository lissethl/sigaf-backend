'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { 
    dir_acronimo: 'PhD.',
    nom_director: 'ALFREDO SEOANE',
    dir_descripcion: 'DIRECTOR CIDES',
    subd_acronimo: 'MSc.',
    nom_subdirector: 'JORGE ALBARRACÍN DEKER',
    sdir_descripcion: 'SUBDIRECTOR CIDES',
    gestion:2021,
    ccn_financiera:1,
    ccm_academica: 1,
    ccn_academica:1,
    cr_notas:1
}
  
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('cons_datosgenerales', items, {});
  },

  down (queryInterface, Sequelize) { }
};
