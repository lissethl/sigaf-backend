'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { 
    grupo: 'PROGRAMA',
    codigo:'P1',
    nombre: 'MAESTRÍA', 
    descripcion: 'MAESTRÍA', 
    orden: 1,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'PROGRAMA',
    codigo:'P2',
    nombre: 'DOCTORADO', 
    descripcion: 'DOCTORADO', 
    orden: 2,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'PROGRAMA',
    codigo:'P3',
    nombre: 'POST DOCTORADO', 
    descripcion: 'POST DOCTORADO', 
    orden: 3,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'GRADO ACADEMICO',
    codigo: 'G1',
    nombre: 'LICENCIATURA', 
    descripcion: 'LICENCIATURA', 
    orden: 4,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'GRADO ACADEMICO',
    codigo: 'G2',
    nombre: 'MAGISTER SCIENTIARUM (MS.C)', 
    descripcion: 'MAGISTER SCIENTIARUM (MS.C)', 
    orden: 5,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'GRADO ACADEMICO',
    codigo: 'G3',
    nombre: 'PHILOSOPHIÆ DOCTOR (PH.D.)', 
    descripcion: 'PHILOSOPHIÆ DOCTOR (PH.D.)', 
    orden: 6,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C1',
    nombre: 'INGENIERIA AGRONOMICA',
    descripcion: 'INGENIERIA AGRONOMICA',
    orden: 7,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C2',
    nombre: 'ADMINISTRACION DE EMPRESAS',
    descripcion: 'ADMINISTRACION DE EMPRESAS',
    orden: 8,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C3',
    nombre: 'AUDITORIA',
    descripcion: 'AUDITORIA',
    orden: 9,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C4',
    nombre: 'ECONOMIA',
    descripcion: 'ECONOMIA',
    orden: 10,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C5',
    nombre: 'BIOLOGIA',
    descripcion: 'BIOLOGIA',
    orden: 11,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C6',
    nombre: 'ESTADISTICA',
    descripcion: 'ESTADISTICA',
    orden: 12,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C7',
    nombre: 'FISICA',
    descripcion: 'FISICA',
    orden: 13,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C8',
    nombre: 'INFORMATICA',
    descripcion: 'INFORMATICA',
    orden: 14,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C9',
    nombre: 'MATEMATICAS',
    descripcion: 'MATEMATICAS',
    orden: 15,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C10',
    nombre: 'QUIMICA',
    descripcion: 'QUIMICA',
    orden: 16,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C11',
    nombre: 'ANTROPOLOGIA Y ARQUEOLOGIA',
    descripcion: 'ANTROPOLOGIA Y ARQUEOLOGIA',
    orden: 17,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C12',
    nombre: 'CIENCIAS DE LA COMUNICACION SOCIAL',
    descripcion: 'CIENCIAS DE LA COMUNICACION SOCIAL',
    orden: 18,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C13',
    nombre: 'SOCIOLOGIA',
    descripcion: 'SOCIOLOGIA',
    orden: 19,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C14',
    nombre: 'TRABAJO SOCIAL',
    descripcion: 'TRABAJO SOCIAL',
    orden: 20,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C15',
    nombre: 'BIBLIOTECOLOGIA Y CIENCIAS DE LA INFORMACION',
    descripcion: 'BIBLIOTECOLOGIA Y CIENCIAS DE LA INFORMACION',
    orden: 21,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C16',
    nombre: 'CIENCIAS DE LA EDUCACION',
    descripcion: 'CIENCIAS DE LA EDUCACION' ,
    orden: 22,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C17',
    nombre: 'FILOSOFIA',
    descripcion: 'FILOSOFIA',
    orden: 23,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C18',
    nombre: 'HISTORIA',
    descripcion: 'HISTORIA',
    orden: 24,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C19',
    nombre: 'LINGUISTICA E IDIOMAS',
    descripcion: 'LINGUISTICA E IDIOMAS',
    orden: 25,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C20',
    nombre: 'LITERATURA',
    descripcion: 'LITERATURA',
    orden: 26,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C21',
    nombre: 'PSICOLOGIA',
    descripcion: 'PSICOLOGIA',
    orden: 27,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C22',
    nombre: 'TURISMO',
    descripcion: 'TURISMO',
    orden: 28,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C23',
    nombre: 'DERECHO',
    descripcion: 'DERECHO',
    orden: 29,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C24',
    nombre: 'CIENCIAS POLITICAS',
    descripcion: 'CIENCIAS POLITICAS',
    orden: 30,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C25',
    nombre: 'BIOQUIMICA',
    descripcion: 'BIOQUIMICA',
    orden: 31,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C26',
    nombre: 'QUIMICA FARMACEUTICA',
    descripcion: 'QUIMICA FARMACEUTICA',
    orden: 32,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C27',
    nombre: 'INGENIERIA GEOGRAFICA',
    descripcion: 'INGENIERIA GEOGRAFICA',
    orden: 33,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C28',
    nombre: 'INGENIERIA GEOLOGICA',
    descripcion: 'INGENIERIA GEOLOGICA',
    orden: 34,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C29',
    nombre: 'INGENIERIA CIVIL',
    descripcion: 'INGENIERIA CIVIL',
    orden: 35,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C30',
    nombre: 'INGENIERIA ELECTRICA',
    descripcion: 'INGENIERIA ELECTRICA',
    orden: 36,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C31',
    nombre: 'INGENIERIA ELECTRONICA',
    descripcion: 'INGENIERIA ELECTRONICA',
    orden: 37,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C32',
    nombre: 'INGENIERIA INDUSTRIAL',
    descripcion: 'INGENIERIA INDUSTRIAL',
    orden: 38,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C33',
    nombre: 'INGENIERIA MECANICA',
    descripcion: 'INGENIERIA MECANICA',
    orden: 39,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C34',
    nombre: 'INGENIERIA METALURGICA Y MATERIALES',
    descripcion: 'INGENIERIA METALURGICA Y MATERIALES',
    orden: 40,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C35',
    nombre: 'INGENIERIA PETROLERA',
    descripcion: 'INGENIERIA PETROLERA',
    orden: 41,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C36',
    nombre: 'INGENIERIA QUIMICA',
    descripcion: 'INGENIERIA QUIMICA',
    orden: 42,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C37',
    nombre: 'MEDICINA',
    descripcion: 'MEDICINA',
    orden: 43,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C38',
    nombre: 'ENFERMERIA',
    descripcion: 'ENFERMERIA',
    orden: 44,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C39',
    nombre: 'NUTRICION Y DIETETICA',
    descripcion: 'NUTRICION Y DIETETICA',
    orden: 45,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C40',
    nombre: 'TECNOLOGIA MEDICA',
    descripcion: 'TECNOLOGIA MEDICA',
    orden: 46,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C41',
    nombre: 'ODONTOLOGIA',
    descripcion: 'ODONTOLOGIA',
    orden: 47,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C42',
    nombre: 'CONSTRUCCIONES CIVILES',
    descripcion: 'CONSTRUCCIONES CIVILES',
    orden: 48,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C43',
    nombre: 'TOPOGRAFIA Y GEODESIA',
    descripcion: 'TOPOGRAFIA Y GEODESIA',
    orden: 49,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C44',
    nombre: 'ELECTRICIDAD',
    descripcion: 'ELECTRICIDAD',
    orden: 50,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C45',
    nombre: 'ELECTRONICA Y TELECOMUNICACIONES',
    descripcion: 'ELECTRONICA Y TELECOMUNICACIONES',
    orden: 51,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C46',
    nombre: 'ELECTROMECANICA',
    descripcion: 'ELECTROMECANICA',
    orden: 52,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C47',
    nombre: 'MECANICA AUTOMOTRIZ',
    descripcion: 'MECANICA AUTOMOTRIZ',
    orden: 53,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C48',
    nombre: 'AERONAUTICA',
    descripcion: 'AERONAUTICA',
    orden: 54,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C49',
    nombre: 'MECANICA INDUSTRIAL',
    descripcion: 'MECANICA INDUSTRIAL',
    orden: 55,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C50',
    nombre: 'QUIMICA INDUSTRIAL',
    descripcion: 'QUIMICA INDUSTRIAL',
    orden: 56,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C51',
    nombre: 'PROFESOR NORMALISTA',
    descripcion: 'PROFESOR NORMALISTA',
    orden: 57,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C52',
    nombre: 'INGENIERIA TEXTIL',
    descripcion: 'INGENIERIA TEXTIL',
    orden: 58,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C53',
    nombre: 'INGENIERIA AMBIENTAL',
    descripcion: 'INGENIERIA AMBIENTAL',
    orden: 59,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C54',
    nombre: 'INGENIERIA DE SISTEMAS',
    descripcion: 'INGENIERIA DE SISTEMAS',
    orden: 60,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C55',
    nombre: 'ARQUITECTURA',
    descripcion: 'ARQUITECTURA',
    orden: 61,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C56',
    nombre: 'ARTES PLASTICAS Y DISEÑO GRAFICO',
    descripcion: 'ARTES PLASTICAS Y DISEÑO GRAFICO',
    orden: 62,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C57',
    nombre: 'DISEÑO DE INTERIORES Y PAISAJISMO',
    descripcion: 'DISEÑO DE INTERIORES Y PAISAJISMO',
    orden: 63,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C58',
    nombre: 'TURISMO Y HOTELERIA',
    descripcion: 'TURISMO Y HOTELERIA',
    orden: 64,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C59',
    nombre: 'GASTRONOMIA',
    descripcion: 'GASTRONOMIA',
    orden: 65,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C60',
    nombre: 'BIOQUIMICA Y FARMACIA',
    descripcion: 'BIOQUIMICA Y FARMACIA',
    orden: 66,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C61',
    nombre: 'FISIOTERAPIA Y KINESIOLOGIA',
    descripcion: 'FISIOTERAPIA Y KINESIOLOGIA',
    orden: 67,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C62',
    nombre: 'COMERCIO INTERNACIONAL',
    descripcion: 'COMERCIO INTERNACIONAL',
    orden: 68,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C63',
    nombre: 'INGENIERIA COMERCIAL',
    descripcion: 'INGENIERIA COMERCIAL',
    orden: 69,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C64',
    nombre: 'DERECHO Y CIENCIAS JURIDICAS',
    descripcion: 'DERECHO Y CIENCIAS JURIDICAS',
    orden: 70,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C65',
    nombre: 'COMUNICACION Y PERIODISMO',
    descripcion: 'COMUNICACION Y PERIODISMO',
    orden: 71,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C66',
    nombre: 'INGENIERIA DE SISTEMAS INFORMATICOS',
    descripcion: 'INGENIERIA DE SISTEMAS INFORMATICOS',
    orden: 72,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C67',
    nombre: 'INGENIERIA ELECTRONICA Y DE SISTEMAS', 
    descripcion: 'INGENIERIA ELECTRONICA Y DE SISTEMAS',
    orden: 73,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C68',
    nombre: 'INGENIERIA BIOMEDICA',
    descripcion: 'INGENIERIA BIOMEDICA',
    orden: 74,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C69',
    nombre: 'INGENIERIA DE TELECOMUNICACIONES', 
    descripcion: 'INGENIERIA DE TELECOMUNICACIONES',
    orden: 75,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C70',
    nombre: 'INGENIERIA EN INDUSTRIAS ALIMENTARIAS', 
    descripcion: 'INGENIERIA EN INDUSTRIAS ALIMENTARIAS',
    orden: 76,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C71',
    nombre: 'INGENIERIA INDUSTRIAL Y DE SISTEMAS', 
    descripcion: 'INGENIERIA INDUSTRIAL Y DE SISTEMAS',
    orden: 77,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C72',
    nombre: 'INGENIERIA MECANICA Y DE AUTOMATIZACION INDUSTRIAL',
    descripcion: 'INGENIERIA MECANICA Y DE AUTOMATIZACION INDUSTRIAL',
    orden: 78,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C73',
    nombre: 'INGENIERIA ELECTROMECANICA', 
    descripcion: 'INGENIERIA ELECTROMECANICA',
    orden: 79,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C74',
    nombre: 'INGENIERIA DE PETROLEO, GAS Y ENERGIAS',
    descripcion: 'INGENIERIA DE PETROLEO, GAS Y ENERGIAS',
    orden: 80,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C75',
    nombre: 'INGENIERIA FINANCIERAS', 
    descripcion: 'INGENIERIA FINANCIERAS',
    orden: 81,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C76',
    nombre: 'INGENIERIA ELECTRONICA Y TELECOMUNICACIONES', 
    descripcion: 'INGENIERIA ELECTRONICA Y TELECOMUNICACIONES',
    orden: 82,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C77',
    nombre: 'INGENIERIA DE LA PRODUCCION', 
    descripcion: 'INGENIERIA DE LA PRODUCCION',
    orden: 83,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C78',
    nombre: 'INGENIERIA DE MINAS Y METALURGIA',
    descripcion: 'INGENIERIA DE MINAS Y METALURGIA',
    orden: 84,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C79',
    nombre: 'ADMINISTRACION TURISTICA', 
    descripcion: 'ADMINISTRACION TURISTICA',
    orden: 85,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C80',
    nombre: 'CIENCIAS RELIGIOSAS', 
    descripcion: 'CIENCIAS RELIGIOSAS',
    orden: 86,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C81',
    nombre: 'CONTADURIA PUBLICA',
    descripcion: 'CONTADURIA PUBLICA',
    orden: 87,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C82',
    nombre: 'INGENIERIA MECATRONICA', 
    descripcion: 'INGENIERIA MECATRONICA',
    orden: 88,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C83',
    nombre: 'PEDAGOGIA SOCIAL',
    descripcion: 'PEDAGOGIA SOCIAL',
    orden: 89,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C84',
    nombre: 'PSICOPEDAGOGIA',
    descripcion: 'PSICOPEDAGOGIA',
    orden: 90,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C85',
    nombre: 'TEOLOGIA',
    descripcion: 'TEOLOGIA',
    orden: 91,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C86',
    nombre: 'DISEÑO INDUSTRIAL', 
    descripcion: 'DISEÑO INDUSTRIAL',
    orden: 92,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C87',
    nombre: 'INGENIERIA ECONOMICA', 
    descripcion: 'INGENIERIA ECONOMICA',
    orden: 93,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C88',
    nombre: 'COMUNICACION ESTRATEGICA Y CORPORATIVA',
    descripcion: 'COMUNICACION ESTRATEGICA Y CORPORATIVA',
    orden: 94,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C89',
    nombre: 'DISEÑO Y GESTION DE LA MODA', 
    descripcion: 'DISEÑO Y GESTION DE LA MODA',
    orden: 95,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C90',
    nombre: 'INGENIERIA INDUSTRIAL EN MADERA',
    descripcion: 'INGENIERIA INDUSTRIAL EN MADERA',
    orden: 96,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C91',
    nombre: 'INGENIERIA INFORMATICA ADMINISTRATIVA', 
    descripcion: 'INGENIERIA INFORMATICA ADMINISTRATIVA',
    orden: 97,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C92',
    nombre: 'INGENIERIA DE REDES Y TELECOMUNICACIONES', 
    descripcion: 'INGENIERIA DE REDES Y TELECOMUNICACIONES',
    orden: 98,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C93',
    nombre: 'INGENIERIA EN ECOLOGIA Y MEDIO AMBIENTE',
    descripcion: 'INGENIERIA EN ECOLOGIA Y MEDIO AMBIENTE',
    orden: 99,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C94',
    nombre: 'INGENIERIA FORESTAL', 
    descripcion: 'INGENIERIA FORESTAL',
    orden: 100,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C95',
    nombre: 'INGENIERIA  EN ACUICULTURA', 
    descripcion: 'INGENIERIA  EN ACUICULTURA',
    orden: 101,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C96',
    nombre: 'MEDICINA VETERINARIA Y ZOOTECNIA',
    descripcion: 'MEDICINA VETERINARIA Y ZOOTECNIA',
    orden: 102,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C97',
    nombre: 'INGENIERIA EN ECOPISCICULTURA',
    descripcion: 'INGENIERIA EN ECOPISCICULTURA',
    orden: 103,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C98',
    nombre: 'CIENCIAS JURIDICAS Y POLITICAS',
    descripcion: 'CIENCIAS JURIDICAS Y POLITICAS',
    orden: 104,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C99',
    nombre: 'INGENIERIA ZOOTECNICA',
    descripcion: 'INGENIERIA ZOOTECNICA',
    orden: 105,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C100',
    nombre: 'INGENIERIA GEODESTA',
    descripcion: 'INGENIERIA GEODESTA',
    orden: 106,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C101',
    nombre: 'INGENIERIA DE ALIMENTOS',
    descripcion: 'INGENIERIA DE ALIMENTOS',
    orden: 107,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C102',
    nombre: 'INGENIERIA EN SISTEMAS ELECTRONICOS',
    descripcion: 'INGENIERIA EN SISTEMAS ELECTRONICOS',
    orden: 108,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C103',
    nombre: 'INGENIERIA EN GAS Y PETROQUIMICA',
    descripcion: 'INGENIERIA EN GAS Y PETROQUIMICA',
    orden: 109,
    estado: 'ACTIVO'
  },
  {
    grupo: 'CARRERA',
    codigo: 'C104',
    nombre: 'INGENIERIA MECANICA AUTOMOTRIZ',
    descripcion: 'INGENIERIA MECANICA AUTOMOTRIZ',
    orden: 110,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R1',
    nombre: 'CARTA DE SOLICITUD DE ADMISION', 
    descripcion: 'CARTA DE SOLICITUD DE ADMISION', 
    orden: 111,
    estado: 'ACTIVO'
  },
  { 
    grupo: 'REQUISITO',
    codigo: 'R2',
    nombre: 'PROPUESTA DE TRABAJO DE TESIS 1 A 2 PAGINAS', 
    descripcion: 'PROPUESTA DE TRABAJO DE TESIS 1 A 2 PAGINAS', 
    orden: 112,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R3',
    nombre: 'PROYECTO DE INVESTIGACION ENCUADRADO EN LAS LINEAS DE INVESTIGACION (MAXIMO 15 HOJAS)', 
    descripcion: 'PROYECTO DE INVESTIGACION ENCUADRADO EN LAS LINEAS DE INVESTIGACION (MAXIMO 15 HOJAS)', 
    orden: 113,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R4',
    nombre: 'APROBACION PROYECTO DE INVESTIGACION POR COMITE ACADEMICO DE DOCTORADO', 
    descripcion: 'APROBACION PROYECTO DE INVESTIGACION POR COMITE ACADEMICO DE DOCTORADO', 
    orden: 114,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R5',
    nombre: 'CURRICULUM VITAE (NO DOCUMENTADO)', 
    descripcion: 'CURRICULUM VITAE (NO DOCUMENTADO)', 
    orden: 115,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R6',
    nombre: 'FOTOCOPIA SIMPLE DE TITULO ACADEMICO UNIVERSITARIO A NIVEL MAESTRIA', 
    descripcion: 'FOTOCOPIA SIMPLE DE TITULO ACADEMICO UNIVERSITARIO A NIVEL MAESTRIA', 
    orden: 116,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R7',
    nombre: 'FOTOCOPIA LEGALIZADA DE TITULO ACADEMICO UNIVERSITARIO A NIVEL MAESTRIA', 
    descripcion: 'FOTOCOPIA LEGALIZADA DE TITULO ACADEMICO UNIVERSITARIO A NIVEL MAESTRIA', 
    orden: 117,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R8',
    nombre: 'FOTOCOPIA SIMPLE DE CEDULA DE IDENTIDAD', 
    descripcion: 'FOTOCOPIA SIMPLE DE CEDULA DE IDENTIDAD', 
    orden: 118,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R9',
    nombre: 'FOTOCOPIA SIMPLE DE TITULO ACADEMICO UNIVERSITARIO A NIVEL LICENCIATURA', 
    descripcion: 'FOTOCOPIA SIMPLE DE TITULO ACADEMICO UNIVERSITARIO A NIVEL LICENCIATURA', 
    orden: 119,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R10',
    nombre: 'FOTOCOPIA LEGALIZADA DE TITULO ACADEMICO UNIVERSITARIO A NIVEL LICENCIATURA', 
    descripcion: 'FOTOCOPIA LEGALIZADA DE TITULO ACADEMICO UNIVERSITARIO A NIVEL LICENCIATURA', 
    orden: 120,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R11',
    nombre: 'FOTOCOPIA LEGALIZADA DE TITULO EN PROVISION NACIONAL', 
    descripcion: 'FOTOCOPIA LEGALIZADA DE TITULO EN PROVISION NACIONAL', 
    orden: 121,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R12',
    nombre: '3 FOTOGRAFIAS TAMAÑO CARNET', 
    descripcion: '3 FOTOGRAFIAS TAMAÑO CARNET', 
    orden: 122,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R13',
    nombre: 'FOTOCOPIA PRIMERA MATRICULA', 
    descripcion: 'FOTOCOPIA PRIMERA MATRICULA', 
    orden: 123,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R14',
    nombre: 'FOTOCOPIA SEGUNDA MATRICULA', 
    descripcion: 'FOTOCOPIA SEGUNDA MATRICULA', 
    orden: 124,
    estado: 'ACTIVO'
  },
  {
    grupo: 'REQUISITO',
    codigo: 'R15',
    nombre: 'FOTOCOPIA TERCERA MATRICULA', 
    descripcion: 'FOTOCOPIA TERCERA MATRICULA', 
    orden: 125,
    estado: 'ACTIVO'
  },
  {
    grupo: 'OPCION PAGO',
    codigo: 'O1',
    nombre: 'CONTADO', 
    descripcion: 'CONTADO', 
    orden: 126,
    estado: 'ACTIVO'
  },
  {
    grupo: 'OPCION PAGO',
    codigo: 'O2',
    nombre: 'CUOTAS - 50% INICIAL', 
    descripcion: 'CUOTAS 50%', 
    orden: 127,
    estado: 'ACTIVO'
  },
  {
    grupo: 'OPCION PAGO',
    codigo: 'O3',
    nombre: 'CUOTAS - 20% INICIAL', 
    descripcion: 'CUOTAS 20%', 
    orden: 128,
    estado: 'ACTIVO'
  },
  {
    grupo: 'HORARIO',
    codigo: 'H1',
    nombre: 'MARTES Y JUEVES: 19:00 A 22:00 Y SÁBADO: 9:00 A 12:00', 
    descripcion: 'MARTES Y JUEVES: 19:00 A 22:00 Y SÁBADO: 9:00 A 12:00', 
    orden: 129,
    estado: 'ACTIVO'
  },
  {
    grupo: 'HORARIO',
    codigo: 'H2',
    nombre: 'LUNES, MIERCOLES Y VIERNES: 19:00 A 22:00', 
    descripcion: 'LUNES, MIERCOLES Y VIERNES: 19:00 A 22:00', 
    orden: 130,
    estado: 'ACTIVO'
  },
  {
    grupo: 'TEMATICA',
    codigo: 'T1',
    nombre: 'DESARROLLO ECONÓMICO Y SOCIAL', 
    descripcion: 'DESARROLLO ECONÓMICO Y SOCIAL', 
    orden: 132,
    estado: 'ACTIVO'
  },
  {
    grupo: 'TEMATICA',
    codigo: 'T2',
    nombre: 'TRANSFORMACIONES TERRITORIALES Y AMBIENTALES', 
    descripcion: 'TRANSFORMACIONES TERRITORIALES Y AMBIENTALES', 
    orden: 133,
    estado: 'ACTIVO'
  },
  {
    grupo: 'TEMATICA',
    codigo: 'T3',
    nombre: 'FILOSOFÍA Y CIENCIA POLÍTICA', 
    descripcion: 'FILOSOFÍA Y CIENCIA POLÍTICA', 
    orden: 134,
    estado: 'ACTIVO'
  },
  {
    grupo: 'TEMATICA',
    codigo: 'T3',
    nombre: 'METODOLOGÍAS DE INVESTIGACIÓN (TALLERES DE TESIS)', 
    descripcion: 'METODOLOGÍAS DE INVESTIGACIÓN (TALLERES DE TESIS)', 
    orden: 135,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D1',
    nombre: 'CARTA DE POSTULACION', 
    descripcion: 'CARTA DE POSTULACION', 
    orden: 136,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D2',
    nombre: 'CERTIFICADO DEL DEPARTAMENTO DE PERSONAL DOCENTE DE LA UMSA', 
    descripcion: 'CERTIFICADO DEL DEPARTAMENTO DE PERSONAL DOCENTE DE LA UMSA', 
    orden: 137,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D3',
    nombre: 'CURRICULUM VITAE', 
    descripcion: 'CURRICULUM VITAE', 
    orden: 138,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D4',
    nombre: 'FOTOCOPIA SIMPLE CARNET DE IDENTIDAD', 
    descripcion: 'FOTOCOPIA SIMPLE CARNET DE IDENTIDAD', 
    orden: 139,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D5',
    nombre: 'FOTOCOPIA LEGALIZADA DIPLOMA ACADEMICO', 
    descripcion: 'FOTOCOPIA LEGALIZADA DIPLOMA ACADEMICO', 
    orden: 140,
    estado: 'ACTIVO'
  }, 
  {
    grupo: 'DREQUISITO',
    codigo: 'D6',
    nombre: 'FOTOCOPIA LEGALIZADA TITULO EN PROVISION NACIONAL A NIVEL LICENCIATURA', 
    descripcion: 'FOTOCOPIA LEGALIZADA TITULO EN PROVISION NACIONAL A NIVEL LICENCIATURA', 
    orden: 141,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D7',
    nombre: 'FOTOCOPIA LEGALIZADA DEL TÍTULO DE MAGISTER SCIENTIARUM', 
    descripcion: 'FOTOCOPIA LEGALIZADA DEL TÍTULO DE MAGISTER SCIENTIARUM', 
    orden: 142,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D8',
    nombre: 'FOTOCOPIA LEGALIZADA DEL DIPLOMADO EN EDUCACIÓN SUPERIOR', 
    descripcion: 'FOTOCOPIA LEGALIZADA DEL DIPLOMADO EN EDUCACIÓN SUPERIOR', 
    orden: 143,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D9',
    nombre: 'FOTOCOPIA LEGALIZADA DEL TITULO DE PHILOSOPHIÆ DOCTOR', 
    descripcion: 'FOTOCOPIA LEGALIZADA DEL TITULO DE PHILOSOPHIÆ DOCTOR', 
    orden: 144,
    estado: 'ACTIVO'
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D10',
    nombre: 'CERTIFICADO DEL DEPARTAMENTO DE ASESORÍA JURÍDICA QUE ACREDITE NO TENER ANTECEDIENTES ANTI AUTONOMISTAS', 
    descripcion: 'CERTIFICADO DEL DEPARTAMENTO DE ASESORÍA JURÍDICA QUE ACREDITE NO TENER ANTECEDIENTES ANTI AUTONOMISTAS', 
    orden: 145,
    estado: 'ACTIVO',
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D11',
    nombre: 'CERTIFICADO DE NO DEUDAS ECONOMICA PENDIENTES CON LA UMSA', 
    descripcion: 'CERTIFICADO DE NO DEUDAS ECONOMICA PENDIENTES CON LA UMSA', 
    orden: 146,
    estado: 'ACTIVO',
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D12',
    nombre: 'PLAN DE TRABAJO DEL MODULO AL QUE POSTULA (ACTUALIZADO), DE ACUERDO CON LOS CONTENIDOS MÍNIMOS VIGENTES', 
    descripcion: 'PLAN DE TRABAJO DEL MODULO AL QUE POSTULA (ACTUALIZADO), DE ACUERDO CON LOS CONTENIDOS MÍNIMOS VIGENTES', 
    orden: 147,
    estado: 'ACTIVO',
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D13',
    nombre: 'CARTA DE DECLARACION JURADA DE NO TENER PARENTESCO CON AUTORIDADES DEL CIDES', 
    descripcion: 'CARTA DE DECLARACION JURADA DE NO TENER PARENTESCO CON AUTORIDADES DEL CIDES', 
    orden: 148,
    estado: 'ACTIVO',
  },
  {
    grupo: 'DREQUISITO',
    codigo: 'D14',
    nombre: 'DECLARACION JURADA, DE COMPATIBILIDAD CARGA HORARIA Y TOPE SALARIAL', 
    descripcion: 'DECLARACION JURADA, DE COMPATIBILIDAD CARGA HORARIA Y TOPE SALARIAL', 
    orden: 149,
    estado: 'ACTIVO',
  }
]
// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_constantes', items, {});
  },

  down (queryInterface, Sequelize) { }
};
