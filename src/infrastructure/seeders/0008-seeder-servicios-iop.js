'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  {
    codigo: 'CIDES-1',
    metodo: 'Buscar persona por nro de Matricula',
    descripcion: 'Servicio que busca una persona por numero de matrícula',
    entidad: 'UMSA',
    url: 'https://urlSIA',
    token: '<token-interoperabilidad>',
    tipo: 'CONVENIO',
    estado: 'ACTIVO'
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('servicios_iop', items, {});
  },

  down (queryInterface, Sequelize) { }
};
