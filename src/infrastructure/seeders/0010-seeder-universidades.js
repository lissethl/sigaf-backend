'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { nombre: 'UNIVERSIDAD MAYOR DE SAN ANDRES', descripcion: 'LA PAZ', sigla:'U.M.S.A.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD CATOLICA BOLIVIANA SAN PABLO', descripcion: 'LA PAZ', sigla:'U.C.B.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'ESCUELA MILITAR DE INGENIERIA', descripcion: 'LA PAZ', sigla:'E.M.I.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD TECNICA DE ORURO', descripcion: 'ORURO', sigla:'U.T.O.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD TOMAS FRIAS', descripcion: 'POTOSI', sigla:'U.T.F.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD JUAN MISAEL SARACHO', descripcion: 'TARIJA', sigla: 'U.A.J.M.S.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD SAN FRANCISCO XAVIER', descripcion: 'CHUQUISACA', sigla: 'U.S.F.X.',pais:'BOLIVIA', estado:'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD SAN SIMON', descripcion: 'COCHABAMBA', sigla: 'U.M.S.S.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD GABRIEL RENE MORENO', descripcion: 'SANTA CRUZ', sigla: 'U.A.G.R.M.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo:'PUBLICA'},
  { nombre: 'UNIVERSIDAD AMAZONICA DE PANDO', descripcion: 'PANDO',sigla:'U.A.P.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo:'PUBLICA'},
  { nombre: 'UNIVERSIDAD AUTONOMICA DEL BENI JOSE BALLIVIAN', descripcion: 'BENI', sigla: 'U.A.B.J.B.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo:'PUBLICA'},
  { nombre: 'UNIVERSIDAD SALESIANA DE BOLIVIA', descripcion: 'LA PAZ', sigla: 'U.S.B.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD LOYOLA', descripcion: 'LA PAZ', sigla: 'U.L.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD LA SALLE', descripcion: 'LA PAZ', sigla: 'U.L.S.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA DOMINGO SAVIO', descripcion: 'LA PAZ', sigla: 'U.P.D.S.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA BOLIVIANA', descripcion: 'LA PAZ', sigla: 'U.P.B.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD CENTRAL', descripcion: 'LA PAZ', sigla: 'UNICEN', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD BOLIVIANA DE INFORMATICA', descripcion: 'LA PAZ', sigla: 'U.B.I.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD TECNOLOGICA BOLIVIANA', descripcion: 'LA PAZ', sigla: 'UTB', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA NUESTRA SEÑORA DE LA PAZ', descripcion: 'LA PAZ', sigla: 'UNSLP', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA SAN FRANCISCO DE ASIS', descripcion: 'LA PAZ', sigla: 'USFA', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PUBLICA DE EL ALTO', descripcion : 'LA PAZ', sigla: 'U.P.E.A.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD PARA LA INVESTIGACION ESTRATEGICA EN BOLIVIA', descripcion: 'LA PAZ', sigla: 'UPIEB', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA DEL VALLE', descripcion: 'LA PAZ', sigla: 'UNIVALLE', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD ANDINA SIMON BOLIVAR', descripcion: 'CHUQUISACA', sigla: 'UASB', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD DE AQUINO BOLIVIA', descripcion: 'LA PAZ', sigla: 'UDABOL', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA FRANZ TAMAYO', descripcion: 'LA PAZ', sigla: 'UNIFRANZ', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD NUR', descripcion: 'LA PAZ', sigla: 'U.N.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD REAL', descripcion: 'LA PAZ', sigla: 'UREAL', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD DE LOS ANDES', descripcion: 'LA PAZ', sigla: 'UDELOSANDES', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD DE LA CORDILLERA', descripcion: 'LA PAZ', sigla: 'UCORDILLERA', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD SAINT PAUL', descripcion: 'LA PAZ', sigla: 'USP', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD SIMON I. PATIÑO', descripcion: 'COCHABAMBA', sigla: 'USIP', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA ABIERTA LATINOAMERICANA', descripcion: 'COCHABAMBA', sigla: 'UPAL', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD TECNICA PRIVADA COSMOS', descripcion:  'COCHABAMBA', sigla: 'UNITEPC', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD UNION BOLIVARIANA',descripcion: 'LA PAZ', sigla: 'UB', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA DE ORURO', descripcion: 'ORURO', sigla: 'UNIOR', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD NACIONAL ECOLOGICA SANTA CRUZ', descripcion: 'SANTA CRUZ', sigla: 'UECOLOGICA', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD CRISTIANA DE BOLIVIA', descripcion: 'SANTA CRUZ', sigla: 'UCEBOL', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD EMPRESARIAL MATEO KULJIS', descripcion: 'SANTA CRUZ', sigla: 'UEMK', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD EVANGELICA BOLIVIANA', descripcion: 'SANTA CRUZ', sigla: 'UEB', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA CUMBRE', descripcion: 'SANTA CRUZ', sigla: 'UPC', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD BETHESDA', descripcion: 'SANTA CRUZ', sigla: 'UNIBETH', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD PRIVADA DE SANTA CRUZ DE LA SIERRA', descripcion: 'SANTA CRUZ', sigla: 'U.P.S.A.', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD TECNOLOGICA PRIVADA DE SANTA CRUZ', descripcion: 'SANTA CRUZ', sigla: 'UTEPSA', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PRIVADA'},
  { nombre: 'UNIVERSIDAD INDIGENA BOLIVIANA AYMARA TUPAK KATARI', descripcion: 'LA PAZ', sigla: 'UIATK', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD INDIGENA BOLIVIANA QUECHUA CASIMIRO HUANCA', descripcion: 'COCHABAMBA', sigla: 'UIBQCH', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD INDIGENA GUARANI Y PUEBLOS DE TIERRAS BAJAS APIAGUAIKI TUP', descripcion: 'CHUQUISACA', sigla: 'UIPTBA', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'},
  { nombre: 'UNIVERSIDAD POLICIAL MARISCAL ANTONIO JOSE DE SUCRE', descripcion: 'LA PAZ', sigla: 'UPMAJS', pais: 'BOLIVIA', estado: 'ACTIVO', tipo: 'PUBLICA'}
  ];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('ges_universidades', items, {});
  },

  down (queryInterface, Sequelize) { }
};
