'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { 
    nro_modulo: 1,
    codigo:'MDE-MOD-01-2020',
    tipo: 'SIMPLE',
    nombre: 'VISIONES HISTÓRICAS Y TEÓRICAS DEL DESARROLLO',
    descripcion: 'VISIONES HISTÓRICAS Y TEÓRICAS DEL DESARROLLO',
    hrs_presenciales: 48 ,
    hrsno_presenciales: 72, 
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 1
  },
  { 
    nro_modulo: 2,
    codigo:'MDE-MOD-02-2020',
    tipo: 'SIMPLE',
    nombre: 'INTERPRETACIÓN Y PRODUCCIÓN DE TEXTOS ACADÉMICOS',
    descripcion: 'INTERPRETACIÓN Y PRODUCCIÓN DE TEXTOS ACADÉMICOS',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 1 
  }, 
  { 
    nro_modulo: 3,
    codigo:'MDE-MOD-03-2020',
    tipo: 'SIMPLE',
    nombre: 'LAS ESTRATEGIAS DE CRECIMIENTO Y DESARROLLO EN BOLIVIA',
    descripcion: 'LAS ESTRATEGIAS DE CRECIMIENTO Y DESARROLLO EN BOLIVIA',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 4,
    codigo:'MDE-MOD-04-2020',
    tipo: 'SIMPLE',
    nombre: 'MACROECONOMÍA PARA EL DESARROLLO',
    descripcion: 'MACROECONOMÍA PARA EL DESARROLLO',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 5,
    codigo:'MDE-MOD-05-2020',
    tipo: 'SIMPLE',
    nombre: 'CRECIMIENTO ECONÓMICO: TEORÍA Y MODELOS',
    descripcion: '',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 6,
    codigo:'MDE-MOD-06-2020',
    tipo: 'MIXTO',
    nombre: 'ECONOMÍA POLÍTICA Y ANTROPOLOGÍA DEL DESARROLLO',
    descripcion: 'ECONOMÍA POLÍTICA Y ANTROPOLOGÍA DEL DESARROLLO',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 7,
    codigo:'MDE-MOD-07-2020',
    tipo: 'MIXTO',
    nombre: 'TRANSFORMACIÓN PRODUCTIVA E INDUSTRIALIZACIÓN',
    descripcion: 'TRANSFORMACIÓN PRODUCTIVA E INDUSTRIALIZACIÓN',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 8,
    codigo:'MDE-MOD-08-2020',
    tipo: 'MIXTO',
    nombre: 'FINANZAS PÚBLICAS Y DESARROLLO',
    descripcion: 'FINANZAS PÚBLICAS Y DESARROLLO',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2
  }, 
  { 
    nro_modulo: 9,
    codigo:'MDE-MOD-09-2020',
    tipo: 'MIXTO',
    nombre: 'POLÍTICAS ECONÓMICAS Y SOCIALES',
    descripcion: 'POLÍTICAS ECONÓMICAS Y SOCIALES',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 10,
    codigo:'MDE-MOD-10-2020',
    tipo: 'MIXTO',
    nombre: 'EXTRACTIVISMO, SOSTENIBILIDAD Y MEDIO AMBIENTE',
    descripcion: 'EXTRACTIVISMO, SOSTENIBILIDAD Y MEDIO AMBIENTE',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2
  }, 
  { 
    nro_modulo: 11,
    codigo:'MDE-MOD-11-2020',
    tipo: 'MIXTO',
    nombre: 'LA NUEVA ECONOMÍA MUNDIAL EN LA GLOBALIZACIÓN',
    descripcion: 'LA NUEVA ECONOMÍA MUNDIAL EN LA GLOBALIZACIÓN',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  },  
  { 
    nro_modulo: 12,
    codigo:'MDE-MOD-12-2020',
    tipo: 'SIMPLE',
    nombre: 'METODOLOGÍA I. EVALUACIÓN DE POLÍTICAS PÚBLICAS',
    descripcion: 'METODOLOGÍA I. EVALUACIÓN DE POLÍTICAS PÚBLICAS',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2 
  }, 
  { 
    nro_modulo: 13,
    codigo:'MDE-MOD-13-2020',
    tipo: 'SIMPLE',
    nombre: 'METODOLOGÍA II. MÉTODOS CUANTITATIVOS PARA ESTUDIOS DEL DESARROLLO ECONÓMICO',
    descripcion: 'METODOLOGÍA II. MÉTODOS CUANTITATIVOS PARA ESTUDIOS DEL DESARROLLO ECONÓMICO',
    hrs_presenciales: 48,
    hrsno_presenciales: 72,
    total_carga: 120,
    total_creditos: 3,
    id_bloque: 2
  }, 
  { 
    nro_modulo: 14,
    codigo:'MDE-MOD-14-2020',
    tipo: 'SIMPLE',
    nombre: 'TALLER DE TESIS I: ELABORACIÓN DE PERFIL DE TESIS',
    descripcion: 'TALLER DE TESIS I: ELABORACIÓN DE PERFIL DE TESIS',
    hrs_presenciales: 112,
    hrsno_presenciales: 168,
    total_carga: 280,
    total_creditos: 7,
    id_bloque: 3 
  }, 
  { 
    nro_modulo: 15,
    codigo:'MDE-MOD-15-2020',
    tipo: 'SIMPLE',
    nombre: 'TALLER DE TESIS II: AVANCES DE TESIS',
    descripcion: 'TALLER DE TESIS II: AVANCES DE TESIS',
    hrs_presenciales: 224,
    hrsno_presenciales: 336,
    total_carga: 560,
    total_creditos: 14,
    id_bloque: 3
  },
  { 
    nro_modulo: 1,
    codigo:'DMC-MOD-01-2020',
    tipo: 'SIMPLE',
    nombre: 'HISTORIA DEL PENSAMIENTO SOBRE EL CRECIMIENTO Y EL DESARROLLO',
    descripcion: 'HISTORIA DEL PENSAMIENTO SOBRE EL CRECIMIENTO Y EL DESARROLLO',
    hrs_presenciales: 56,
    hrsno_presenciales: 104,
    total_carga: 160,
    total_creditos: 4,
    id_bloque: 4
  },
  { 
    nro_modulo: 2,
    codigo:'DMC-MOD-02-2020',
    tipo: 'SIMPLE',
    nombre: 'LA ECONOMÍA DE LAS INSTITUCIONES Y DE LOS COMUNES',
    descripcion: 'LA ECONOMÍA DE LAS INSTITUCIONES Y DE LOS COMUNES',
    hrs_presenciales: 56,
    hrsno_presenciales: 104,
    total_carga: 160,
    total_creditos: 4,
    id_bloque: 4
  },
  { 
    nro_modulo: 3,
    codigo:'DMC-MOD-03-2020',
    tipo: 'SIMPLE',
    nombre: 'DESARROLLO SOSTENIBLE Y DESIGUALDADES',
    descripcion: 'DESARROLLO SOSTENIBLE Y DESIGUALDADES',
    hrs_presenciales: 56,
    hrsno_presenciales: 104,
    total_carga: 160,
    total_creditos: 4,
    id_bloque: 4
  },
  { 
    nro_modulo: 4,
    codigo:'DMC-MOD-04-2020',
    tipo: 'SIMPLE',
    nombre: 'CAPITALISMO DEL SIGLO XXI',
    descripcion: 'CAPITALISMO DEL SIGLO XXI',
    hrs_presenciales: 56,
    hrsno_presenciales: 104,
    total_carga: 160,
    total_creditos: 4,
    id_bloque: 4
  },
  { 
    nro_modulo: 5,
    codigo:'DMC-MOD-05-2020',
    tipo: 'SIMPLE',
    nombre: 'TALLER DE TESIS I',
    descripcion: 'TALLER DE TESIS I',
    hrs_presenciales: 40,
    hrsno_presenciales: 360,
    total_carga: 400,
    total_creditos: 10,
    id_bloque: 4
  },
  { 
    nro_modulo: 6,
    codigo:'DMC-MOD-06-2020',
    tipo: 'SIMPLE',
    nombre: 'METODOLOGÍAS DE ANÁLISIS I',
    descripcion: 'METODOLOGÍAS DE ANÁLISIS I',
    hrs_presenciales: 40,
    hrsno_presenciales: 360,
    total_carga: 400,
    total_creditos: 10,
    id_bloque: 4
  },
  { 
    nro_modulo: 7,
    codigo:'DMC-MOD-07-2020',
    tipo: 'SIMPLE',
    nombre: 'TRANSFORMACIONES PRODUCTIVAS E INDUSTRIALIZACIÓN',
    descripcion: 'TRANSFORMACIONES PRODUCTIVAS E INDUSTRIALIZACIÓN',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 5
  },
  { 
    nro_modulo: 8,
    codigo:'DMC-MOD-08-2020',
    tipo: 'SIMPLE',
    nombre: 'EXTRACTIVISMO Y MEDIO AMBIENTE',
    descripcion: 'EXTRACTIVISMO Y MEDIO AMBIENTE',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 5
  },
  { 
    nro_modulo: 9,
    codigo:'DMC-MOD-09-2020',
    tipo: 'SIMPLE',
    nombre: 'TALLER DE TESIS II',
    descripcion: 'TALLER DE TESIS II',
    hrs_presenciales: 40,
    hrsno_presenciales: 360,
    total_carga: 400,
    total_creditos: 10,
    id_bloque: 5
  },
  { 
    nro_modulo: 10,
    codigo:'DMC-MOD-010-2020',
    tipo: 'SIMPLE',
    nombre: 'METODOLOGÍAS DE ANÁLISIS II',
    descripcion: 'METODOLOGÍAS DE ANÁLISIS II',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 5
  },
  { 
    nro_modulo: 7,
    codigo:'DMC-MOD-07-2020',
    tipo: 'MIXTO',
    nombre: 'DESIGUALDADES SOCIALES Y DINÁMICAS POBLACIONALES',
    descripcion: 'DESIGUALDADES SOCIALES Y DINÁMICAS POBLACIONALES',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 6
  },
  { 
    nro_modulo: 8,
    codigo:'DMC-MOD-08-2020',
    tipo: 'MIXTO',
    nombre: 'PLURALIDAD ECONÓMICA Y MUNDOS DE TRABAJO',
    descripcion: 'PLURALIDAD ECONÓMICA Y MUNDOS DE TRABAJO',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 6
  },
  { 
    nro_modulo: 9,
    codigo:'DMC-MOD-09-2020',
    tipo: 'MIXTO',
    nombre: 'TALLER DE TESIS II',
    descripcion: 'TALLER DE TESIS II',
    hrs_presenciales: 40,
    hrsno_presenciales: 360,
    total_carga: 400,
    total_creditos: 10,
    id_bloque: 6
  },
  { 
    nro_modulo: 10,
    codigo:'DMC-MOD-010-2020',
    tipo: 'MIXTO',
    nombre: 'METODOLOGÍAS DE ANÁLISIS II',
    descripcion: 'METODOLOGÍAS DE ANÁLISIS II',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 6
  },
  { 
    nro_modulo: 7,
    codigo:'DMC-MOD-07-2020',
    tipo: 'MIXTO',
    nombre: 'INSERCIÓN INTERNACIONAL',
    descripcion: 'INSERCIÓN INTERNACIONAL',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 7
  },
  { 
    nro_modulo: 8,
    codigo:'DMC-MOD-08-2020',
    tipo: 'MIXTO',
    nombre: 'EL DESAFÍO ASIÁTICO',
    descripcion: 'EL DESAFÍO ASIÁTICO',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 7
  },
  { 
    nro_modulo: 9,
    codigo:'DMC-MOD-09-2020',
    tipo: 'MIXTO',
    nombre: 'TALLER DE TESIS II',
    descripcion: 'TALLER DE TESIS II',
    hrs_presenciales: 40,
    hrsno_presenciales: 360,
    total_carga: 400,
    total_creditos: 10,
    id_bloque: 7
  },
  { 
    nro_modulo: 10,
    codigo:'DMC-MOD-010-2020',
    tipo: 'MIXTO',
    nombre: 'METODOLOGÍAS DE ANÁLISIS II',
    descripcion: 'METODOLOGÍAS DE ANÁLISIS II',
    hrs_presenciales: 56,
    hrsno_presenciales: 264,
    total_carga: 320,
    total_creditos: 8,
    id_bloque: 7
  },
  { 
    nro_modulo: 11,
    codigo:'DMC-MOD-011-2020',
    tipo: 'MIXTO',
    nombre: 'TALLER DE TESIS III',
    descripcion: 'TALLER DE TESIS III',
    hrs_presenciales: 40,
    hrsno_presenciales: 520,
    total_carga: 560,
    total_creditos: 14,
    id_bloque: 8
  },


];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('ges_modulos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
