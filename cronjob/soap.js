'use strict'

const soapRequest = require('easy-soap-request');
const fs = require('fs');
const parseString = require('xml2js').parseString;
const { nano } = require('../src/common').text;
 
// example data
const url = 'http://200.7.160.39:8080/axis/SiaWS/cidesWS.jws';
const sampleHeaders = {
  'user-agent': 'sampleTest',
  'Content-Type': 'text/xml;charset=UTF-8',
  'soapAction': ''
};

// usage of module
(async () => {
  const xml = fs.readFileSync('soap.xml', 'utf-8');
  console.log('ingresa hasta xml: ', xml);
  const respuesta = await getWebService(nano(
    xml, 
    { ci: 4882853 },
    'getPersonaResponse',
    'getPersonaReturn'
  ))
  console.log('CODIGO:', respuesta);
})();

function getWebService (xml, getResponse = 'getPersonaResponse', getReturn = 'getPersonaReturn') {
  return new Promise(async (resolve, reject) => {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml, timeout: 1000 }); // Optional timeout parameter(milliseconds)
    const { headers, body, statusCode } = response;
    // console.log(headers);
    // console.log(body);
    parseString(body, function (err, result) {
      if (err) {
        return reject(err)
      }
      // console.dir(result['soapenv:Envelope']['soapenv:Body']);
      const res = result['soapenv:Envelope']['soapenv:Body'];
      for (let item of res) {
        // console.log(item['ns1:getPersonaResponse'])
        for (let data of item[`ns1:${getResponse}`]) {
          // console.log(data.getPersonaReturn[0]['_'])
          return resolve({
            codigo: data[getReturn][0]['_'],
            estado: statusCode
          })
        }
      }
      reject(new Error('No existe el dato'))
      // console.log('RESPUESTA', res[0]['ns1:getPersonaResponse'][0].getPersonaReturn[0]['_'])
    });
    // console.log(statusCode);
  })
}